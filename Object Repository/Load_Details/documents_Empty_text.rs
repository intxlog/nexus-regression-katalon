<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>documents_Empty_text</name>
   <tag></tag>
   <elementGuidId>0a1854ad-02a1-4879-a12f-8ccac3d63893</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Empty.' or . = 'Empty.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Empty.</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>view_details_button</name>
   <tag></tag>
   <elementGuidId>09132e6c-4bb2-4c90-be5d-29fb5cef992c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles-module__primary___2rSQt styles-module__small___3TpxN' and (text() = 'View Details' or . = 'View Details')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscribe'])[1]/preceding::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.styles-module__primary___2rSQt.styles-module__small___3TpxN</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles-module__primary___2rSQt styles-module__small___3TpxN</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/load/804908</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>View Details</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;styles_container__kAnOe&quot;]/main[@class=&quot;styles_pageWrapper__37nuH&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__1k855&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5 styles_padded__2_Nmu&quot;]/div[@class=&quot;styles_container__9R0e7&quot;]/div[@class=&quot;styles_contentWrapper__170P9&quot;]/div[@class=&quot;styles_container__15DCu&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_contentWrapper__3CDq7&quot;]/div[@class=&quot;styles_sortersContainer__3bDWB&quot;]/div[@class=&quot;styles_listWrapper__29nHZ&quot;]/div[@class=&quot;styles_container__3r0cB&quot;]/div[@class=&quot;styles_cell__36eyB styles_highlighted__3_VDo&quot;]/a[@class=&quot;styles-module__primary___2rSQt styles-module__small___3TpxN&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/main/div/div/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div[21]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'View Details')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ordered'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CO'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscribe'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/load/804908')])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[21]/a</value>
   </webElementXpaths>
</WebElementEntity>

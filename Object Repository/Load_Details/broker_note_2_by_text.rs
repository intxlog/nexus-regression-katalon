<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>find the second broker notes element by text, not by xpath</description>
   <name>broker_note_2_by_text</name>
   <tag></tag>
   <elementGuidId>b54dbc42-61c5-4e87-9ebf-c5df9022bbde</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Note: nexus notes 2' or . = 'Note: nexus notes 2')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Note: nexus notes 2</value>
   </webElementProperties>
</WebElementEntity>

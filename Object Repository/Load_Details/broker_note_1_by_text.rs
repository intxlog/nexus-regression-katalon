<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>find the first broker notes element by text, not xpath</description>
   <name>broker_note_1_by_text</name>
   <tag></tag>
   <elementGuidId>159a8aa3-0b01-4f11-8e1f-1b6356c06fee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Note: nexus notes 1' or . = 'Note: nexus notes 1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Note: nexus notes 1</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Notes text section that displays the retrieved text from the nexus notes in iTMS</description>
   <name>Notes_broker</name>
   <tag></tag>
   <elementGuidId>64679a97-098f-4041-96ee-c68177114cfc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Note: ' or . = 'Note: ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Note: </value>
   </webElementProperties>
</WebElementEntity>

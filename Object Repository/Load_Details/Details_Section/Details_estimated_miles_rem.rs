<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Details_estimated_miles_rem</name>
   <tag></tag>
   <elementGuidId>08c7744a-b74b-48d0-9b44-1296e77afdb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), ' estimated miles remaining') or contains(., ' estimated miles remaining'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> estimated miles remaining</value>
   </webElementProperties>
</WebElementEntity>

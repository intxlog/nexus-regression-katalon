<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>the X button to remove a document which was added</description>
   <name>document_x_button</name>
   <tag></tag>
   <elementGuidId>b69aa84d-73a0-4b13-ba3b-27620451dce0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_unsubmit__2M420']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>✕</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_unsubmit__2M420</value>
   </webElementProperties>
</WebElementEntity>

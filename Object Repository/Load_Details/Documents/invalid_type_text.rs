<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid_type_text</name>
   <tag></tag>
   <elementGuidId>71f29203-a53a-475f-8c72-6d63e22a2a7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = concat(&quot;That&quot; , &quot;'&quot; , &quot;s not a supported file type. Please try again.&quot;) or . = concat(&quot;That&quot; , &quot;'&quot; , &quot;s not a supported file type. Please try again.&quot;))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>That's not a supported file type. Please try again.</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_deliveryPO</name>
   <tag></tag>
   <elementGuidId>b634f70e-e7db-47de-974f-ccf0de2c82f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@class, 'form-control input-sm droppo')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control input-sm droppo ediControl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td:nth-child(7) > input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm droppo ediControl</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[contains(@class, 'form-control input-sm droppo')]</value>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox_delivered</name>
   <tag></tag>
   <elementGuidId>394a9471-ba71-4475-a1cd-3180892831c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id, 'rowDropDel')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>#rowDropDel000000</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#rowDropDel000000</value>
   </webElementProperties>
</WebElementEntity>

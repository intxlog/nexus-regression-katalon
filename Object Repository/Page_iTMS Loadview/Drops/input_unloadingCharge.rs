<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_unloadingCharge</name>
   <tag></tag>
   <elementGuidId>9e5df10c-339d-4acb-b0e8-72f7c5cdeae8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[count(. | id(&quot;426591&quot;)/td[10]/input[@class=&quot;form-control input-sm dropunchrg&quot;]) = count(id(&quot;426591&quot;)/td[10]/input[@class=&quot;form-control input-sm dropunchrg&quot;])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td:nth-child(10) > input</value>
   </webElementProperties>
</WebElementEntity>

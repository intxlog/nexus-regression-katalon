<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_consigneePhone</name>
   <tag></tag>
   <elementGuidId>569e8dbc-69a1-4fd0-969e-cbb7661a91e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#\30 00000 > td:nth-child(4)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div[2]/table/tbody/tr/td[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td:nth-child(4)</value>
   </webElementProperties>
</WebElementEntity>

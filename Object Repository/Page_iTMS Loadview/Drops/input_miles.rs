<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_miles</name>
   <tag></tag>
   <elementGuidId>beba7c6b-454b-40e5-a29c-ce2dcd27ac2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#\30 00000 > td:nth-child(13)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Notes'])[2]/preceding::td[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td:nth-child(13)</value>
   </webElementProperties>
</WebElementEntity>

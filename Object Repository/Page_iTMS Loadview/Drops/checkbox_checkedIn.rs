<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox_checkedIn</name>
   <tag></tag>
   <elementGuidId>31dfea18-cd61-401c-9193-c40244b6607b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id, 'rowDropCI')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>#rowDropCI000000</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#rowDropCI000000</value>
   </webElementProperties>
</WebElementEntity>

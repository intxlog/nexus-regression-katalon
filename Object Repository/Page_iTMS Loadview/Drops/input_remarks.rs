<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_remarks</name>
   <tag></tag>
   <elementGuidId>5dc7bbcb-e3db-4286-a403-58b0a67a0756</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@class, 'form-control input-sm dropremark')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control input-sm dropremark ediControl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td:nth-child(14) > input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm dropremark ediControl</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[contains(@class, 'form-control input-sm dropremark')]</value>
   </webElementXpaths>
</WebElementEntity>

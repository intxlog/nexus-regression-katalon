<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_consigneeAddress</name>
   <tag></tag>
   <elementGuidId>1fae2826-fca1-40e6-845e-571e490ea29d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#\30 00000 > td.dropAddressCol</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div[2]/table/tbody/tr/td[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td.dropAddressCol</value>
   </webElementProperties>
</WebElementEntity>

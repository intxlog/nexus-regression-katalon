<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_deleteDrop</name>
   <tag></tag>
   <elementGuidId>afb73404-fae6-4b41-8b55-580a5dd28edd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#\30 00000 > td:nth-child(15)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Add'])[1]/following::button[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#\30 00000 > td:nth-child(15)</value>
   </webElementProperties>
</WebElementEntity>

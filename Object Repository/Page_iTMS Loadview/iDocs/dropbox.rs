<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>dropbox area to drag files to for uploading</description>
   <name>dropbox</name>
   <tag></tag>
   <elementGuidId>6016aced-ffdb-4e6f-9956-b2d7512e9363</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'dropbox']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dropbox</value>
   </webElementProperties>
</WebElementEntity>

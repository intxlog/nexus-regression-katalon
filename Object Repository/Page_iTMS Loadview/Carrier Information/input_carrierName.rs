<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_carrierName</name>
   <tag></tag>
   <elementGuidId>6b202a5a-2f8b-4790-b8fc-e6234670cb7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[count(. | //*[@id=&quot;carriername&quot;]) = count(//*[@id=&quot;carriername&quot;])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;carriername&quot;]</value>
   </webElementProperties>
</WebElementEntity>

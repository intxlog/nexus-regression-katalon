<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_carrierInformation</name>
   <tag></tag>
   <elementGuidId>172eea5e-83db-417f-8303-627b22368f44</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;carrierinfo-wrapper&quot;)/div[@class=&quot;row row-color-purple&quot;]/div[@class=&quot;col-md-9&quot;]/div[@class=&quot;col-md-10 col-sm-10&quot;]/p[@class=&quot;loadview-section-header&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CARRIER INFORMATION</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>loadview-section-header</value>
   </webElementProperties>
</WebElementEntity>

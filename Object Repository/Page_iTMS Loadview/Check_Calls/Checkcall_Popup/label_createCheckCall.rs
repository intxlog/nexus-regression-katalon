<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_createCheckCall</name>
   <tag></tag>
   <elementGuidId>e72b122e-c2ba-4e38-84ff-dad5309eff46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;checkCallModal&quot;]/div/div/div[1]/h4[text()=&quot;Create Check Call&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;checkCallModal&quot;]/div/div/div[1]/h4[text()=&quot;Create Check Call&quot;]</value>
   </webElementProperties>
</WebElementEntity>

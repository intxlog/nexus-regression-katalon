<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_makePrimaryNote</name>
   <tag></tag>
   <elementGuidId>cb5f2b93-7fc3-42e8-8b54-0c1f9b3f3637</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;checkcall-form&quot;]/div[8]/label[text() = &quot;Make this the Primary Note&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;checkcall-form&quot;]/div[8]/label[text() = &quot;Make this the Primary Note&quot;]</value>
   </webElementProperties>
</WebElementEntity>

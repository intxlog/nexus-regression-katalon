<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>amount_input</name>
   <tag></tag>
   <elementGuidId>6d44a29e-e72a-4d98-8395-c39606494f72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='0.00']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3a565d20-6d7c-4f64-932e-5147bc8c6364</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>111a5589-fec3-437c-97f1-e4f7b62ba326</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>saveExpenseAmount(this.value, 385495)</value>
      <webElementGuid>275f5ad7-408d-40f6-9c81-b84dfe3d0bdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0.00</value>
      <webElementGuid>f9a17670-2519-41ab-8568-30f741c7aec6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;expensesboxtable&quot;)/tbody[1]/tr[1]/td[3]/input[1]</value>
      <webElementGuid>1db3904a-381e-4fb5-a35b-5429f8071d82</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='0.00']</value>
      <webElementGuid>10212d2b-6b20-45fc-b813-32cfb3583189</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='expensesboxtable']/tbody/tr/td[3]/input</value>
      <webElementGuid>a1bd7986-fde1-4c2c-88ec-f7acb9d68b9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>7bd8c252-80a3-4403-bfae-dc5bf82332c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>0a7756b8-4db5-4724-aebf-7a4ee08d1a15</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_margin</name>
   <tag></tag>
   <elementGuidId>862482e1-d42f-4dac-99cf-ea90112a6be2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#loadview-body > div.container-fluid > div:nth-child(4) > div.col-md-4 > div:nth-child(3) > div:nth-child(2) > h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Margin: </value>
   </webElementProperties>
</WebElementEntity>

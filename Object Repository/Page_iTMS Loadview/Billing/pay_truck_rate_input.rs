<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pay_truck_rate_input</name>
   <tag></tag>
   <elementGuidId>1f710a54-a933-4259-b950-eb50080c1a08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@onblur, 'savePTRate')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>savePTRate</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_profit</name>
   <tag></tag>
   <elementGuidId>c5720349-dbc8-4ca2-9c4f-2ddca733bad8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#loadview-body > div.container-fluid > div:nth-child(4) > div.col-md-4 > div:nth-child(3) > div:nth-child(1) > h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Profit: </value>
   </webElementProperties>
</WebElementEntity>

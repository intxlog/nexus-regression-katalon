<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pay_truck_add_btn</name>
   <tag></tag>
   <elementGuidId>c63276dd-b530-43b8-a02e-fce70d6a96d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@onclick = 'addPayTruck();']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>addPayTruck();</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create New</name>
   <tag></tag>
   <elementGuidId>25905cf6-484e-4eb7-9e7b-f0478520bcee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#newBtn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='newBtn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ec254dc8-0708-4a45-a2c5-e2677bc22326</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1f4adad9-e61f-45c6-b6d5-f1af87064d74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>newBtn</value>
      <webElementGuid>d97c60b4-555f-45ca-a1cf-9490dd3c57f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>createCC();</value>
      <webElementGuid>3ca86719-61c7-4231-9ee3-3d61675842a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>6b386886-7264-43d3-8030-1ccdfe0064a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create New</value>
      <webElementGuid>c0bff080-874b-49fb-bfab-2a486a37f419</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;newBtn&quot;)</value>
      <webElementGuid>e53c0e08-85ef-4c3a-89bd-905479dd2eb1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='newBtn']</value>
      <webElementGuid>5562a011-15e3-41ef-a5b7-68243e7ca4b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Waive Fee'])[1]/following::button[1]</value>
      <webElementGuid>8800df9f-4b76-4503-938f-f19bcc379a3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::button[1]</value>
      <webElementGuid>e2c8f665-25a7-4dd8-9ed4-b07d0ea554e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Issued Advances'])[1]/preceding::button[2]</value>
      <webElementGuid>be324238-f110-439b-b0a3-00b30c3d0cfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create New']/parent::*</value>
      <webElementGuid>4c13be17-cc0d-40de-b380-974a194aa5a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>4321868b-fc9c-4540-ba37-f4b52bfd2e6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'newBtn' and (text() = 'Create New' or . = 'Create New')]</value>
      <webElementGuid>b00f12b7-a4e5-4db4-a890-3dc39e5aeab7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_typeSD</name>
   <tag></tag>
   <elementGuidId>138a5da6-66fc-43dd-84b6-e621bf733221</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#pt000000row > td:nth-child(3) > select > option[selected]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#pt000000row > td:nth-child(3) > select > option[selected]</value>
   </webElementProperties>
</WebElementEntity>

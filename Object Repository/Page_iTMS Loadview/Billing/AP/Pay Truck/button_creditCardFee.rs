<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_creditCardFee</name>
   <tag></tag>
   <elementGuidId>197f2a56-5be4-47a8-8e54-80e27df94752</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;bottomBtnBox&quot;]/button[text()=&quot;Credit Card Fee&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;bottomBtnBox&quot;]/button[text()=&quot;Credit Card Fee&quot;]</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_add</name>
   <tag></tag>
   <elementGuidId>09c827fc-6ca7-4c0d-ad93-f0f5e0fa521c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;bottomBtnBox&quot;]/button[text()=&quot;Add&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;bottomBtnBox&quot;]/button[text()=&quot;Add&quot;]</value>
   </webElementProperties>
</WebElementEntity>

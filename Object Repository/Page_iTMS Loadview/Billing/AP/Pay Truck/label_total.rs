<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_total</name>
   <tag></tag>
   <elementGuidId>011cecc9-3cf7-4f07-a57e-0e8c30d65e34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;paytruckboxtable&quot;]/thead/tr/th[text()=&quot;Total&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;paytruckboxtable&quot;]/thead/tr/th[text()=&quot;Total&quot;]</value>
   </webElementProperties>
</WebElementEntity>

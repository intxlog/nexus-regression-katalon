<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_netPay</name>
   <tag></tag>
   <elementGuidId>bd434897-dacd-4ac3-8c24-ec85c5701f40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#loadview-body > div.container-fluid > div:nth-child(4) > div.col-md-4 > div:nth-child(2) > div:nth-child(2) > div > label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Net Pay</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#loadview-body > div.container-fluid > div:nth-child(4) > div.col-md-4 > div:nth-child(2) > div:nth-child(2) > div > label</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>trip_miles_btn</name>
   <tag></tag>
   <elementGuidId>707304f5-4c37-43e3-982d-0ca782346443</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-default btn-xs tripMilesButton label-button' and (text() = 'Trip Miles' or . = 'Trip Miles')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-xs tripMilesButton label-button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Trip Miles</value>
   </webElementProperties>
</WebElementEntity>

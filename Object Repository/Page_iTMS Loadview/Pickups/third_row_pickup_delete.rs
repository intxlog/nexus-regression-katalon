<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>third_row_pickup_delete</name>
   <tag></tag>
   <elementGuidId>cb1bae7d-be9d-47fe-a04b-c5eb96beddc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[2]/td[13]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-trash']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-trash</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;830381&quot;)/td[@class=&quot;text-center&quot;]/button[@class=&quot;btn btn-default btn-xs cust-delete-btn&quot;]/i[@class=&quot;fa fa-trash&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='830381']/td[13]/button/i</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[13]/button/i</value>
   </webElementXpaths>
</WebElementEntity>

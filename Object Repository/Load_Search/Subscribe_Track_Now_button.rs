<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Subscribe_Track_Now_button</name>
   <tag></tag>
   <elementGuidId>149cb66e-ac79-45d7-b09a-db8d463ecba0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Track Now']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@value = 'Track Now']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.styles_btn__1N32v.styles_btn_submit__3P8Ab</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_btn__1N32v styles_btn_submit__3P8Ab</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Track Now</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;styles_container__kAnOe&quot;]/main[@class=&quot;styles_pageWrapper__37nuH&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__1k855&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5 styles_padded__2_Nmu&quot;]/div[@class=&quot;styles_container__9R0e7&quot;]/div[@class=&quot;styles_contentWrapper__170P9&quot;]/div[@class=&quot;styles_container__15DCu&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_contentWrapper__3CDq7&quot;]/div[@class=&quot;styles_sortersContainer__3bDWB&quot;]/div[@class=&quot;styles_listWrapper__29nHZ&quot;]/div[@class=&quot;styles_container__3r0cB&quot;]/div[@class=&quot;styles_modalWrapper__1TTfZ&quot;]/div[@class=&quot;styles_modal__2K-RV&quot;]/div[@class=&quot;styles_emailCCSubmit__-PKjD&quot;]/div[@class=&quot;styles_buttonContainer__37PLa&quot;]/input[@class=&quot;styles_btn__1N32v styles_btn_submit__3P8Ab&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Track Now']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/main/div/div/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div[23]/div/div[2]/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[23]/div/div[2]/div/input</value>
   </webElementXpaths>
</WebElementEntity>

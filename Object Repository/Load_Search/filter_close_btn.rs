<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>filter_close_btn</name>
   <tag></tag>
   <elementGuidId>dbbc43b9-231d-4ca1-9c73-5efccd2c86de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles-module__plain___2T3h0 styles_closeButton__360wu']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles-module__plain___2T3h0 styles_closeButton__360wu</value>
      <webElementGuid>e7234844-b75d-4a13-a950-399c40dec52a</webElementGuid>
   </webElementProperties>
</WebElementEntity>

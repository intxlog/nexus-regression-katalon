<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delivery_CheckIn_Status</name>
   <tag></tag>
   <elementGuidId>405f87cb-6fc3-44e8-a60a-b20b090b4d82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_cell__36eyB styles_highlighted__3_VDo' and (text() = 'Delivery Check-In' or . = 'Delivery Check-In')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_cell__36eyB styles_highlighted__3_VDo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delivery Check-In</value>
   </webElementProperties>
</WebElementEntity>

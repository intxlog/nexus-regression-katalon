<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Ordered_Status</name>
   <tag></tag>
   <elementGuidId>e82dff6d-49ff-4ecf-a320-cfd3ac5c284a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_cell__36eyB styles_highlighted__3_VDo' and (text() = 'Ordered' or . = 'Ordered')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_cell__36eyB styles_highlighted__3_VDo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ordered</value>
   </webElementProperties>
</WebElementEntity>

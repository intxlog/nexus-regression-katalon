<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delivered_Status</name>
   <tag></tag>
   <elementGuidId>28f61c03-22b5-4c3e-b8d5-f0fba4171f3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_cell__36eyB styles_highlighted__3_VDo' and (text() = 'Delivered' or . = 'Delivered')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_cell__36eyB styles_highlighted__3_VDo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delivered</value>
   </webElementProperties>
</WebElementEntity>

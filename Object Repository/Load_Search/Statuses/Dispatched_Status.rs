<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dispatched_Status</name>
   <tag></tag>
   <elementGuidId>573c6530-e251-4be5-baca-ec69f81c64df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_cell__36eyB styles_highlighted__3_VDo' and (text() = 'Dispatched' or . = 'Dispatched')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_cell__36eyB styles_highlighted__3_VDo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Dispatched</value>
   </webElementProperties>
</WebElementEntity>

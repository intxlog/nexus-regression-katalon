<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pick_CheckIn_Status</name>
   <tag></tag>
   <elementGuidId>d05ae515-674a-427e-990c-3ee130fc8586</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_cell__36eyB styles_highlighted__3_VDo' and (text() = 'Pickup Check-In' or . = 'Pickup Check-In')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_cell__36eyB styles_highlighted__3_VDo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pickup Check-In</value>
   </webElementProperties>
</WebElementEntity>

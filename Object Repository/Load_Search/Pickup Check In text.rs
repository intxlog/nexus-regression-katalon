<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pickup Check In text</name>
   <tag></tag>
   <elementGuidId>4cb06dd5-34f3-432b-8105-4f498977f04b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Pickup Check-In' or . = 'Pickup Check-In')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pickup Check-In</value>
      <webElementGuid>8ea8d05b-4647-4e49-8e75-6f0dfa404c1d</webElementGuid>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>total_AR_column</name>
   <tag></tag>
   <elementGuidId>d3e658a3-fb07-496a-a8b3-dd909efbdbd4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Total Billed' or . = 'Total Billed')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Total Billed</value>
      <webElementGuid>fc6fc134-100b-47a3-8fba-a3d61ad5328c</webElementGuid>
   </webElementProperties>
</WebElementEntity>

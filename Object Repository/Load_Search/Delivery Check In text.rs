<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delivery Check In text</name>
   <tag></tag>
   <elementGuidId>c544ee64-dfea-4e98-a08d-661a173bd071</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Delivery Check-In' or . = 'Delivery Check-In')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delivery Check-In</value>
      <webElementGuid>25ce0b7e-d473-4608-b4ac-cf251cd84419</webElementGuid>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Unsubscribe_Yes_confirm_btn</name>
   <tag></tag>
   <elementGuidId>1a63163a-accc-4822-a3c4-c911fc70ffd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@value = 'Yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>edb4c047-22ff-453f-b4d5-039265700808</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>45c786f7-d331-460a-9fea-189ecd14887a</webElementGuid>
   </webElementProperties>
</WebElementEntity>

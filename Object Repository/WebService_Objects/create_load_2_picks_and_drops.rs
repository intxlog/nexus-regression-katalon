<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>create a new load with the IEL Tester toolkit, but add the condition to add two picks and two drops</description>
   <name>create_load_2_picks_and_drops</name>
   <tag></tag>
   <elementGuidId>0319707f-5a8b-4caf-a545-deeb4641ef8f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;customerid&quot;,
      &quot;value&quot;: &quot;8160&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;billinglinecount&quot;,
      &quot;value&quot;: &quot;1&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;pickcount&quot;,
      &quot;value&quot;: &quot;2&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;dropcount&quot;,
      &quot;value&quot;: &quot;2&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;loadcondition&quot;,
      &quot;value&quot;: &quot;rtb&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;loadcount&quot;,
      &quot;value&quot;: &quot;1&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>15aaad1b-95ba-4c45-8c52-e3f3e0537929</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZmJjZGE0NGYwODU3ZjkwMDdhOTkzYjc1Y2Q4OTE2YTkzYTY2OGM4MmY3OGE1YzAwMjcwZjM0OGM1NDI3N2IwNjM0Yjg2OGQwZGEwM2VlNWEiLCJpYXQiOjE2NTYwODAwMzguNTcxNTEzLCJuYmYiOjE2NTYwODAwMzguNTcxNTIsImV4cCI6MTY4NzYxNjAzOC41NjQ0MTcsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.E8abz4zeA4eDuirfzfJkBB-XvCJ6fryVomyqS96BB9dMKOT5OoWOwIgSlSETeSXXQwerDcwqk4D8Od77R2knmqsi5ZPf9BXmziZeLVarm8cOOC7lJeuU6shMU6IID_Pqifwk0IbGAjZc2FjYxvGHZDukyFhuJAGr9hJtIDqf4u3bSO6sKaG0pge2KlBrFyNsawcWeZ6nhnL-qTqiHSo9jV0NeZioCF-50M07DIWVV364ImGSX5Z5I29OiVSMI5CEx3TdCV3YOnke09jwBYghDlhi0xHKZYZA0rJ53NzbtLeDz1VlKLXBSPIpNMQBbrgTetEe4k0ldyzMeDQ3BVsGXigwSlrm68F_0AgAS_RVC469C621ZICyeWXh-0TazuTk2fWzP_39LCCx5MsqmWIstL3MJDpo4zlv1ph2B26-pTsktebIXyyYSNnyqnCgCQLQ6JK-Z4Vg4BvwYR9WjZxYpAAk_FsPWCr4UNr0XVWFE2DAmuIVwoAHU8scLC35XfKkDbSjsn4Q-huofyjMuA3vG66okOubT3QhUjH4CPzy51MZq8eBD7DySN0VFsWb1hrIKWQ1zY0h7ejapzhJXvOQD8ylkEuQkERFn37gfr6p17vFQwoE-CBIuIyDjBOHjPNhf_DQbt5GKqxK1LJde1HlSt6R7n-AL5mkBb1p-H97Odk</value>
      <webElementGuid>aa36de31-80a7-4ea5-9026-f0904e0aba10</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.1.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${itms_api_url}/testertoolkit/load/create</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'https://itmsqa-api.intxlog.com'</defaultValue>
      <description></description>
      <id>5306bf1a-f3c6-4657-bbf9-85eff6be0869</id>
      <masked>false</masked>
      <name>itms_api_url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>locked_out_msg</name>
   <tag></tag>
   <elementGuidId>17993e17-f998-4dd9-8ee4-5f00a0797c22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'You have been locked out. Please wait at least 1 minute before trying your password again.' or . = 'You have been locked out. Please wait at least 1 minute before trying your password again.') and @class = 'styles_errorMessage__2zBZI']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>You have been locked out. Please wait at least 1 minute before trying your password again.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_errorMessage__2zBZI</value>
   </webElementProperties>
</WebElementEntity>

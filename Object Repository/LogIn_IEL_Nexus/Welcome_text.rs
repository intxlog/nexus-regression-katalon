<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Welcome_text</name>
   <tag></tag>
   <elementGuidId>35b79f40-be3c-44d3-8832-bb72c4ffec4e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Welcome! ' or . = ' Welcome! ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Welcome! </value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>in DEV, the 401 Unauth error shows up for expired link</description>
   <name>unauthorized_error</name>
   <tag></tag>
   <elementGuidId>a2d1d8f9-a169-4a65-a4b3-2fe59b211fff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '401 Unauthorized' or . = '401 Unauthorized')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>401 Unauthorized</value>
   </webElementProperties>
</WebElementEntity>

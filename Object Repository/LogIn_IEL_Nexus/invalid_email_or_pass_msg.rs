<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid_email_or_pass_msg</name>
   <tag></tag>
   <elementGuidId>025cd15f-7b6f-4ff2-904b-c62c730e53c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Invalid Email or Password. Please contact NEXUS@ielfreight.com for assistance.' or . = 'Invalid Email or Password. Please contact NEXUS@ielfreight.com for assistance.') and @class = 'styles_errorMessage__2zBZI']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invalid Email or Password. Please contact NEXUS@ielfreight.com for assistance.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_errorMessage__2zBZI</value>
   </webElementProperties>
</WebElementEntity>

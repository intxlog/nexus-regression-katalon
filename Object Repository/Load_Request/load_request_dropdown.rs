<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>drop-down menu from the top toolbar</description>
   <name>load_request_dropdown</name>
   <tag></tag>
   <elementGuidId>eb52650b-8c99-4e13-9e0f-d44378edba79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_loadRequest__2DJja']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_loadRequest__2DJja</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>accepted_text</name>
   <tag></tag>
   <elementGuidId>fd8451dc-77ec-4073-bf87-96a5b61baa5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'ACCEPTED' or . = 'ACCEPTED')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ACCEPTED</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>utton in the Load Request drop-down</description>
   <name>submit_request_button</name>
   <tag></tag>
   <elementGuidId>26d1f649-4cc7-468e-a1b4-420f5060bb48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Submit Request' or . = 'Submit Request')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_link__3FL7M</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit Request</value>
   </webElementProperties>
</WebElementEntity>

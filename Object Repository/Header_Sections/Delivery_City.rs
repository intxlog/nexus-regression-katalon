<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delivery_City</name>
   <tag></tag>
   <elementGuidId>a9f44c7d-0537-4667-91fa-c88e2f3636de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Delivery City' or . = 'Delivery City')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div/div[6]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_label__1rOOz</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delivery City</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;styles_container__kAnOe&quot;]/main[@class=&quot;styles_pageWrapper__37nuH&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__1k855&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__2wAsA&quot;]/div[@class=&quot;styles_content__37-XK styles_dataViewWrapper__3mZRV&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__15DCu&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_contentWrapper__3CDq7&quot;]/div[@class=&quot;styles_sortersContainer__3bDWB&quot;]/div[@class=&quot;styles_listWrapper__29nHZ&quot;]/div[@class=&quot;styles_container__3r0cB&quot;]/div[@class=&quot;styles_container__2ffKE&quot;]/div[@class=&quot;styles_textWrapper__148LD&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div/div[6]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delivery Date'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pickup State'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delivery State'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Delivery City']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div</value>
   </webElementXpaths>
</WebElementEntity>

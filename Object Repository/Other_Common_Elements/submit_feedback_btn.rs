<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submit_feedback_btn</name>
   <tag></tag>
   <elementGuidId>8ab2be03-2e23-4a6d-a94d-6776b9d1080f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Submit Feedback' or . = 'Submit Feedback')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit Feedback</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>capitalized OK button</description>
   <name>OK_capital_btn</name>
   <tag></tag>
   <elementGuidId>adf8d1e1-feec-4a2a-a087-df7c11880319</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;loadview-body&quot;]/div[2]/div/div[4]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal-button swal-button--confirm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;loadview-body&quot;]/div[2]/div/div[4]/div/button</value>
   </webElementProperties>
</WebElementEntity>

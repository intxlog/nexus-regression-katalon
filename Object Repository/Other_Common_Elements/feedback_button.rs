<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>feedback_button</name>
   <tag></tag>
   <elementGuidId>0b1d8541-ebc4-495c-b1d1-ddf24712a118</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Leave Feedback!' or . = 'Leave Feedback!')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Leave Feedback!</value>
   </webElementProperties>
</WebElementEntity>

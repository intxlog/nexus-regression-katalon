<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>customer display element -When the drop down selection is NOT present</description>
   <name>customer_name_display</name>
   <tag></tag>
   <elementGuidId>47a439e0-0bff-4d12-80c6-58274d97b608</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_companyName__2phGD']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_companyName__2phGD</value>
   </webElementProperties>
</WebElementEntity>

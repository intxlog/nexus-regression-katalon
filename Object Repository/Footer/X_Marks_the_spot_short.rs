<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>X_Marks_the_spot_short</name>
   <tag></tag>
   <elementGuidId>5758b996-1f18-46ff-b0ce-e432ee98f8ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'X marks the spot' or . = 'X marks the spot')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>X marks the spot</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>terms_of_use_doc</name>
   <tag></tag>
   <elementGuidId>687446ba-bad0-4b92-80b3-e1e04c743921</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@src = 'https://www.ielfreight.com/wp-content/uploads/2019/10/IEL-Standard-Terms-of-Use.pdf']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://www.ielfreight.com/wp-content/uploads/2019/10/IEL-Standard-Terms-of-Use.pdf</value>
   </webElementProperties>
</WebElementEntity>

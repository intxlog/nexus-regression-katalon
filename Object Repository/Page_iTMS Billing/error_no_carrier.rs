<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>error_no_carrier</name>
   <tag></tag>
   <elementGuidId>d690c0c1-e3a3-476e-b5c8-07e84b2efc30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'A carrier must be on the load' or . = 'A carrier must be on the load')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A carrier must be on the load</value>
   </webElementProperties>
</WebElementEntity>

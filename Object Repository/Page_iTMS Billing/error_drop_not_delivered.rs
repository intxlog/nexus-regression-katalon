<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>error popup for drop not delivered</description>
   <name>error_drop_not_delivered</name>
   <tag></tag>
   <elementGuidId>a965236c-7e7e-4f8e-b070-b27487c7a0da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'A drop is not delivered' or . = 'A drop is not delivered')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A drop is not delivered</value>
   </webElementProperties>
</WebElementEntity>

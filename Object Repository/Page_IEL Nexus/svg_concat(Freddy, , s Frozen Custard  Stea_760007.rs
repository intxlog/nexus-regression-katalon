<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_concat(Freddy, , s Frozen Custard  Stea_760007</name>
   <tag></tag>
   <elementGuidId>dfc90ada-947b-4a1b-bc33-2ff12a409878</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Freddy', &quot;'&quot;, 's Frozen Custard &amp; Steakburgers aka coffee high 8')])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.css-19bqh2r</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>0b0b191e-bfee-4ec4-9ba4-b99889bd9a9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>fe1390cc-2170-43f1-b7ff-d150b5515e12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>186a073f-c020-47de-8a3b-ed96a1b360c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
      <webElementGuid>3a02b39a-1fa8-4370-b1d3-3c38b202756b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d4f6af6b-3c70-4bd9-a472-8842b171325f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>dbef6ea6-c402-4d6d-8e9c-8c3f97cda7a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-19bqh2r</value>
      <webElementGuid>35caf0b4-7a7b-41e0-a84f-e4b3c924f04d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;styles_container__3gKqB&quot;]/main[@class=&quot;styles_pageWrapper__UQUjG&quot;]/div[@class=&quot;styles_content__HBh4y&quot;]/div[@class=&quot;styles_innerContent__3yOBz&quot;]/div[@class=&quot;styles_container__1M3QA&quot;]/div[@class=&quot;styles_headingContainer__1qnAw styles_light__ooL01&quot;]/div[1]/div[1]/div[@class=&quot;styles_dropdownButton__36dF-&quot;]/div[@class=&quot;css-2b097c-container&quot;]/div[@class=&quot;css-6kky7w-control&quot;]/div[@class=&quot;css-1wy0on6&quot;]/div[@class=&quot;css-1gtu0rj-indicatorContainer&quot;]/svg[@class=&quot;css-19bqh2r&quot;]</value>
      <webElementGuid>30aa254f-bc3d-426a-a205-0705d09e512c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Freddy', &quot;'&quot;, 's Frozen Custard &amp; Steakburgers aka coffee high 8')])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>52e7df5c-4d81-4fb3-993a-eface954ab31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billed A/R'])[2]/following::*[name()='svg'][1]</value>
      <webElementGuid>30304e8f-c905-427d-ac70-91033bdb3211</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Freddy', &quot;'&quot;, 's Frozen Custard &amp; Steakburgers aka coffee high 8')])[2]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>9bc33f78-31b1-4300-9ef2-db6b19b08fed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clorox Disinfectant'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>96e7903a-1c92-4a5f-ab56-b3e8bdb01ffc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

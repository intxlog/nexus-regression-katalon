<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Test EDI Customer 0001</name>
   <tag></tag>
   <elementGuidId>f78474ef-08b4-4324-a3d6-6ecafa4cce56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-select-2-option-5']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#react-select-2-option-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1276756d-08f5-4bb5-8077-12ad5d3d5edb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> css-1lkmrtr-option</value>
      <webElementGuid>22e3f445-6e98-47ba-8504-9896236e9e4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-2-option-5</value>
      <webElementGuid>2f83f1bc-dec1-41d7-a138-9d4440935fff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>93346a2b-3922-44d3-b798-977344bbf172</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test EDI Customer 0001</value>
      <webElementGuid>96c05efe-61e7-4ca5-917f-812b5ff877b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-2-option-5&quot;)</value>
      <webElementGuid>ab8d5e6a-b8dc-466b-b1e8-b45944950892</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='react-select-2-option-5']</value>
      <webElementGuid>bb3c7230-bc7c-4484-ab46-3d0657a7c6b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/main/div/div/div/div/div/div/div/div/div[2]/div/div[6]</value>
      <webElementGuid>8e9b0be5-7871-468a-9d37-8331c5c67214</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Anderson Ridge1'])[1]/following::div[1]</value>
      <webElementGuid>8c640667-a6db-44b8-9f33-79c7b04875ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alder Springs Church3'])[1]/following::div[2]</value>
      <webElementGuid>f3fa87e3-14b4-4313-9733-6305f1dbc959</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test EDI Customer 0002'])[1]/preceding::div[1]</value>
      <webElementGuid>1e192a44-c637-4915-b583-00e6227c39f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make a payment'])[1]/preceding::div[2]</value>
      <webElementGuid>723af164-95ab-46d1-9baa-88ab6b836494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Test EDI Customer 0001']/parent::*</value>
      <webElementGuid>d2b298eb-6f1a-4c6c-8318-4db5985a98e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]</value>
      <webElementGuid>580107ef-78c1-42e9-9858-5984be4905e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'react-select-2-option-5' and (text() = 'Test EDI Customer 0001' or . = 'Test EDI Customer 0001')]</value>
      <webElementGuid>28cd9ed7-acbf-496e-ad3b-d47bada4e1ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>last_view_details_btn</name>
   <tag></tag>
   <elementGuidId>5d51897e-bd93-491f-b15a-dc73180f224d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'View Details')])[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles-module__primary___2rSQt styles-module__small___3TpxN</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/load/926950</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>View Details</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;styles_container__kAnOe&quot;]/main[@class=&quot;styles_pageWrapper__37nuH&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__1k855&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__2wAsA&quot;]/div[@class=&quot;styles_content__37-XK styles_dataViewWrapper__3mZRV&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_container__15DCu&quot;]/div[@class=&quot;styles_content__37-XK&quot;]/div[@class=&quot;styles_innerContent__sKtg5&quot;]/div[@class=&quot;styles_contentWrapper__3CDq7&quot;]/div[@class=&quot;styles_sortersContainer__3bDWB&quot;]/div[@class=&quot;styles_listWrapper__29nHZ&quot;]/div[@class=&quot;styles_container__3r0cB&quot;]/div[@class=&quot;styles_cell__36eyB&quot;]/a[@class=&quot;styles-module__primary___2rSQt styles-module__small___3TpxN&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div/div[76]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'View Details')])[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ordered'])[6]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscribe'])[2]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscribe'])[3]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Today', &quot;'&quot;, 's')])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/load/926950')])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[76]/a</value>
   </webElementXpaths>
</WebElementEntity>

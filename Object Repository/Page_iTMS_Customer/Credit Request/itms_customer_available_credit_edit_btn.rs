<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>itms_customer_available_credit_edit_btn</name>
   <tag></tag>
   <elementGuidId>b0d9d6e4-2e24-4b52-b602-c82b719b6e6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'available-credit-text']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>available-credit-text</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>itms_customer_available_credit_save_btn</name>
   <tag></tag>
   <elementGuidId>57082bc5-eccd-4e80-84b4-eec85a0ae796</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'submit-available-credit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>submit-available-credit</value>
   </webElementProperties>
</WebElementEntity>

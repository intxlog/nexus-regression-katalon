<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>itms_customer_available_credit_incease_input</name>
   <tag></tag>
   <elementGuidId>ca5a4bad-b699-40cd-99ec-a603bb28dd5a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'available-credit-decrease']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>available-credit-decrease</value>
   </webElementProperties>
</WebElementEntity>

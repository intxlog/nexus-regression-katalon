<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>green_age_box</name>
   <tag></tag>
   <elementGuidId>2b7cc614-1771-41dc-8a09-516e1f5417f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@style = 'background-color: rgb(220, 57, 18);']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(220, 57, 18);</value>
      <webElementGuid>a89c9c1a-1f90-44d2-bd24-7d15b56440b5</webElementGuid>
   </webElementProperties>
</WebElementEntity>

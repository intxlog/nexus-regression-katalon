<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>blue_age_folder</name>
   <tag></tag>
   <elementGuidId>7b037d40-94a1-415c-982c-c6e1967de8ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@style = 'background-color: rgb(51, 102, 204);']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(51, 102, 204);</value>
      <webElementGuid>04a0a964-9a6e-43d9-992c-dbf294fb01ca</webElementGuid>
   </webElementProperties>
</WebElementEntity>

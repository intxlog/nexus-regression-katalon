<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>orange_age_box</name>
   <tag></tag>
   <elementGuidId>2b31f37c-70d0-40de-950f-8f6c0dcdc752</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@style = 'background-color: rgb(255, 153, 0);']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(255, 153, 0);</value>
      <webElementGuid>6a671730-78bd-4c0c-a35b-e9ccee88e8fb</webElementGuid>
   </webElementProperties>
</WebElementEntity>

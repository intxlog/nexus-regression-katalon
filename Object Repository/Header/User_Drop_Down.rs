<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>The top right, logged-in user drop-down button</description>
   <name>User_Drop_Down</name>
   <tag></tag>
   <elementGuidId>9ec20817-3570-43c9-bd9e-155a7362a06f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'styles_avatar__TEvVo']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styles_avatar__TEvVo</value>
   </webElementProperties>
</WebElementEntity>

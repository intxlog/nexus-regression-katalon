<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Second test suite for itms tests - for breaking up the tetsts to run in parallel.</description>
   <name>Nexus_and_iTMS_2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>530d79d1-0da8-45c3-895e-8670df73eb38</testSuiteGuid>
   <testCaseLink>
      <guid>adcee79f-a28d-4a86-b6e5-d3efbac8378c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Load_Summary/FE2ELoadSummaryBrokerNotes_00001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5ba2799c-56f4-4d10-9aa8-fc1a939ab183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/New_Tests/FE2ECustomerNameDisplay_00001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ee545575-d027-4d17-a262-c14f47898379</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/New_Tests/FE2ECustomerNameDisplay_00002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>66c647e0-5a3d-475e-9fa9-6e466ea83566</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/General/CUPO-FLoadDetailsAddDocuments_000002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>

<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Collection for Billed AR tests and Unbilled Loads tests</description>
   <name>Billed_Unbilled_And_Make_Payment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>87d5e293-b505-4e90-8d41-af8a4c221389</testSuiteGuid>
   <testCaseLink>
      <guid>c34fcc9e-a7cf-45a0-a6c0-3d66c3d718ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Billed_AR/FBilledAR_00002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bde69b7b-79df-418e-9a82-49e0293a2f35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Billed_AR/FBilledAR_00003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fcb3db3a-637a-450c-a96e-ed0d79bfcbbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Billed_AR/FBilledAR_00004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>173b441f-b839-4d19-855d-6b970691d616</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Un_Billed_Loads/FUnbilledLoads_00003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2f4f15c5-e058-40b3-b59d-d0d9c351a859</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Un_Billed_Loads/FUnbilledLoads_00004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>09063d0d-a3fe-4195-ba71-d2a41ce782b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Make_A_Payment/FMakeAPaymentShortPay_00001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e7b3f65a-b785-4000-b80c-4577955c39f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Nexus_Regression/New_Tests/FMakeAPaymentPrepayment_00001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4635a681-39c6-4e21-8245-ae79a08f7e8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/General/FMakeAPaymentCustomerPo_00001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d2ae9ed0-466c-40ed-aa1f-71ecf6d80c4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/General/FMakeAPaymentError_00001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>

<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>test suite for in-sprint automated test cases</description>
   <name>Sprint_Work_Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>1828e5fa-bf41-47d9-afac-2cb427958621</testSuiteGuid>
   <testCaseLink>
      <guid>7d8d4739-753e-4dcf-adf6-4017719e7cc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Sprint_Work/CUPO-451</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a2e36751-f56c-43f4-9a1e-4f8c491afa70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Sprint_Work/CUPO-FLoadSearch_00002_split_load</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cc5dcdc8-f783-43a7-a60a-8f0ac11c9e46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Sprint_Work/RWDY-1827</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>11cc102b-1a69-44c6-b547-88d9826f71bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Nexus_Regression/Sprint_Work/RWDY-1841</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>

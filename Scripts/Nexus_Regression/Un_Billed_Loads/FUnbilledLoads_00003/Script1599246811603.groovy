import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import un_billed_AR.*
import load_details.*
import billed_AR.*

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String itms_po = GlobalVariable.unbilled_itms_po
String url = GlobalVariable.URL

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//go to the UnBilled loads page from the top menu
//and verify that the page appears
Billed_AR_Behaviors ar = new Billed_AR_Behaviors()
ar.click_billing_tools()

Un_Billed_AR_Behaviors unbilled = new Un_Billed_AR_Behaviors()
unbilled.goto_un_billed_loads()

//enter a IEL PO number and apply the filter
ar.filter_by_iel_po(itms_po)

//verify that the same IEL PO is in the filtered section when the filter is applied for it
ar.verify_iel_po_filtered(itms_po)
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.testng.Assert as Assert
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import billed_AR.*
import un_billed_AR.*
import helper_methods.Helper_Methods

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//name of the CSV file
//String file_name = 'Billed A_R export.csv'
String file_name = 'Unbilled loads export.csv'

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//go to the Unbilled Loads section, selected frUn_Billed_AR_Behaviors unbilled = new Un_Billed_AR_Behaviors()
Un_Billed_AR_Behaviors unbilled = new Un_Billed_AR_Behaviors()
unbilled.click_billing_tools()
unbilled.goto_un_billed_loads()

//use methods from the unbilled AR class to download the CSV file
Billed_AR_Behaviors ar = new Billed_AR_Behaviors()
ar.download_csv()

//use the helper method to verify the CSV file download
Helper_Methods file = new Helper_Methods()
boolean file_is_downloaded = file.is_file_downloaded(file_name)

//use testNG Assert imports to verify the file download successful
//assert that the CSV is found in the specified folder
//display the "Failed to download" message if the assertion fails
Assert.assertTrue(file_is_downloaded, "Failed to download the report document")

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import un_billed_AR.*
import load_summary.Load_Summary_Verifications

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select the Unbilled Loads section from the top menu
Un_Billed_AR_Behaviors unbilled = new Un_Billed_AR_Behaviors()
unbilled.click_billing_tools()
unbilled.goto_un_billed_loads()

//verify the header and top elements of the Unbilled AR page
unbilled.unbilled_header_elements_verify()

//TODO -> verify the Information section elements

//verify the Financial Summary section elemets
unbilled.unbilled_financial_summary_elements_verify()

//verify the Filter section elements
unbilled.unbilled_filter_section_elements_verify()

//verify the loads table elements and the Export to CSV button
unbilled.unbilled_loads_table_elements_verify()

//verify the footer section elements (taken from the load summary verifications class)
Load_Summary_Verifications footer = new Load_Summary_Verifications()
footer.footer_verifications()

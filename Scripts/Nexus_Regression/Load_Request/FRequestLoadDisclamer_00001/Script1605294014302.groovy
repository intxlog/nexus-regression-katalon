import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import load_details.*
import load_request.*
import general_misc_tests.Back_Button
import load_summary.Load_Summary_Verifications

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Request - Disclaimer page
Load_Request request = new Load_Request()
request.goto_load_request_disclaimer()

//cancel the load request on the Disclaimer page - click Previous Page button
Back_Button back = new Back_Button()
back.click_previous_page_button()

//re-select the Load Request
request.goto_load_request_disclaimer()
 
//re-select the Previous Page button on the load request on the Disclaimer page
back.click_previous_page_button()

//verify user is on the Load Summary page, and not the Load Request page
Load_Summary_Verifications page = new Load_Summary_Verifications()
page.verify_on_load_summary_page()

//re-select the Load Request
request.goto_load_request_disclaimer()

//continue with the Load Request - Agree to the Disclaimer
request.continue_to_load_request()

//Cancel the Load Request from the Load Request form page
request.cancel_load_request()

//re-select the Load Request
request.goto_load_request_disclaimer()

//continue with the Load Request - Agree to the Disclaimer
request.continue_to_load_request()

//verify the user landed on the Request Load form page
request.verify_load_request_page()


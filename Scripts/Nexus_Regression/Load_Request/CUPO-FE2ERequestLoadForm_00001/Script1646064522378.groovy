import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import load_details.*
import load_request.*
import load_summary.Load_Summary_Verifications
import itms_behaviors.*

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String user_email = GlobalVariable.from_email

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Request - Disclaimer page
Load_Request request = new Load_Request()
request.goto_load_request_disclaimer()
request.continue_to_load_request()
List vals = request.fill_in_load_request_data_random_values()

//submit the request
request.submit_load_request()

//close Nexus
WebUI.closeBrowser()

//Log into iTMS
//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//go to the load requests tb
Loadboard_Behaviors loadboard = new Loadboard_Behaviors()
loadboard.goto_load_requests_tab()

//verify that the values from the Nexus load request show in load board
loadboard.verify_request_values(vals)

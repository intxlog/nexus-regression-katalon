import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import load_details.*
import load_request.*
import general_misc_tests.Back_Button
import load_summary.Load_Summary_Verifications
import itms_behaviors.*
import email_verification.FetchEmailVarLink

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String user_email = GlobalVariable.from_email

//email strings
String subject_string_1 = "RE: New Nexus Order Request:"
String search_string = "submitted an order request on"
//String subject_string_2 = "RE: Voided Nexus Order Request:"

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Request - Disclaimer page
Load_Request request = new Load_Request()
request.goto_load_request_disclaimer()
request.continue_to_load_request()
String po_1 = request.fill_in_load_request_data()

//submit the request
request.submit_load_request()

//go to the request history
request.select_request_history()

//open the pending request
request.open_top_pending_request()

//modify the request and save
String rand_update = request.modify_pending_request()

//email - verify that the updated info is in the email
//FetchEmailVarLink emailobj = new FetchEmailVarLink()
//emailobj.check_email_for_string(user_email, p, subject_string_1, rand_update)

//close Nexus
WebUI.closeBrowser()

//Log into iTMS
//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//go to the load requests tb
Loadboard_Behaviors loadboard = new Loadboard_Behaviors()
loadboard.goto_load_requests_tab()

//accept the request
loadboard.accept_load_request()

WebUI.closeBrowser()

//log in to Nexus
//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
select.select_customer(customer)

//go to request history
request.select_request_history()

//verify that the request was accepted
//request.verify_load_accepted_status_in_request_history(po_1)

//go to reqest load to submit another load
request.goto_load_request_disclaimer()
request.continue_to_load_request()
String po_2 = request.fill_in_load_request_data()
request.submit_load_request()

//go to request history
request.select_request_history()

//sort and open the new request
request.open_top_pending_request()

//click to void the request
request.void_load_request()

//go back to request history
request.select_request_history()

//confirm the new request has a status of Voided
request.verify_load_voided_status_in_request_history(po_2)

WebUI.closeBrowser()

////go back to iTMS
////open browser and navigate to the iTMS log in page
//Open_Nexus.open(itms_url)
//
////log in to iTMS with email from the global variables/default profile
//Nexus_Login.itms_login(itms_email, itms_pass)
//
////go to load requests tab
//loadboard.goto_load_requests_tab()
//
////verify that the user cannot accept or reject the request that was voided
//loadboard.verify_request_is_voided(po_2)

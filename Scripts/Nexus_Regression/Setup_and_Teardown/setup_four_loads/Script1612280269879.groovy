import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import itms_behaviors.*

//global variables:
String pass = GlobalVariable.pass
String email = GlobalVariable.email
String url = GlobalVariable.URL
String to_email = GlobalVariable.email
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String from_email = GlobalVariable.from_email

Open_Nexus.open_itms()
Nexus_Login.itms_login(itms_email, itms_pass)

Compound_Behaviors_ITMS setup = new Compound_Behaviors_ITMS()

//For each of the four statuses, open iTMS, create a load, then update the load to have the different statuses
//Each of the four loads should show up in the four different sections in the Nexus Load Summary page
setup.create_todays_load()
//setup.create_future_load()
//setup.create_in_transit_load()
////returns the IEL PO number
//String iel_po = setup.create_delivered_load()

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import load_details.*
import load_search.Load_Search_Behaviors
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL

String iel_po = GlobalVariable.itms_po_1

//paths for test file
String file_path_1 = RunConfiguration.getProjectDir() + '/test_ex.csv'
String file_path_2 = RunConfiguration.getProjectDir() + '/test_invalid.PNG'
String file_path_3 = RunConfiguration.getProjectDir() + '/test_v.txt'

//file names
String file_name_1 = 'test-ex.csv'
String file_name_2 = 'test_invalid.PNG'
String file_name_3 = 'test_v.txt'

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Search and search for the PO
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)

//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()

//drop a file into the Document Upload section and submit
Documents doc = new Documents()
doc.add_document(file_path_1)

//verify that the uploaded file appears in the Documents section in load details
doc.verify_doc_present_in_nexus(file_name_1)

//try to upload an invalid document type and verify the element is not present in load details
doc.upload_invalid_doc_type(file_path_2, file_name_2)

//WebUI.refresh()

//try to upload a different invalid document type and verify the element is not present in load details
//doc.upload_invalid_doc_type(file_path_3, file_name_3)
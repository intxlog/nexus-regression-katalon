import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import general_misc_tests.*
import load_summary.Load_Summary_Behaviors
import load_details.*
import load_search.Load_Search_Behaviors
import common_behaviors.Select_Customer

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String iel_po = GlobalVariable.itms_po_1
String url = GlobalVariable.URL
String customer = GlobalVariable.customer_2

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer.select_customer(customer)

//select the Make A Payment button
Make_A_Payment_Behaviors pay = new Make_A_Payment_Behaviors()
pay.select_make_a_payment()

//Select the back button
Back_Button back = new Back_Button()
back.click_previous_page_button()

//Verify that the user returns to the Load Summary screen
back.verify_load_summary_page()

//Select a load from load summary
Load_Summary_Behaviors page = new Load_Summary_Behaviors()
page.click_in_transit()

//then select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors ()
details.click_view_details()

//Select the back button
back.click_previous_page_button()

//Verify that the user returns to the Load Summary screen
back.verify_load_summary_page()

//Select Make a Payment button
pay.select_make_a_payment()

//Add payment amounts
pay.select_first_include_payment()
pay.add_first_overpayment()

//Select one of the Customer PO links
pay.select_customer_po()

//Select the back button
back.click_previous_page_button()

//Verify that the user returns to the Make A Payment screen
back.verify_make_a_payment_page()

//Select Load Search button
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
//Enter IEL PO from ITMS from the global profile as the search criteria
search.iel_po_search(iel_po)

//Select the View Details button for one of the listed loads
details.click_view_details()

//Select the back button
back.click_previous_page_button()

//Verify that the user returns to the Load Search screen AND the search criteria is still selected.
search.verify_load_search_page()
search.verify_iel_po_input(iel_po)
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import general_misc_tests.Make_A_Payment_Behaviors
import common_behaviors.Select_Customer

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String customer = GlobalVariable.customer

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//select the Make A Payment button
Make_A_Payment_Behaviors pay = new Make_A_Payment_Behaviors()
pay.select_make_a_payment()

//select the first Include Payment check box
pay.select_first_include_payment()

//change the first amount to be an over payment
pay.add_first_overpayment()

//select the second Include Payment check box
pay.select_second_include_payment()

//change the second amount to be an over payment
pay.add_second_overpayment()

//select the Pay Now button
//pay.click_pay_now()

//verify the error pop up appears with 2 number of invoices that are invalid or missing
//pay.verify_popup_2_errors()
//close the popup
//pay.close_popup()

//enter a reason for over payment in the first reason input
pay.enter_reason_1()

//select the Pay now button
//pay.click_pay_now()

//verify the error pop up appears with 1 number of invoices that are invalid or missing after entering one reason
//pay.verify_popup_1_error()
//close the popup
//pay.close_popup()

//enter a reason in the second reason text input
pay.enter_reason_2()

//select the Pay now button
pay.click_pay_now()

//verify that the error popup does not appear
pay.verify_error_popup_not_present()
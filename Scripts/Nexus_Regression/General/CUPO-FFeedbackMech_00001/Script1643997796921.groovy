import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import common_behaviors.Feedback_Mech
import database_methods.DB

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL
//def data = findTestData("Data Files/db_data")
//String p = data.getValue(1, 2)
String column_name = "comment"

DB db = new DB()
Feedback_Mech feedback = new Feedback_Mech()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//click on the right side button for "Leave feedback!"
feedback.click_feedback_mech()

//enter randomized feedback and save it
String feedback_str = feedback.enter_feedback()
WebUI.delay(6)

//make a query string with the randomized feedback - if there is a valid result set, it will pass
String query_string = /SELECT * FROM feedback WHERE comment = '/ + feedback_str + /'/

//connect to db
db.connect_db()

//check for randomized feedback string
String comment_from_db = db.get_result_string(query_string, column_name)

//verify that the retrieved databse value matches the randomized value
WebUI.verifyMatch(feedback_str, comment_from_db, false)

//close db
db.close_connection()

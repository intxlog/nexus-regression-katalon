import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import load_details.*
import itms_behaviors.*
import load_search.Load_Search_Behaviors

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
//path for test file
String file_path = RunConfiguration.getProjectDir() + '/test_ex.csv'
String file_path_2 = RunConfiguration.getProjectDir() + '/test.docx'

//string for the name of the file that is uploaded
String file_name = 'test-ex.csv'
String file_name_2 = 'test.docx'

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po = load.get_iel_po()

WebUI.closeBrowser()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)

//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()

//drop a file into the Document Upload section and submit
Documents doc = new Documents()
doc.add_document(file_path)

WebUI.closeBrowser()

//go back to iTMS
//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//search for the load and open in load view
load.itms_load_search(iel_po)

//open the iDocs window
Idocs idocs = new Idocs()
idocs.open_idocs()

//verify that the uploaded document from Nexus is present in iDocs
idocs.verify_document_from_nexus(file_name)

//verify the the user cannot modify or delete the file
//idocs.verify_doc_delete_not_allowed()

//add another document in iDocs, then move it to the the Nexus Documents folder
idocs.drop_file_into_idocs(file_path_2)
idocs.drag_unfiled_file_into_nexus_folder(file_name_2)

//WebUI.closeBrowser()
//
////go back to Nexus
////open Nexus
//Open_Nexus.open(url)
////log in with email from the global variables/default profile
//Nexus_Login.login(email, pass)
//
////select customer
//select.select_customer(customer)
//
////search for the same load again
//search.click_load_search()
//search.iel_po_search(iel_po)
//
////select View Details
//details.click_view_details()
//
////verify that the document added iTMS is present in Nexus
//doc.verify_itms_doc_present_in_nexus(file_name_2)

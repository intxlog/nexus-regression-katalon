import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import general_misc_tests.Make_A_Payment_Behaviors
import login_page.*

//global variables:
//String email = GlobalVariable.email
//String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//open Nexus
Open_Nexus.open(url)

//verify elements above the log in box
Login_Page_Verifications login = new Login_Page_Verifications()
login.verify_above_login_box_elements()

//verify the elements inside the log in box
login.verify_inside_login_box_elements()

//verify the log in page footer elments
login.verify_login_footer_elements()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import general_misc_tests.Footer_Behaviors

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String iel_po = GlobalVariable.itms_po_1
String url = GlobalVariable.URL
String version_url = GlobalVariable.version_url

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//click the Facebook link and verify the page opens
Footer_Behaviors footer = new Footer_Behaviors()
footer.click_facebook()

//click and verify the Twitter link
footer.twitter()

//verify the support link has the correct email
footer.verify_email_link()

//got to the privacy policy page and confirm
footer.privacy_policy()

//click the Terms of Use link and verify the link
footer.terms_of_use()

//click and verify the version link and page with either dev or qa passed as global variables
footer.version(version_url)
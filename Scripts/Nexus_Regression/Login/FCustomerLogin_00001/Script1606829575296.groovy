import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import login_page.Login_Behaviors
import load_summary.Load_Summary_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String customer_name = GlobalVariable.customer_2
String iel_po = GlobalVariable.itms_po_1

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//enter an invalid format email and verify the "invalid format" red validation message
Login_Behaviors login = new Login_Behaviors()
login.invalid_email_format()

//try to sign in with and verify that the user is still on the login page
login.sign_in_unsuccessful_verify()

//enter a valid email, which is not a registered user
login.random_email()

//enter the valid testing email with a incorrect password
login.valid_email_wrong_pass_multiple_times(email)

//wait at least 60 seconds seconds for the password to be reset
WebUI.delay(75)

//enter the valid testing email with the valid password and log in
Nexus_Login.login(email, pass)

//verify the user is on the Load Summary page
Load_Summary_Verifications user = new Load_Summary_Verifications()
user.verify_on_load_summary_page()

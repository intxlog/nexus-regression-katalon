import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import login_page.*
import common_behaviors.Open_Nexus
import email_verification.FetchEmailVarLink

//Verify "Forgot Password" feature only responds to existing, enabled portal accounts in Nexus

String email = GlobalVariable.email
String inactive_email = GlobalVariable.inactive_email
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//click Forgot Password link and verify the Reset Password text
Password_Reset.click_forgot_pass()

//enter an invalid email format
//verify the invalid format validation error
Password_Reset.invalid_email_format()

//refresh and click forgot email again
WebUI.refresh()
Password_Reset.click_forgot_pass()

//enter a random email
//verify the valid user not found error message
Password_Reset.random_email()

//enter an inactive and existing portal account email
//verify the valid user not found message
Password_Reset.inactive_email(inactive_email)
//WebUI.refresh()

//enter an active and existing account
//verify the message for the reset password email is sent
//enter qa testing email and click button for the reset request
Password_Reset.enter_email_and_reset(email)
Password_Reset.verify_email_sent_page()

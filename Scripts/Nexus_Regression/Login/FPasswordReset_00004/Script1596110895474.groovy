import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import login_page.*
import database_methods.*
import email_verification.*
import email_verification.FetchEmailVarLink

//global variable for desired testing email and password
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//variables for email parameters
String username = "qatesting@intxlog.com"
//get data from data file for connection parameters
def data = findTestData("Data Files/db_data")
String p = data.getValue(2, 4)

//SQL query string to select the ID
String query_get_id = "SELECT id, concat(firstname, ' ' ,lastname) as name, email, username, authcodeexpiredt FROM users WHERE email ilike '%qatesting%' AND username is null"
//partial SQL query to update the database to make the reset password to expired ** when ID is added to it
String query_update = "UPDATE users SET authcodeexpiredt = current_timestamp - interval '29 hours' WHERE id = "

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//click Forgot Password link and verify the Reset Password text
Password_Reset.click_forgot_pass()

//enter an active and existing account
//enter testing email and click button for the reset request
Password_Reset.enter_email_and_reset(email)
Password_Reset.verify_email_sent_page()

//database steps to update the request link to be expired
//connect tp the database and get ID, then pass the ID to the update query string
DB db = new DB()
db.connect_db()
//get ID from database
int id = db.get_result(query_get_id)
//pass ID to the execute update query
db.execute_update(query_update, id)
//db_execute.execute_with_int(id)
db.close_connection()

//delay for email to be sent
WebUI.delay(2)
//get the password reset URL from the email
FetchEmailVarLink email_verify = new FetchEmailVarLink()
String email_url = email_verify.get_reset_url_string(username, p)

//close the first browser instance, so it's quicker opening the change password page
WebUI.closeBrowser()

//go to the URL found in the email from above
Open_Nexus.open_pass_reset_url(email_url)

//verify the elements are present on the opened password reset page
Password_Reset.verify_pass_reset_elements()

//key in and try to submit valid password
Password_Reset.enter_valid_password_reset(pass)

//verify the password reset is expired error message
Password_Reset.verify_password_link_expired()

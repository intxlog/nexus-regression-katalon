import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import email_verification.*
import login_page.*
//import database_methods.db_execute

String email = GlobalVariable.email
String url_nexus = GlobalVariable.URL
String pass = GlobalVariable.pass
//variables for email parameters
String host = "outlook.office365.com"
String mailStoreType = "imap"
String reset_subject = "Integrity Express Logistics Password Reset performed!"
String reset_body_snippet = "Password Reset Complete"

//String username = "qatesting@intxlog.com"
def data = findTestData("Data Files/db_data")
String p = data.getValue(1, 2)
//println p

//open browser and navigate to Nexus log in page
Open_Nexus.open(url_nexus)

//click Forgot Password link and verify the Reset Password text
Password_Reset.click_forgot_pass()

//enter an active and existing account
//enter qa testing email and click button for the reset request
Password_Reset.enter_email_and_reset(email)
Password_Reset.verify_email_sent_page()

//delay for email to be sent
WebUI.delay(9)
//get the password reset URL from the email
FetchEmailVarLink email_verify = new FetchEmailVarLink()
String url = email_verify.get_reset_url_string(email, p)

//close the first browser instance, so it's quicker opening the change password page
WebUI.closeBrowser()

//go to the URL found in the email above
Open_Nexus.open_pass_reset_url(url)

//verify the elements are present on the opened password reset page
Password_Reset.verify_pass_reset_elements()

//enter invalid passes, and verify the validation messages appear, then disappear with a valid pass
Password_Reset.password_tries()

//enter valid password
Password_Reset.enter_valid_password_reset(pass)

//logout of Nexus
Nexus_Login.logout()

//log into Nexus
Nexus_Login.login(email, pass)

WebUI.delay(8)

//search for the email with the Password Reset Performed text
email_verify.check_email_for_string(email, pass, reset_subject, reset_body_snippet)

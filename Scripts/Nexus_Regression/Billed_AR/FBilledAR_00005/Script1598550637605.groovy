import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.testng.Assert as Assert
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Select_Customer
import load_details.*
import billed_AR.*
import helper_methods.Helper_Methods

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL

//TODO -> keep the file name in the method, so that I can use a REGEX
//name of the CSV file
String file_name = 'Billed A_R export.csv'

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Billed A/R page from the top menu
//and verify that the page appears
Billed_AR_Behaviors ar = new Billed_AR_Behaviors()
//ar.click_billing_tools()
ar.goto_billed_ar()

//select the link to download the csv file
ar.download_csv()

//use the helper method to verify the download of the CSV file
//verify that the correct CSV was downloaded and saved into the downloads folder
Helper_Methods file = new Helper_Methods()
boolean file_is_downloaded = file.is_file_downloaded(file_name)

//use testNG Assert imports to verify the file download successful
//assert that the CSV is found in the specified folder
//display the "Failed to download" message if the assertion fails
Assert.assertTrue(file_is_downloaded, "Failed to download the report document")


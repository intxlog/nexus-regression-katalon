import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import common_behaviors.Footer_Verifications
import load_search.Load_Search_Behaviors
import itms_behaviors.*
import load_details.*
import load_summary.*
import common_behaviors.Select_Customer

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String customer = GlobalVariable.customer_2

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select a customer which has at least one load
Select_Customer.select_customer(customer)

//expand In Transit section from the Load Summary page that has at least one load in it
Load_Summary_Behaviors.click_in_transit()
//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()
//Verify the various elements in the different sections of the Load Details page//

//verify the elements in the "Details" section at the top of the page
Load_Details_Verifications verify = new Load_Details_Verifications()
verify.details_section_verify()

//verify that the Google maps element appears
verify.google_map_verify()

//verify elements in the "Accessorial Charges" section and "Billing"
verify.accessorial_and_billing_verify()

//verify the elements in the "Check Calls" section
//verify.check_calls_details_verify()

//verify the elements in the "Origin" and "Destination" sections
verify.origin_and_destination_verify()

//verify the footer elements
Footer_Verifications footer = new Footer_Verifications()

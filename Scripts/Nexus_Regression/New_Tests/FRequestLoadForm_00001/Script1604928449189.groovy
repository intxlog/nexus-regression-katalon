import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import load_details.*
import billed_AR.*
import load_request.*
import common_behaviors.Select_Customer
import load_summary.Load_Summary_Verifications

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//go to the Load Request Disclaimer page
Load_Request request = new Load_Request()
request.goto_load_request_disclaimer()

//select wanted customer from the Discalimer page
//Select_Customer select = new Select_Customer()
//select.select_customer_disclaimer(customer)

//continue from the Disclaimer page to the Load Request page
request.continue_to_load_request()

//verify the user is on the Load Request page
request.verify_load_request_page()

//try to submit the form with no data entered
request.submit_load_request_no_input_values()

//verify the user is not brought to the next page
request.verify_load_request_page()

//verify the Required text in red
request.verify_load_request_required_validations()

//fill all fields with data and submit
request.fill_in_load_request_data()

//try to submit the form with All Required data entered
request.submit_load_request()

//verify success message
//request.verify_load_request_success_msg()

//verify that the user is brought back to the Load Summary page
Load_Summary_Verifications load_summary = new Load_Summary_Verifications()
load_summary.verify_on_load_summary_page()

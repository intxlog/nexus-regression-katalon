import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import common_behaviors.Select_Customer
import common_behaviors.View_Details
import load_summary.Load_Summary_Behaviors
import load_details.Load_Details_Behaviors
import common_behaviors.Customer_Name
import load_search.Load_Search_Behaviors
import general_misc_tests.Make_A_Payment_Behaviors
import billed_AR.*
import un_billed_AR.*

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String customer_name = GlobalVariable.customer_2
String iel_po = GlobalVariable.itms_po_1

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select a customer which has at least one load; do nothing if there is only one customer
Select_Customer.select_customer(customer_name)

//expand one of the sections (In Transit) from the Load Summary page that has at least one load in it
Load_Summary_Behaviors.click_in_transit()

//verify that the correct customer name appears on the In Transit info page
Customer_Name customer = new Customer_Name()
//customer.verify_load_section_customer(customer)
customer.verify_customer_selection(customer_name)

//verify that the customer name appears on the Load Search page
customer.verify_customer_selection(customer_name)

//open View Details page for a load after searching for one
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)
//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()

//verify that the customer name appears on the Load Details page
//customer.verify_customer_load_details(customer_name)
customer.verify_customer_selection(customer_name)

//select the Billed A/R link
Billed_AR_Behaviors ar = new Billed_AR_Behaviors()
//first click of billing tools closes the drop-down
ar.click_billing_tools()
ar.goto_billed_ar()

//verify that the customer name appears in the drop down in the Billed A/R page
customer.verify_customer_selection(customer_name)

//verify that the customer name appears Customer Contact info section of the Billed A/R page (not the drop-down element)
customer.verify_customer_ar(customer_name)

//select the UnBilled Loads page
Un_Billed_AR_Behaviors unbilled = new Un_Billed_AR_Behaviors()
unbilled.click_billing_tools()
//need to click the billing tools again to open the drop-down
//unbilled.click_billing_tools()
WebUI.delay(1)
unbilled.goto_un_billed_loads()

//verify that the customer name appears in the drop down in the UnBilled Loads page
customer.verify_customer_selection(customer_name)

//verify that the customer name appears Customer Contact info section of the UnBilled loads page (not the drop-down)
customer.verify_customer_ar(customer_name)

//select the Make a Payment link
Make_A_Payment_Behaviors pay = new Make_A_Payment_Behaviors()
pay.select_make_a_payment()

//verify that the customer name appears in the drop down on the Make a Payment page
customer.verify_customer_selection(customer_name)

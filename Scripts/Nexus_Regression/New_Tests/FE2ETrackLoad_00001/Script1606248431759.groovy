import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.*
import load_search.Load_Search_Behaviors
import itms_behaviors.*
import load_details.*
import email_verification.FetchEmailVarLink

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc
//variables for email
def data = findTestData("Data Files/db_data")
String p = data.getValue(1, 2)
//email search strings
String subject_checkin = "Pickup Check-In at Pickup"
String subject_loaded = "Loaded at Pickup"
String subject_delivered = "Drop Delivered"

//variables for different sheds and consignees
String shed_1 = "Abiding Savior Lutheran Church"
String consignee_1 = "Abells Wharf"
String shed_2 = "A A Hopper Pond Dam"
String consignee_2 = "Abbey Point"

///iTMS behaviors///

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po = load.get_iel_po()
//open the top load from the future loads
load.open_future_load()

//add two picks and two drops and carrier info
Loadview_Behaviors load_update = new Loadview_Behaviors()
load_update.add_pickup(shed_1)
//add new pickup line
load_update.new_pickup()
//add a second shed with info
load_update.add_second_pickup(shed_2)

//add first consignee
load_update.add_drop(consignee_1)
//add new drop line
load_update.new_drop()
//add a second consignee with info
load_update.add_second_drop(consignee_2)
//add carrier and info
load_update.add_carrier(carrier)

WebUI.refresh()

//close the browser to close iTMS
WebUI.closeBrowser()

///NEXUS behaviors///

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select the wanted customer
Select_Customer.select_customer(customer)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)

//Recieve Updates for the searched load
//select Subscribe, after searching for the load, and confirm the tracking subscription
search.select_subscribe()

///iTMS behaviors///

//close the browser and then log back into itms
WebUI.closeBrowser()
Open_Nexus.open(itms_url)
Nexus_Login.itms_login(itms_email, itms_pass)

//search for the same load as before and open it
load.itms_load_search(iel_po)

//////make the check calls externally facing
//dispatch the load and then fill in the check call info
load_update.dispatch_load()
//load_update.create_check_call()

//add status for pick Checked In, and add check call info
load_update.pick_checked_in()
//load_update.create_check_call()

//delay for email to be sent
WebUI.delay(25)
//search for and verify the status emails in the testing email inbox
FetchEmailVarLink email_search = new FetchEmailVarLink()
//search for the pickup Checked-In subject line, and the saved IEL PO number from before
email_search.check_email_for_string(email, p, subject_checkin, iel_po)

//add status for pick Loaded, and add check call info
load_update.pick_loaded()
//load_update.create_check_call()

//delay for email to be sent - sometimes it takes more time
WebUI.delay(25)

//search for the pickup Loaded subject line, and the saved IEL PO number from before
email_search.check_email_for_string(email, p, subject_loaded, iel_po)

//select Checked-In for the second pickup
load_update.second_pick_checked_in()

//select Loaded for the second pickup
load_update.second_pick_loaded()

//delay for email to be sent
WebUI.delay(25)
//search for the pickup Checked-In subject line, and the saved IEL PO number from before
email_search.check_email_for_string(email, p, subject_checkin, iel_po)

//check-in drop 1
load_update.drop_checked_in()
//select Delivered for first drop
load_update.drop_delivered()

//delay for email to be sent
WebUI.delay(25)
//search for the pickup Delivered subject line, and the saved IEL PO number from before
email_search.check_email_for_string(email, p, subject_delivered, iel_po)



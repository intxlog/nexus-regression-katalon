import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import general_misc_tests.Make_A_Payment_Behaviors
import load_summary.Load_Summary_Verifications
import general_misc_tests.Make_A_Payment_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String po = GlobalVariable.nexus_po_1
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select Make a Payment
Make_A_Payment_Behaviors pay = new Make_A_Payment_Behaviors()
pay.select_make_a_payment()

//select Prepay Load
pay.select_prepay_load()

//verify that the Prepay Load popup appears
pay.verify_prepay_popup()

//enter values into the Prepay popup - passing to it, the global nexus po variable, and Submit it
pay.enter_prepay_values(po)

//verify that the user is taken to a Payeezy page - verify the footer text for Payeezy
pay.verify_payeezy_text()

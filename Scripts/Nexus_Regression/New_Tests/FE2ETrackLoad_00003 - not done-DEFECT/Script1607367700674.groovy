import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.*
import load_search.Load_Search_Behaviors
import itms_behaviors.*
import load_details.*
import email_verification.FetchEmailVarLink

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc
//variables for email parameters
String host = "outlook.office365.com"
String mailStoreType = "imap"
def data = findTestData("Data Files/db_data")
String p = data.getValue(1, 2)
//email search strings
String subject_checkin = "Pickup Check-In at Pickup"
String subject_loaded = "Loaded at Pickup"

//variables for different sheds and consignees
String shed_1 = "Abiding Savior Lutheran Church"
String conignee_1 = "Abells Wharf"

//open browser and navigate to the iTMS log in page
Open_Nexus.open_itms()

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po = load.get_iel_po()
//open the top load from the future loads
load.open_future_load()

//add pick and drop and carrier info
Loadview_Behaviors load_update = new Loadview_Behaviors()
load_update.add_pickup(shed_1)
load_update.add_drop(conignee_1)
load_update.add_carrier(carrier)

WebUI.refresh()
//close the browser to close iTMS
WebUI.closeBrowser()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)

//select Subscribe, after searching for the load, and confirm the tracking subscription
search.select_subscribe()

//close the browser and then log back into itms
WebUI.closeBrowser()
Open_Nexus.open_itms()
Nexus_Login.itms_login(itms_email, itms_pass)

//search for the same load as before and open it
load.itms_load_search(iel_po)

//////make the check calls externally facing
//dispatch the load and then fill in the check call info
load_update.dispatch_load()
load_update.create_check_call()

//add status for pick Checked In, and add check call info
load_update.pick_checked_in()
load_update.create_check_call()

//add status for pick Loaded, and add check call info
load_update.pick_loaded()
load_update.create_check_call()

//search for and verify the status emails in the testing email inbox
FetchEmailVarLink email_search = new FetchEmailVarLink()
//search for the pickup Checked-In subject line, and the saved IEL PO number from before
email_search.check_email_for_string(email, p, subject_checkin, iel_po)
//search for the pickup Loaded subject line, and the saved IEL PO number from before
email_search.check_email_for_string(email, p, subject_loaded, iel_po)

////DEFECT: https://intxlog.atlassian.net/browse/CUPO-430
// TODO when fixed:
//go to the Unsubcribe URL from the email

//update load in itms

//verify that no status email is received


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.testng.Assert as Assert
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import billed_AR.*
import un_billed_AR.*
import helper_methods.Helper_Methods
import general_misc_tests.Payment_History

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//name of the CSV file
String file_name = 'Pay_History.csv'

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select the user login avatar at the top right, expand the drop-down, and select Pay History
Payment_History pay = new Payment_History()
pay.select_pay_history()

//click the Export CSV button
pay.export_pay_history()

//use the helper method to check if the file was downloaded
Helper_Methods file = new Helper_Methods()
boolean file_is_downloaded = file.is_file_downloaded(file_name)

//use testNG Assert imports to verify the file download successful
//assert that the CSV is found in the specified folder
//display the "Failed to download" message if the assertion fails
Assert.assertTrue(file_is_downloaded, "Failed to download the pay history document")
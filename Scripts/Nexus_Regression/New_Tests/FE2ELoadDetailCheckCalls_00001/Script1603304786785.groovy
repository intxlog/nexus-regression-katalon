import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import helper_methods.Helper_Methods
import common_behaviors.*
import load_search.Load_Search_Behaviors
import itms_behaviors.*
import load_details.*
import email_verification.FetchEmailVarLink

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc

//variables for shed and consignee
String shed_1 = "Abiding Savior Lutheran Church"
String consignee_1 = "Abells Wharf"

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po = load.get_iel_po()
//open the top load from the future loads
load.open_future_load()

//add picks and drop and carrier info
Loadview_Behaviors load_update = new Loadview_Behaviors()
load_update.add_pickup(shed_1)
load_update.add_drop(consignee_1)
load_update.add_carrier(carrier)

//dispatch the load
load_update.dispatch_load()
//fill out the Dispatch check call
//load_update.create_check_call()

//get the current time and date of the dispatch
Helper_Methods time = new Helper_Methods()
String current_time = time.get_time()
String date = time.today_date_long()

println date
println current_time

WebUI.refresh()

//close the browser to close iTMS
WebUI.closeBrowser()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select a customer which has at least one load
Select_Customer.select_customer(customer)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)

//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()

//verify that the time and date match the created load from before
load_update.verify_load_details_checkcall_data(date)


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import general_misc_tests.Make_A_Payment_Behaviors
import load_summary.Load_Summary_Verifications
import general_misc_tests.Make_A_Payment_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select Make a Payment
Make_A_Payment_Behaviors pay = new Make_A_Payment_Behaviors()
pay.select_make_a_payment()

//verify the top button elements, invoices, and other elements
Make_A_Payment_Verifications verify = new Make_A_Payment_Verifications()
verify.top_elements_verify()

//verify the top invoice column elements
verify.payment_columns_verify()

//verify the footer section elements
Load_Summary_Verifications footer = new Load_Summary_Verifications()
footer.footer_verifications()

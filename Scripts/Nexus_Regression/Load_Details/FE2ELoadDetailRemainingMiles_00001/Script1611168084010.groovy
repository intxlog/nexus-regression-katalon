import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import load_details.Load_Details_Verifications
import load_search.Load_Search_Behaviors
import common_behaviors.Select_Customer
import itms_behaviors.*
import load_details.*

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc

//variables for different sheds and consignees
String shed_1 = "Abiding Savior Lutheran Church"
String conignee_1 = "Abells Wharf"

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

////create a new load for the Nexus customer in iTMS
//Create_Load new_load = new Create_Load()
//new_load.create_new_load(customer)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
//create a load using the IEL tester toolkit with 2 picks and 2 drops
List load_info_list = new_load.api_create_load_list_2_picks_drops()
//second list index returned from the ceate laod API is the load PO
String iel_po = load_info_list[1]

//
////iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
////go to the future loads tab
//load.goto_future_loads_tab()
////save the PO for use in Nexus
//String iel_po = load.get_iel_po()
////open the top load from the future loads
//load.open_future_load()
//
////add pickup and drop, and a carrier to the load
Loadview_Behaviors load_update = new Loadview_Behaviors()

//search for and open previous load
load.itms_load_search(iel_po)
WebUI.delay(1)

////pickups
//load_update.add_pickup(shed_1)
////drops
//load_update.add_drop(conignee_1)
////carrier
//load_update.add_carrier(carrier)
//
////move load through to delivered
//load_update.add_pickup_date()
//load_update.add_drop_date()
//load_update.dispatch_load()
//load_update.pick_checked_in()
//load_update.pick_loaded()
//load_update.drop_checked_in()
//load_update.drop_delivered()
//load_update.add_carrier_rating()

//Trip miles and get the value and save it
String miles = load_update.trip_miles()

WebUI.refresh()

WebUI.closeBrowser()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
Load_Search_Behaviors search = new Load_Search_Behaviors()
search.click_load_search()
search.iel_po_search(iel_po)

//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()

//verify for the delivered load, that there is no remaining miles text or value
//verify that the details shows "load delivered" text
Load_Details_Verifications verify = new Load_Details_Verifications()
verify.verify_details_delivered_text()

WebUI.closeBrowser()

/////itms Section 2: add new load, pick and drop, but don't dispatch the load//////

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
new_load.create_new_load(customer)

//iTMS Load board behaviors
//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po2 = load.get_iel_po()
//open the top load from the future loads
load.open_future_load()

//add pickup and drop, and a carrier to the load
//pickups
load_update.add_pickup(shed_1)
//drops
load_update.add_drop(conignee_1)
//carrier
load_update.add_carrier(carrier)

//move load through to delivered
load_update.add_pickup_date()
load_update.add_drop_date()

//Trip miles and get the value and save it
String miles2 = load_update.trip_miles()

WebUI.refresh()

WebUI.closeBrowser()

////Nexus section 2: verify the text for remaining miles is present when the load is not dispatched

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
select.select_customer(customer)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
search.click_load_search()
search.iel_po_search(iel_po2)

//select View Details
details.click_view_details()

//verify for the delivered load, that there is no remaining miles text or value
//verify that the details shows "load delivered" text
verify.verify_estimated_remaining_miles_text()

//verify estimated remaining miles VALUE
verify.verify_remaining_miles_value(miles2)

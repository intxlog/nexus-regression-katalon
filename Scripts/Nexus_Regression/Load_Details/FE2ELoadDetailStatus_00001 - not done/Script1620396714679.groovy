import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import load_search.Load_Search_Behaviors
import load_search.Load_Search_Statuses
import itms_behaviors.*
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import load_details.*
import common_behaviors.Select_Customer

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc

/* In order to have two browser instances open at the same time, (one for Nexus and one for iTMS),
 * need to use two specified chrome drivers using selenium/webdriver.
 * Use DriverFactory.changeWebDriver(driver) to switch between the two browsers.
 * */
//find the path to the chrome driver on the machine directory
//System.setProperty("webdriver.chrome.driver", DriverFactory.getChromeDriverPath())
//first web driver -> iTMS
//WebDriver driver_itms = new ChromeDriver()
//second web driver -> Nexus
//WebDriver driver_nexus = new ChromeDriver()

//start with the iTMS driver
//DriverFactory.changeWebDriver(driver_itms)

//open browser and navigate to the iTMS log in page
//driver_itms.get("https://qa.intxlog.com/")

Open_Nexus.open(itms_url)
//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

////In iTMS, create and setup different loads with various statuses////

Create_Load new_load = new Create_Load()
//create a load with Not Dispatched status
new_load.create_new_load(customer)
//iTMS Load board behaviors

Loadboard_Behaviors load = new Loadboard_Behaviors()
//save the PO from lodaboard for use in Nexus
String iel_po_1 = load.get_iel_po()
//go to the future loads tab
load.goto_future_loads_tab()
//open the load
load.open_future_load()

//iel_po_1 load should be Ordered and have a yellow color

//close the load
Loadview_Behaviors view = new Loadview_Behaviors()
view.close_load()

//compound behaviors to setup the various loads with statuses
Compound_Behaviors_ITMS setup = new Compound_Behaviors_ITMS()

//Create load and update it to have the Dispatched status
String iel_po_2 = setup.create_dispatched_load()
//iel_po_2 load should be Dispatched and have a teal color

//Create load and update it to have the Pick Checked-In status
String iel_po_3 = setup.create_pick_checkedin_load()
//iel_po_3 load should be Checked-In and have a Blue color

//create load and update it to have the Loaded status
String iel_po_4 = setup.create_pick_loaded_load()
//iel_po_4 load should be Loaded and have a Purple color

//create load and update it to have the Drop Checked-in status
String iel_po_5 = setup.create_drop_checkedin_load()
//iel_po_5 load should be Checked-In and have a Blue color

//create load and update it to have the Delivered status
String iel_po_6 = setup.create_delivered_load()
//iel_po_6 load should be Delivered and have a blue color

WebUI.closeBrowser()

////Nexus Load Search status verifications////
Load_Search_Behaviors search = new Load_Search_Behaviors()
Load_Search_Statuses status = new Load_Search_Statuses()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer select = new Select_Customer()
select.select_customer(customer)

//go to the Load Search and search for the PO from iTMS in the IEL PO search box
search.click_load_search()

//search for the load and verify the Ordered status in Yellow
search.iel_po_search(iel_po_1)
status.verify_ordered_status()
WebUI.refresh()

//verify the Dispatched status in Teal
search.iel_po_search(iel_po_2)
status.verify_dispatched_status()
WebUI.refresh()

//verify the pick Checked In status in Blue
search.iel_po_search(iel_po_3)
status.verify_pick_checkin_status()
WebUI.refresh()

//verify the Loaded status in Purple
search.iel_po_search(iel_po_4)
status.verify_loaded_status()
WebUI.refresh()

//verify the Drop checked in status in Blue
search.iel_po_search(iel_po_5)
status.verify_drop_checkin_status()
WebUI.refresh()

//verify the Delivered status in Green
search.iel_po_search(iel_po_6)
status.verify_delivered_status()


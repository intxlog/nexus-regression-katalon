import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import common_behaviors.Select_Customer
import load_summary.Load_Summary_Behaviors
import load_summary.Load_Summary_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String customer = GlobalVariable.customer_2

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select a customer
Select_Customer.select_customer(customer)

//verify the common elements in the Load Summary section
Load_Summary_Verifications.load_summary_elements_verify()

//click the Today's Loads section and verify the elements present in that section
Load_Summary_Behaviors.click_today_loads()
Load_Summary_Verifications.verify_load_section_headers()
//back to the summary page
WebUI.back()

//click the In Transit section and verify elements present in that section
Load_Summary_Behaviors.click_in_transit()
Load_Summary_Verifications.verify_load_section_headers()
WebUI.back()

//click the Delivered Loads section and verify the elements present in that section
Load_Summary_Behaviors.click_delivered_loads()
//verify Delivered text font weight
//Load_Summary_Verifications.delivered_font_verification()
Load_Summary_Verifications.verify_load_section_headers()
WebUI.back()

//click the Future Loads section and verify the elements present in that section
Load_Summary_Behaviors.click_future_loads()
Load_Summary_Verifications.verify_load_section_headers()
WebUI.back()

//verify footer elements
Load_Summary_Verifications.footer_verifications()

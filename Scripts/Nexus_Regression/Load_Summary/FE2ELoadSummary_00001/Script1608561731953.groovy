import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import common_behaviors.Select_Customer
import common_behaviors.View_Details
import load_summary.Load_Summary_Behaviors
import load_details.Load_Details_Behaviors
import itms_behaviors.*

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc
//variables for different sheds and consignees
String shed_1 = "Abiding Savior Lutheran Church"
String conignee_1 = "Abells Wharf"

//open browser and navigate to the iTMS log in page
//Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
//Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
//new_load.create_new_load(customer)

//create a load using the IEL tester toolkit with the "rtb" load condition; get the load ID and PO in a list
List load_info_list = new_load.api_create_load_list()

//second list index returned from the ceate laod API is the load PO
String iel_po = load_info_list[1]
println "po is " + iel_po

//iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()

//search for the return IEL PO number
//load.itms_load_search(iel_po)

//go to the future loads tab
//load.goto_future_loads_tab()
//save the PO for use in Nexus
//String iel_po = load.get_iel_po()
//open the top load from the future loads
//load.open_future_load()

//add 2 different pickups and 2 drops, and a carrier to the load
Loadview_Behaviors load_update = new Loadview_Behaviors()
//pickups
//load_update.add_pickup(shed_1)
//drops
//load_update.add_drop(conignee_1)
//add carrier
//load_update.add_carrier(carrier)

///load update statuses////

//dispatch the load
//load_update.dispatch_load()
//fill out the Dispatch check call
//load_update.create_check_call()

//pickup loaded
//load_update.pick_checked_in()

//pickup loaded
//load_update.pick_loaded()

//drop checked in
//load_update.drop_checked_in()

//drop delivered
//load_update.drop_delivered()

//must add review section
//load_update.add_carrier_rating()

//WebUI.refresh()

//close the browser to close iTMS
//WebUI.closeBrowser()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer.select_customer(customer)

//get the number displayed in the corner for Delivered loads total on the Load Summary page
String del_amnt_1 = Load_Summary_Behaviors.get_delivered_loads_amount()
//** convert the String to integer
int del_amnt_int_1 = del_amnt_1 as Integer

WebUI.closeBrowser()

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//WebUI.closeWindowIndex(1)

//WebUI.switchToWindowIndex(0)

//search for and open previous load
load.itms_load_search(iel_po)

//WebUI.switchToWindowIndex(1)

WebUI.delay(1)
//change the Delivered date to 4 days ago
load_update.add_past_delivery_date()
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/Other_Common_Elements/OK_capital_btn'))
load_update.drop_delivered()

//click OK for error pop up
WebUI.delay(2)
//WebUI.click(findTestObject('Object Repository/Other_Common_Elements/OK_capital_btn'))

//load_update.add_past_delivery_date()

///try mousover after the error popup shows
WebUI.mouseOver(findTestObject('Object Repository/Page_iTMS Loadview/Drops/drop_mouse_over'))

WebUI.delay(2)

WebUI.refresh()

//WebUI.delay(6)

WebUI.refresh()

WebUI.closeBrowser()

//log back into Nexus
//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer.select_customer(customer)

//get the Delivered number again after the Delivery date was changed in iTMS
String del_amnt_2 = Load_Summary_Behaviors.get_delivered_loads_amount()
//** convert second Delivery load total to integer
int del_amnt_int_2 = del_amnt_2 as Integer

//assert that the number of Delivered loads shown decreased by 1
assert del_amnt_int_2 == del_amnt_int_1 - 1

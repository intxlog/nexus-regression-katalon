import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import common_behaviors.Select_Customer
import common_behaviors.View_Details
import load_summary.Load_Summary_Behaviors
import load_details.Load_Details_Behaviors
import load_search.Load_Search_Behaviors

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select a customer which has at least one load
Select_Customer.select_customer(customer)

//expand one of the sections (In Transit) from the Load Summary page that has at least one load in it
Load_Summary_Behaviors.click_in_transit()

//get the number of Update button Elements before clicking Subscribe
Integer update_1 = Load_Summary_Behaviors.get_update_element_count()

//select the first Subscribe button BUT don't submit, so the popup is still open
Load_Summary_Behaviors.keep_subscribe_popup_open()

//go to the Load Search page
Load_Search_Behaviors click = new Load_Search_Behaviors()
click.click_load_search()

//verify that the subscribe popup is not present after going to Load Search
Load_Summary_Behaviors.verify_subscribe_popup_not_present()

//go back to the Load Summary page
WebUI.back()

//open the subscribe popup again ---> DEFECT https://intxlog.atlassian.net/browse/CUPO-450
//Defect needs to be fixed -- the popup stays open as of 1/8/2021
Load_Summary_Behaviors.keep_subscribe_popup_open()
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Nexus_Login
import common_behaviors.Open_Nexus
import load_search.Load_Search_Behaviors
import load_summary.Load_Summary_Behaviors
import itms_behaviors.*
import load_details.*
import common_behaviors.Select_Customer
import load_summary.Load_Summary_Behaviors

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2

////////iTMS Section 1//////////

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//create a load using the IEL tester toolkit
//List load_info_list = new_load.api_create_load_list()

//second list index is the load PO
//String iel_po = load_info_list[1]

////iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po = load.get_iel_po()
//open the top load from the future loads
//load.open_future_load()

//search for the load from api in iTMS
load.itms_load_search(iel_po)

//load update behaviors
Loadview_Behaviors load_update = new Loadview_Behaviors()

//add pickup date with today's date
load_update.add_pickup_date()

//add drop with today's date
load_update.add_drop_date()

WebUI.refresh()
//close the browser to close iTMS
WebUI.closeBrowser()
  
///Nexus section 1////

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer.select_customer(customer)

//go to future loads
Load_Summary_Behaviors summary = new Load_Summary_Behaviors()
summary.click_future_loads()

//select View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_last_view_details()

//verify that there is no broker notes ection present yet
details.verify_broker_notes_section_not_present()

//close the browser to close Nexus
WebUI.closeBrowser()

//////iTMS Section 2////////

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)
//search for the previous load in iTMS
load.itms_load_search(iel_po)
//create check call and do enter text into the Nexus notes section
load_update.click_generic_check_call()
//create check call that includes special characters
load_update.create_check_call_special_chars()

WebUI.refresh()
//close the browser to close iTMS
WebUI.closeBrowser()

////////Nexus Section 2/////////

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

Select_Customer.select_customer(customer)

//go to future loads
summary.click_future_loads()

//select View Details
details.click_last_view_details()

//verify that there is a Broker Notes section now
details.verify_broker_notes_section_is_present()
//verify that the Nexus notes test is present with the special characters
WebUI.delay(2)
details.verify_broker_notes_text_special_chars()

WebUI.closeBrowser()

////////iTMS Section 3/////////
//Add a second check call

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)
//search for the previous load in iTMS
load.itms_load_search(iel_po)
//create check call and do enter text into the Nexus notes section
load_update.click_generic_check_call()
load_update.create_second_check_call()

WebUI.refresh()
//close the browser to close iTMS
WebUI.closeBrowser()

////////Nexus Section 3/////////


//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

Select_Customer.select_customer(customer)

//go to future loads
summary.click_future_loads()

//select View Details
details.click_last_view_details()

//verify that there is a Broker Notes section now
details.verify_broker_notes_section_is_present()
//verify that the second Nexus notes test is present
details.verify_second_broker_notes_text()

WebUI.closeBrowser()

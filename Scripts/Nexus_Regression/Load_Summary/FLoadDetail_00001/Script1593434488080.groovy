import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import common_behaviors.Select_Customer
import common_behaviors.View_Details
import load_summary.Load_Summary_Behaviors
import load_details.Load_Details_Behaviors

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String customer = GlobalVariable.customer_2
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select a customer which has at least one load
Select_Customer.select_customer(customer)

//expand one of the sections (In Transit) from the Load Summary page that has at least one load in it
Load_Summary_Behaviors.click_in_transit()

//verify that In Transit has a load PO
Load_Summary_Behaviors.verify_load_present_in_transit()

//get the po number of the first load in the list to be able to verify it
String po = View_Details.get_first_po()

//click the View Details button
View_Details.click_view_load_details()

//get the po label in the view details and save it
String view_details_po = View_Details.get_view_details_po()

//verify that the Load Details label appears
Load_Details_Behaviors verify = new Load_Details_Behaviors()
verify.verify_load_details_label()

//verify that the first po number matches the po label in the load details
//View_Details.verify_load_details_po(po, view_details_po)

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import general_misc_tests.Make_A_Payment_Behaviors
import load_summary.Load_Summary_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select Make a Payment
Make_A_Payment_Behaviors pay = new Make_A_Payment_Behaviors()
pay.select_make_a_payment()

//select include payment for a payment
pay.select_first_include_payment()

//add the over payment
pay.add_first_overpayment()

//verify thereason box is bordered in red when there is no reason entered
pay.verify_reason_red_border()

//add reason
pay.enter_reason_1()
WebUI.delay(1)
//verify the reason box is no longer bordered in red
pay.verify_reason_no_border()
WebUI.delay(1)

//deselect the include payment
pay.deselect_first_incl_payment()
WebUI.delay(1)

//verify that the notes field is no longer visible
pay.verify_notes_box_removed()
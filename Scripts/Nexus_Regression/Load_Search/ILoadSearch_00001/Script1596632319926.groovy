import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import load_search.*
import load_summary.Load_Summary_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//click the Load Search link in the page header
Load_Search_Behaviors click = new Load_Search_Behaviors()
click.click_load_search()

//verify the elements on the Load Search page
Load_Search_Verifications elements = new Load_Search_Verifications()
elements.load_search_elements_verify()

//verify the colors of some of the side-bar elements
elements.load_search_sidebar_colors_verify()

//verify the column header elements for the loads section
Load_Summary_Verifications.verify_load_section_headers()

//verify the footer elements
Load_Summary_Verifications.footer_verifications()
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import load_details.Load_Details_Behaviors
import org.openqa.selenium.Keys as Keys
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import load_search.*
import load_summary.Load_Summary_Verifications


//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//click the Load Search link in the page header
Load_Search_Behaviors click = new Load_Search_Behaviors()
click.click_load_search()

//click the Documents Filters button
Documents docs = new Documents()
docs.open_documents_filter()

//select the None checkbox and verify that the other options are disabled
docs.select_none_filter()
//apply the filter
docs.click_apply_filters()

//open one of the loads with View Details
Load_Details_Behaviors details = new Load_Details_Behaviors()
details.click_view_details()

//verify the documents section has no documents in it (the Empty. text is present)
docs.verify_docs_section_blank()

//reopen the Documents Filters
click.click_load_search()
docs.open_documents_filter()

//select both the BOL and Invoice check boxes
docs.select_bol_and_invoice_filters()
docs.click_apply_filters()

//go to the View Details page
details.click_view_details()

//verify that one of the documents for BOL or invoice is present
docs.verify_bol_or_invoice_docs_section()

//reopen the Documents Filters
click.click_load_search()
docs.open_documents_filter()

//select the BOL filter only
docs.select_bol_filter()
docs.click_apply_filters()

//go to the View Details page
details.click_view_details()

//verify that just the BOL document is present
docs.verify_bol_docs_section()

//reopen the Documents Filters
click.click_load_search()
docs.open_documents_filter()

//select the invoice filter only
docs.select_invoice_filter()
docs.click_apply_filters()

//go to the View Details page
details.click_view_details()

//verify that just the BOL document is present
docs.verify_invoice_docs_section()
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import load_search.*
import load_summary.Load_Summary_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//filter name strings
String pickcheck = "Pickup Check-In"
String dropcheck = "Delivery Check-In"

Load_Search_Behaviors search = new Load_Search_Behaviors()

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//click the Load Search link in the page header
search.click_load_search()

//filter the results by pickup check-in and delivery check-in
search.apply_filter(pickcheck)

//verify the results
search.verify_filtered_results(pickcheck)

//remove the first filter
search.clear_status_filter()

//apply a filter for deilvery check-in
search.apply_filter(dropcheck)

//verify the results
search.verify_filtered_results(dropcheck)
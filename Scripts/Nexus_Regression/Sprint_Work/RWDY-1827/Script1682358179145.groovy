import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import load_search.*
import load_summary.Load_Summary_Verifications
import itms_behaviors.*
import billed_AR.Billed_AR_Behaviors
import un_billed_AR.Un_Billed_AR_Behaviors

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL

//filter name strings
String pickcheck = "Pickup Check-In"
String dropcheck = "Delivery Check-In"
String poOrder_num_upper_case1 = "Anpanman"
String poOrder_num_lower_case1 = "anpanman"
//
String poOrder_num_upper_case2 = "Test PO 1/"
String poOrder_num_lower_case2 = "test Po 1/"

Load_Search_Behaviors search = new Load_Search_Behaviors()
Billed_AR_Behaviors bill = new Billed_AR_Behaviors()
Un_Billed_AR_Behaviors unbill = new Un_Billed_AR_Behaviors()

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

////// First Defect//////
///// Status filter is broken (Not Working)
//click the Load Search link in the page header
search.click_load_search()

//filter the results by pickup check-in and delivery check-in
search.apply_filter(pickcheck)

//verify the results
search.verify_filtered_results(pickcheck)

//remove the first filter
search.clear_status_filter()

//apply a filter for deilvery check-in
search.apply_filter(dropcheck)

//verify the results
search.verify_filtered_results(dropcheck)

///////Second Defect//////
////PO/Order, Origin City, Destination City value applied case sensitive, the load is not pulling up
WebUI.refresh()
WebUI.delay(4)

//search with a lower case query, which also has an upper case result in the search table
search.search_with_po_order_number(poOrder_num_lower_case1)

//verify that the upper case result in the element shows when searching for the lower case equivalent of it 
search.verify_po_order_number_not_case_sensitive(poOrder_num_upper_case1)

//////Third Defect//////
/////Billed A/R Page
//////PO/Order(Alphanumeric or Alphabetical) value applied case sensitive, the load is not pulling up
WebUI.refresh()
WebUI.delay(4)

//go to the Bill A/R page
bill.goto_billed_ar()

//search with a lower case query, which also has an upper case result in the search table
bill.search_with_po_order_number(poOrder_num_lower_case2)

//verify that the upper case result in the element shows when searching for the lower case equivalent of it
bill.verify_po_order_number_not_case_sensitive(poOrder_num_upper_case2)

//////Fourth Defect//////
//////PO/Order(Alphanumeric or Alphabetical) value applied case sensitive, the load is not pulling up.
//////Unilled Loads Page
WebUI.refresh()
WebUI.delay(4)

//go to unbilled loads page
unbill.click_billing_tools()
unbill.goto_un_billed_loads()

//search with a lower case query, which also has an upper case result in the search table
unbill.search_with_po_order_number(poOrder_num_lower_case2)

//verify that the upper case result in the element shows when searching for the lower case equivalent of it
unbill.verify_po_order_number_not_case_sensitive(poOrder_num_upper_case2)
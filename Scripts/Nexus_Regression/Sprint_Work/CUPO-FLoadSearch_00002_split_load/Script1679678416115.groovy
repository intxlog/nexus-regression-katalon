import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import load_search.*
import load_summary.Load_Summary_Verifications

//variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String iel_po = "1024042"
String po_split_1 = "1024042A"
String po_split_2 = "1024042B"

//filter name strings
String pickcheck = "Pickup Check-In"
String dropcheck = "Delivery Check-In"

Load_Search_Behaviors search = new Load_Search_Behaviors()

//open browser and navigate to Nexus log in page
Open_Nexus.open(url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//click the Load Search link in the page header
search.click_load_search()

search.iel_po_search(iel_po)

search.verify_split_load_elements(po_split_1, po_split_2)


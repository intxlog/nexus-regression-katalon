import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.*
import itms_behaviors.*
import load_search.*

//global variables
String email = GlobalVariable.email
String n_p = GlobalVariable.pass
String n_url = GlobalVariable.URL
String login = GlobalVariable.itms_user
String pass = GlobalVariable.itms_pass
String url = GlobalVariable.URL_itms
String customer = GlobalVariable.customer
String carrier = GlobalVariable.carrier_mc
String pick = "Abiding Savior Lutheran Church"
String drop = "Aaron Lake Dam"
String AR_amount = "100"
String customer_po = "45656363"
String customer_rate = "33.33"

Load_Search_Behaviors search = new Load_Search_Behaviors()

//open browser and navigate to the iTMS log in page
Open_Nexus.open(url)

//log in to itms
Nexus_Login itms = new Nexus_Login()
itms.itms_login(login, pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//Load Board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()

////get the PO number////
String po = load.get_iel_po()

//open the top load from the future loads
load.open_future_load()

//load view behaviors
Loadview_Behaviors load_update = new Loadview_Behaviors()
//add carrier
load_update.add_carrier(carrier)

//add customer rate and po
load_update.add_customer_information(customer_po, customer_rate)

//add AP amount
load_update.add_AP_amounts()

//add AR amount
load_update.add_AR_amounts_bill_amount(AR_amount)

//add pick and drop
load_update.add_pickup(pick)
load_update.add_drop(drop)
//add pickup date with today's date
load_update.add_pickup_date()
//checkcalls
load_update.dispatch_load()
load_update.pick_checked_in()
load_update.pick_loaded()
load_update.drop_checked_in()
load_update.drop_delivered()
load_update.add_carrier_rating()

//Release to Billing
Release_To_Billing bill = new Release_To_Billing()
//click release to billing button
bill.click_release_to_billing()
WebUI.delay(3)

WebUI.closeBrowser()

//go to Nexus
//open browser and navigate to Nexus log in page
Open_Nexus.open(n_url)

//log in with email from the global variables/default profile
Nexus_Login.login(email, n_p)

//go to load search
search.click_load_search()

//search for the load
search.iel_po_search(po)

//verify that the column Total Cost is present
search.verify_total_AR_present()

//verify the AR amount from the load
search.verify_total_AR_amount(customer_rate)

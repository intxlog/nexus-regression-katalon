import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Open_Nexus as Open_Nexus
import common_behaviors.Nexus_Login as Nexus_Login
import billed_AR.Billed_AR_Behaviors as Billed_AR_Behaviors
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

//variables:
WebUI.openBrowser('')

WebUI.navigateToUrl('https://cupo-qa.web.app/ar/billed')

WebUI.setText(findTestObject('Object Repository/Page_IEL Nexus/input_Email Address_email'), 'qatesting@intxlog.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_IEL Nexus/input_Password_password'), '8SQVv/p9jVQfUrZwUPgLFA==')

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/button_Sign In'))

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/svg_concat(Freddy, , s Frozen Custard  Stea_760007'))

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/div_Test EDI Customer 0001'))

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/path'))

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/path_1'))

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/path_1_2'))

WebUI.click(findTestObject('Object Repository/Page_IEL Nexus/path_1_2_3'))


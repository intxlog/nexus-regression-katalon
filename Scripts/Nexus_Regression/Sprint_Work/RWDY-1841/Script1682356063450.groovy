import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.Open_Nexus
import common_behaviors.Nexus_Login
import common_behaviors.Select_Customer
import common_behaviors.View_Details
import load_summary.Load_Summary_Behaviors
import itms_behaviors.*

//https://intxlog.atlassian.net/browse/RWDY-1841

//global variables:
String email = GlobalVariable.email
String pass = GlobalVariable.pass
String url = GlobalVariable.URL
String itms_url = GlobalVariable.URL_itms
String itms_email = GlobalVariable.itms_user
String itms_pass = GlobalVariable.itms_pass
String customer = GlobalVariable.customer_2
String carrier = GlobalVariable.carrier_mc
//variables for different sheds and consignees
String shed_1 = "Abiding Savior Lutheran Church"
String conignee_1 = "Abells Wharf"
String status = "Dispatched"

//open browser and navigate to the iTMS log in page
Open_Nexus.open(itms_url)

//log in to iTMS with email from the global variables/default profile
Nexus_Login.itms_login(itms_email, itms_pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

////iTMS Load board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()

//go to the future loads tab
load.goto_future_loads_tab()
//save the PO for use in Nexus
String iel_po = load.get_iel_po()
println "po is " + iel_po

//open the top load from the future loads
load.open_future_load()

//add 2 different pickups and 2 drops, and a carrier to the load
Loadview_Behaviors load_update = new Loadview_Behaviors()
//pickups
load_update.add_pickup(shed_1)
//drops
load_update.add_drop(conignee_1)
//add carrier
load_update.add_carrier(carrier)  

load_update.dispatch_load()

//close itms
WebUI.closeBrowser()

//open Nexus
Open_Nexus.open(url)
//log in with email from the global variables/default profile
Nexus_Login.login(email, pass)

//select customer
Select_Customer.select_customer(customer)

//go to Today's loads
Load_Summary_Behaviors.click_today_loads()

//verify the ordered status
Load_Summary_Behaviors.verify_status_test_on_todays_loads(status)
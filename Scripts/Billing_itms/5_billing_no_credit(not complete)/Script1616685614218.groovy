import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import common_behaviors.*
import itms_behaviors.*

//global variables
String login = GlobalVariable.itms_user
String pass = GlobalVariable.itms_pass
String url = GlobalVariable.URL_itms
String carrier = GlobalVariable.carrier_mc

//Error message string
String error_msg = "Customer does not have enough credit"
//picks and drops
String pick = "Abiding Savior Lutheran Church"
String drop = "Aaron Lake Dam"

//open browser and navigate to the iTMS log in page
Open_Nexus.open(url)

//log in to itms
Nexus_Login itms = new Nexus_Login()
itms.itms_login(login, pass)

//create a new load for the Nexus customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load("Aaron Cemetery3")

//Load Board behaviors
Loadboard_Behaviors load = new Loadboard_Behaviors()
//go to the future loads tab
load.goto_future_loads_tab()
//open the top load from the future loads
load.open_future_load()

//load view behaviors
Loadview_Behaviors load_update = new Loadview_Behaviors()
//add customer information
load_update.add_customer_information("6676454", "5")
//add carrier
load_update.add_carrier(carrier)
//add pick and drop
load_update.add_pickup(pick)
load_update.add_drop(drop)
//add pickup date with today's date
load_update.add_pickup_date()
//checkcalls
load_update.dispatch_load()
load_update.pick_checked_in()
load_update.pick_loaded()
load_update.drop_checked_in()
load_update.drop_delivered()
//rate carrier
load_update.add_carrier_rating()

//change the customer credit
load_update.open_customer_from_loadview()
Customers customer = new Customers()
customer.decrease_available_credit("10000")

//close customer window
WebUI.closeWindowIndex(1)

//Release to Billing
Release_To_Billing bill = new Release_To_Billing()
//click release to billing button
bill.click_release_to_billing()

//verify that the correct message is Visible
bill.verify_error_message(error_msg)
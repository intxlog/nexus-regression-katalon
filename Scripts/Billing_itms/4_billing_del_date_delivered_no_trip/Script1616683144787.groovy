import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//package imports
import common_behaviors.*
import itms_behaviors.*

//global variables
String login = GlobalVariable.itms_user
String pass = GlobalVariable.itms_pass
String url = GlobalVariable.URL_itms
String customer = GlobalVariable.customer
String carrier = GlobalVariable.carrier_mc

//Error message string
String error_msg = "Valid trip miles are required to release this load"

//picks and drops
String pick = "Abiding Savior Lutheran Church"
String drop = "Aaron Lake Dam"

//open browser and navigate to the iTMS log in page
Open_Nexus.open(url)

//log in to itms
Nexus_Login itms = new Nexus_Login()
itms.itms_login(login, pass)

//create a new load for the customer in iTMS
Create_Load new_load = new Create_Load()
new_load.create_new_load(customer)

//Load Board behaviors
Loadboard_Behaviors loadboard = new Loadboard_Behaviors()
//go to the future loads tab
loadboard.goto_future_loads_tab()
//open the top load from the future loads
loadboard.open_future_load()

//get the load to the Delivered status
//Loadview behaviors
Loadview_Behaviors loadview = new Loadview_Behaviors()
loadview.add_pickup(pick)
//add a drop
loadview.add_drop(drop)
//add carrier
loadview.add_carrier(carrier)

//dispatch the load
loadview.dispatch_load()
//check in the pickup
loadview.pick_checked_in()
//mark the pickup as loaded
loadview.pick_loaded()
//check in the drop
loadview.drop_checked_in()
//mark the drop as delivered
loadview.drop_delivered()
//add the carrier rating
loadview.add_carrier_rating()

//No Trip Miles
//change Trip Miles to 0
loadview.remove_trip_miles()

//Release to Billing
Release_To_Billing bill = new Release_To_Billing()
//click release to billing button
bill.click_release_to_billing()

//verify that the correct "Failed - Valid trip miles are required to release this load" message is Visible
bill.verify_error_message(error_msg)

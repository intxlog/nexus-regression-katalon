package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Load_Statuses {

	//*Add Carrier* -> load into Booked status -> Ordered (Yellow) status in Nexus
	//	public void book_load(){
	//
	//	}

	//Dispatch the load -> Teal in Nexus
	//	public void dispatched_load(){
	//
	//	}

	//Pick checked-in -> Blue status in Nexus
	//	public void pick_checked_in(){
	//
	//	}

	//Loaded check box for pick -> purple in Nexus
	//	public void loaded_load(){
	//
	//	}

	//Drop Checked-In -> Blue in Nexus
	public void drop_checked_in(){

	}

	//Load Delivered -> Green in Nexus
	public void delivered_load(){

	}
}

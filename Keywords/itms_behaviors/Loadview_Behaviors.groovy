package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr
import helper_methods.*

import internal.GlobalVariable

public class Loadview_Behaviors {

	//Page element declarations for pickups section of load view
	public TestObject dispatchButton = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/button_Dispatch')
	public TestObject newPickButton = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/button_add_new_pickup')
	public TestObject addShedButton = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/button_pickup_add_shed')
	//pickup elements
	public TestObject checkInPickCheckbox = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickup_checkedin_box')
	public TestObject loadPickCheckbox = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickup_loaded_box')
	public TestObject calendarPickButton = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickup_calendar')
	public TestObject shedSaveButton = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/button_shed_save')
	public TestObject shedNameDropDown = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/dropdown_shed_select')
	public TestObject shedSearchInput = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_shed_Search')
	public TestObject pickupAddress1 = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_pickup_address_1')
	public TestObject pickupNumInput = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_pickup_number')
	public TestObject pickupDate = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_pickup_date')
	public TestObject pickupTime = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_pickup_time')
	public TestObject pickupCommodity = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_commodity_input')
	public TestObject pickupRemarks = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/input_remarks')
	public TestObject pickCheckedIn = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickup_checkedin_box')
	public TestObject pickLoaded = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickup_loaded_box')
	public TestObject select_consingee_text = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/select_consignee_text')
	public TestObject second_pick_checkedin = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/second_row_pick_checkedin')

	//second row pickup elements for second pick
	public TestObject pickupLoaded2 = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickLoaded2')
	public TestObject pickupCheckedIn2 = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/pickCheckedIn2')
	public TestObject pickupNumInput2 = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/PickNumber2')
	public TestObject pickupDate2 = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/PickDateInput2')
	public TestObject pickupTime2 = findTestObject('Object Repository/Page_iTMS Loadview/Pickups/PickTime2')
	//Page elements for Drops
	public TestObject addConsigneeButton = findTestObject('Object Repository/Page_iTMS Loadview/Drops/button_Add_shed_drops')
	public TestObject checkedInDropCheckBox = findTestObject('Object Repository/Page_iTMS Loadview/Drops/checkbox_checkedIn')
	public TestObject deliveredCheckBox = findTestObject('Object Repository/Page_iTMS Loadview/Drops/checkbox_delivered')
	public TestObject deleteDropButton = findTestObject('Object Repository/Page_iTMS Loadview/Drops/button_deleteDrop')
	public TestObject newDropButton = findTestObject('Object Repository/Page_iTMS Loadview/Drops/button_add_new_drop')
	public TestObject dropPOInput = findTestObject('Object Repository/Page_iTMS Loadview/Drops/input_deliveryPO')
	public TestObject dropDateInput = findTestObject('Object Repository/Page_iTMS Loadview/Drops/input_deliveryDate')
	public TestObject dropTimeInput = findTestObject('Object Repository/Page_iTMS Loadview/Drops/input_deliveryTime')
	public TestObject dropRemarks = findTestObject('Object Repository/Page_iTMS Loadview/Drops/input_remarks')
	public TestObject dropcalendarButton = findTestObject('Object Repository/Page_iTMS Loadview/Drops/button_calendarDrop')
	public TestObject drop_calendar_mouseover = findTestObject('Object Repository/Page_iTMS Loadview/Drops/drop_mouse_over')
	//second row Drop elements for the second consignee
	public TestObject dropCheckedIn2 = findTestObject('Object Repository/Page_iTMS Loadview/Drops/dropCheckedIn2')
	public TestObject dropDelivered2 = findTestObject('Object Repository/Page_iTMS Loadview/Drops/dropDelivered2')
	public TestObject dropPO2 = findTestObject('Object Repository/Page_iTMS Loadview/Drops/DropNumber2')
	public TestObject dropDate2 = findTestObject('Object Repository/Page_iTMS Loadview/Drops/DropDateInput2')
	//Carrier section elements
	public TestObject carrierNameInput = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/input_carrierName')
	public TestObject carSearchInput = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Carrier Popup/input_carrierSearch')
	public TestObject carListDropdown = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Carrier Popup/dropdown_carrierList')
	public TestObject carRemoveButton = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Carrier Popup/button_remove')
	public TestObject carCloseButton = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Carrier Popup/button_close')
	public TestObject carSelectButton = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Carrier Popup/button_select')
	public TestObject carSelectXButton = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Carrier Popup/button_x')
	public TestObject drivNameDropdown = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Driver/dropdown_driverName')
	public TestObject dispatcherNameDropdown = findTestObject('Object Repository/Page_iTMS Loadview/Carrier Information/Dispatcher/dropdown_dispatcherName')
	//check call inputs
	public TestObject checkCallTemp = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/input_temperature')
	public TestObject checkCallCity = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/input_city')
	public TestObject checkCallState = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/dropdown_state')
	public TestObject checkCallZip = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/input_zip')
	public TestObject checkCallResult = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/dropdown_callResults')
	public TestObject checkCallRemarks = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/input_checkCallRemarks')
	public TestObject checkCallMakePrimary = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/checkbox_makePrimaryNote')
	public TestObject checkCallSaveBtn = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/button_save')
	public TestObject nexus_notes = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/nexus_notes')
	public TestObject nexus_extern_facing = findTestObject('Object Repository/Page_iTMS Loadview/Check_Calls/Checkcall_Popup/nexus_externally_facing_checkbox')
	//carrier rating pop up
	public TestObject carrierRatingDropDown = findTestObject('Object Repository/Page_iTMS Loadview/Drops/dropdown_carrier_rating')
	public TestObject carrierRatingOnTimeRadio = findTestObject('Object Repository/Page_iTMS Loadview/Drops/radio_on_time')
	public TestObject carrierRatingComment = findTestObject('Object Repository/Page_iTMS Loadview/Drops/textarea_carrier_rating_comment')
	public TestObject carrierRatingSubmit = findTestObject('Object Repository/Page_iTMS Loadview/Drops/button_Submit_rating')

	//elements that have the check calls text.
	//They are block elements with more than one string for the text
	public TestObject date_time = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Date_and_Time')
	public TestObject city_state_zip = findTestObject('Object Repository/Load_Details/Details_Check_Calls/City_State_and_Zip')
	public TestObject temp_type = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Temp_and_Type')
	//Notes section
	public TestObject check_calls_tab = findTestObject('Object Repository/Page_iTMS Loadview/Notes/Checkcalls_tab')
	public TestObject external_checkbx_1 = findTestObject('Object Repository/Page_iTMS Loadview/Notes/external_note_1_checkbox')
	public TestObject external_checkbx_2 = findTestObject('Object Repository/Page_iTMS Loadview/Notes/external_note_2_checkbox')
	public TestObject external_save_change = findTestObject('Object Repository/Page_iTMS Loadview/Notes/external_Save_Change_popup')
	public TestObject add_check_call = findTestObject('Object Repository/Page_iTMS Loadview/Notes/generic_check_call_add_btn')
	public TestObject trip_miles = findTestObject('Object Repository/Page_iTMS Loadview/Load Information/trip_miles_btn')
	public TestObject trip_miles_field = findTestObject('Object Repository/Page_iTMS Loadview/Load Information/trip_miles_field')
	//Customer information section
	public TestObject input_po_number = findTestObject('Object Repository/Page_iTMS Loadview/Customer Information/input_po_number')
	public TestObject input_rate = findTestObject('Object Repository/Page_iTMS Loadview/Customer Information/input_rate')
	public TestObject billCustomerButton = findTestObject('Object Repository/Page_iTMS Loadview/Customer Information/button_bill_customer')

	//String input values
	String pickup_num = "19654822328"
	String pickup_num2 = "14568798148"
	String time = "12:00"
	String commodity = "Dry Goods"
	String remarks = "Random Remark 1"
	String delivery_PO = "66666666666"
	String driver_name = "Driver1"
	//dispatch check call input values
	String check_temp = "49"
	String check_city = "Cincinnati"
	String check_state = "OH"
	String check_results = "Spoke with Dispatch"
	String check_remarks = "random remark"
	String notes = "nexus notes 1"
	String notes_2 = "nexus notes 2"
	//TODO : put in profiles
	String notes_spec_char = "nexus_note_3 !!#^&!!"

	String rating_comments = "random rating comment"

	//Object declarations for methods
	//Instantiate helper method object to get a date
	Helper_Methods date = new Helper_Methods()

	public void close_load() {
		WebUI.closeWindowIndex(1)
		WebUI.delay(2)
		//WebUI.refresh()
	}

	//add new pickup line (by clicking the plus sign button)
	public void new_pickup(){
		WebUI.click(newPickButton)
	}

	//add new drop line (by clicking the plus sign button)
	public void new_drop(){
		WebUI.click(newDropButton)
	}

	public void add_pickup(String shed){
		//search for and add a shed, then tab out and save
		WebUI.click(addShedButton)
		WebUI.waitForElementVisible(shedSearchInput, 6)
		WebUI.setText(shedSearchInput, shed)
		//Tab out of the search field
		WebUI.sendKeys(shedSearchInput, Keys.chord(Keys.TAB))
		WebUI.delay(1)
		//click outside of the search box to add the consignee searched for
		//WebUI.click(select_consingee_text)
		WebUI.click(shedSaveButton)
		WebUI.delay(1)
		//		WebUI.waitForElementClickable(pickupNumInput, 6)
		//		WebUI.waitForElementClickable(pickupDate, 6)

		//Enter pickup date and random values//

		//WebUI.setText(pickupNumInput, pickup_num)
		//get today's date and set it
		String todays_date = date.today_date()
		WebUI.setText(pickupDate, todays_date)
		WebUI.delay(1)
		//WebUI.click(calendarPickButton)
		//there's a mouseover event for the calendar that needs to be triggered
		WebUI.mouseOver(pickupDate)
		WebUI.delay(1)
		//set pickup time
		//WebUI.setText(pickupTime, time)
		//set commodity
		WebUI.setText(pickupCommodity, commodity)
		WebUI.setText(pickupRemarks, remarks)
		//tab out of the remarks field
		WebUI.sendKeys(pickupRemarks, Keys.chord(Keys.TAB))
	}

	//** When adding two pickups, the first pickup date is cleared from the input field.
	//** Need to add the pickup date after the second pickup is added
	//add pick date with today's date
	public void add_pickup_date(){
		//get today's date and set it
		String todays_date = date.today_date()
		WebUI.setText(pickupDate, todays_date)
		WebUI.mouseOver(pickupDate)
		WebUI.sendKeys(pickupDate, Keys.chord(Keys.TAB))
	}

	public void add_pickup_future_date(){
		//get today's date and set it
		String future_date = date.future_date()
		WebUI.setText(pickupDate, future_date)
		WebUI.mouseOver(pickupDate)
		WebUI.sendKeys(pickupDate, Keys.chord(Keys.TAB))
	}

	public void add_second_pickup_date(){
		//get today's date and set it
		String todays_date = date.today_date()
		WebUI.setText(pickupDate2, todays_date)
		WebUI.sendKeys(pickupDate2, Keys.chord(Keys.TAB))
	}

	public void add_second_pickup(String shed){
		//search for and add a shed, then tab out and save
		WebUI.click(addShedButton)
		WebUI.setText(shedSearchInput, shed)
		//Tab out of the search field
		WebUI.sendKeys(shedSearchInput, Keys.chord(Keys.TAB))
		WebUI.delay(1)
		WebUI.click(shedSaveButton)
		WebUI.delay(2)
		//Enter pickup date and random values//

		WebUI.setText(pickupNumInput2, pickup_num2)
		//get today's date and set it
		String todays_date = date.today_date()
		WebUI.setText(pickupDate2, todays_date)
		//there's a mouseover event for the calendar that needs to be triggered
		WebUI.mouseOver(pickupDate2)
	}

	public void add_drop(String consignee){
		//search for and add a consignee, then tab out and save
		WebUI.click(addConsigneeButton)
		WebUI.waitForElementVisible(shedSearchInput, 6)
		WebUI.setText(shedSearchInput, consignee)
		//Tab out of the search field
		WebUI.sendKeys(shedSearchInput, Keys.chord(Keys.TAB))
		WebUI.delay(1)
		//WebUI.click(select_consingee_text)
		WebUI.click(shedSaveButton)
		WebUI.delay(1)

		//Enter pickup date and random values//
		//WebUI.setText(dropPOInput, delivery_PO)
		//get today's date plus two days
		String future_date = date.future_date()
		WebUI.setText(dropDateInput, future_date)
		//WebUI.click(dropcalendarButton)
		//there's a mouseover event for the calendar that needs to be triggered
		WebUI.mouseOver(drop_calendar_mouseover)
		WebUI.delay(1)
		//WebUI.setText(dropTimeInput, time)
		WebUI.setText(dropRemarks, remarks)
		//tab out of the remarks field
		WebUI.sendKeys(dropRemarks, Keys.chord(Keys.TAB))
	}

	//add drop date with today's date
	public void add_drop_date(){
		//get today's date and set it
		String todays_date = date.today_date()
		WebUI.setText(dropDateInput, todays_date)
		WebUI.mouseOver(drop_calendar_mouseover)
		WebUI.sendKeys(dropDateInput, Keys.chord(Keys.TAB))
	}

	//add drop date with a future date
	public void add_drop_future_date(){
		//get today's date and set it
		String todays_date = date.future_date()
		WebUI.setText(dropTimeInput, todays_date)
		WebUI.mouseOver(drop_calendar_mouseover)
		WebUI.sendKeys(dropDateInput, Keys.chord(Keys.TAB))
	}

	//add a Delivery date 4 days in the past
	public void add_past_delivery_date(){
		//First change the pickup date to same as the delivery date
		//clear date text
		date.clear_text(pickupDate)
		String pick_date = date.past_date()
		WebUI.setText(pickupDate, pick_date)
		//WebUI.click(calendarPickButton)
		//there's a mouseover event for the calendar that needs to be triggered
		WebUI.mouseOver(pickupDate)

		//get today's date and set it
		String past_date = date.past_date()
		WebUI.setText(dropDateInput, past_date)
		//WebUI.click(dropcalendarButton)
		WebUI.mouseOver(drop_calendar_mouseover)

	}

	public void add_second_drop(String consignee){
		//search for and add a consignee, then tab out and save
		WebUI.click(addConsigneeButton)
		WebUI.setText(shedSearchInput, consignee)
		//Tab out of the search field
		WebUI.sendKeys(shedSearchInput, Keys.chord(Keys.TAB))
		WebUI.click(shedSaveButton)

		//Enter pickup date and random values//
		WebUI.setText(dropPOInput, delivery_PO)
		//get today's date plus two days
		String future_date = date.future_date()
		WebUI.setText(dropDate2, future_date)
		//there's a mouseover event for the calendar that needs to be triggered
		WebUI.mouseOver(dropDate2)
	}

	public void add_customer_information(String customer_po, String customer_rate)
	{
		//add Customer PO and Rate
		WebUI.setText(input_po_number, customer_po)
		WebUI.setText(input_rate, customer_rate)
	}

	public void open_customer_from_loadview()
	{
		//open the customer from loadview
		WebUI.click(billCustomerButton)
	}

	public void add_carrier(String carrier){
		//add carrier from the global variable / profile
		WebUI.click(carrierNameInput)
		WebUI.waitForElementVisible(carSearchInput, 5)
		//enter carrier MC number from global variables, passed into the method the test case
		WebUI.setText(carSearchInput, carrier)
		WebUI.delay(1)
		WebUI.click(carSelectButton)
		WebUI.delay(2)
		//WebUI.waitForElementClickable(drivNameDropdown, 5)
		//add a driver
		WebUI.selectOptionByIndex(drivNameDropdown, 1)
		//WebUI.selectOptionByValue(drivNameDropdown, driver_name, false)
		WebUI.sendKeys(drivNameDropdown, Keys.chord(Keys.TAB))
		//add dispatcher
		WebUI.selectOptionByIndex(dispatcherNameDropdown, 1)
		WebUI.sendKeys(dispatcherNameDropdown, Keys.chord(Keys.TAB))
	}

	public void dispatch_load(){
		WebUI.click(dispatchButton)
		WebUI.delay(1)
		create_check_call()
	}

	public void pick_checked_in(){
		WebUI.click(pickCheckedIn)
		WebUI.delay(1)
		//call the check call method to fill out the check call
		create_check_call()
	}

	public void second_pick_checked_in(){
		WebUI.click(pickupCheckedIn2)
		WebUI.delay(1)
		create_check_call()
	}

	public void pick_loaded(){
		//click the check box for pickup Loaded
		WebUI.click(pickLoaded)
		WebUI.delay(1)
		//call the check call method to fill out the check call
		create_check_call()
	}

	public void second_pick_loaded(){
		WebUI.click(pickupLoaded2)
		WebUI.delay(1)
		//call the check call method to fill out the check call
		create_check_call()
	}

	public void drop_checked_in(){
		WebUI.click(checkedInDropCheckBox)
		WebUI.delay(1)
		//call the check call method to fill out the check call
		create_check_call()
	}

	public void drop_delivered(){
		WebUI.click(deliveredCheckBox)
		WebUI.delay(1)
		//call the check call method to fill out the check call
		create_check_call()
	}

	public void second_drop_delivered(){
		WebUI.click(dropDelivered2)
		WebUI.delay(1)
		create_check_call()
	}

	public void add_carrier_rating(){
		WebUI.click(carrierRatingOnTimeRadio)
		WebUI.setText(carrierRatingComment, rating_comments)
		WebUI.delay(1)
		WebUI.click(carrierRatingSubmit)
		WebUI.delay(3)
	}

	//click the generic check call button
	public void click_generic_check_call(){
		WebUI.click(add_check_call)
		WebUI.delay(1)
	}

	//Fill Out Check Calls info
	//TODO : return the time, so that it can be used in other methods
	public void create_check_call(){
		WebUI.delay(2)
		WebUI.setText(checkCallTemp, check_temp)
		WebUI.setText(checkCallCity, check_city)
		WebUI.selectOptionByValue(checkCallState, check_state, false)
		WebUI.delay(2)
		WebUI.setText(checkCallZip, "45236")
		WebUI.selectOptionByValue(checkCallResult, check_results, false)
		WebUI.delay(1)
		WebUI.setText(checkCallRemarks, check_remarks)
		//make this new note the primary one and then save
		WebUI.click(checkCallMakePrimary)
		//nexus notes
		WebUI.setText(nexus_notes, notes)
		//mark external with the check box
		WebUI.click(nexus_extern_facing)
		//save dispatch check call
		WebUI.click(checkCallSaveBtn)
		WebUI.delay(2)
	}

	public void create_second_check_call(){
		WebUI.delay(2)
		WebUI.setText(checkCallTemp, check_temp)
		WebUI.setText(checkCallCity, check_city)
		WebUI.selectOptionByValue(checkCallState, check_state, false)
		WebUI.setText(checkCallZip, "45236")
		WebUI.selectOptionByValue(checkCallResult, check_results, false)
		WebUI.setText(checkCallRemarks, check_remarks)
		//make this new note the primary one and then save
		WebUI.click(checkCallMakePrimary)
		//** seond nexus notes
		WebUI.setText(nexus_notes, notes_2)
		//mark external with the check box
		WebUI.click(nexus_extern_facing)
		//save dispatch check call
		WebUI.click(checkCallSaveBtn)
		WebUI.delay(2)
	}

	//Create a check call without entering text into the Nexus notes section
	public void create_check_call_no_nexus_notes(){
		WebUI.delay(2)
		WebUI.setText(checkCallTemp, check_temp)
		WebUI.setText(checkCallCity, check_city)
		WebUI.selectOptionByValue(checkCallState, check_state, false)
		WebUI.setText(checkCallZip, "45236")
		WebUI.selectOptionByValue(checkCallResult, check_results, false)
		WebUI.setText(checkCallRemarks, check_remarks)
		//make this new note the primary one and then save
		WebUI.click(checkCallMakePrimary)
		//mark external with the check box
		WebUI.click(nexus_extern_facing)
		//save dispatch check call
		WebUI.click(checkCallSaveBtn)
		WebUI.delay(2)
	}

	//create a check call with special characters in the Neus notes
	public void create_check_call_special_chars(){
		WebUI.delay(2)
		WebUI.setText(checkCallTemp, check_temp)
		WebUI.setText(checkCallCity, check_city)
		WebUI.selectOptionByValue(checkCallState, check_state, false)
		WebUI.setText(checkCallZip, "45236")
		WebUI.selectOptionByValue(checkCallResult, check_results, false)
		WebUI.setText(checkCallRemarks, check_remarks)
		//make this new note the primary one and then save
		WebUI.click(checkCallMakePrimary)
		//nexus notes
		WebUI.setText(nexus_notes, notes_spec_char)
		//mark external with the check box
		WebUI.click(nexus_extern_facing)
		//save dispatch check call
		WebUI.click(checkCallSaveBtn)
		WebUI.delay(2)
	}

	//Verify that the data entered in the check calls is saved correctly in the Load Details for the load in Nexus
	//Since the data is in this class, it's easier to have this method here, instead of the Load Details class
	public void verify_load_details_checkcall_data(String date){
		//get the text value for the Date and Time (can't get just the text for the individual sections)
		String date_and_time = WebUI.getText(date_time)
		println "the text from the element is: " + date_and_time
		//regex to remove the seconds portion of the text
		//String result = date_and_time.replace(/(:\d{2}| [AP]M)$/, "")
		//println "the text after the regex is: " + result
		//assert that the date and time string contain the date and time match the values from the other method
		assert date_and_time.contains(date)
		//can't get the time to match up yet - it needs to be very precise to match
		//assert result.contains(time)
	}

	public void uncheck_first_note(){
		//go to check calls tab in Notes section
		WebUI.click(check_calls_tab)
		WebUI.delay(1)
		WebUI.waitForElementClickable(external_checkbx_1, 5)
		//un-check
		WebUI.uncheck(external_checkbx_1)
		WebUI.delay(2)
		//save change
		//WebUI.waitForElementClickable(external_save_change, 5)
		WebUI.click(external_save_change)
		//WebUI.refresh()
	}

	public void uncheck_second_note(){
		//go to check calls tab in Notes section
		WebUI.click(check_calls_tab)
		WebUI.delay(1)
		//un-check
		WebUI.uncheck(external_checkbx_2)
		WebUI.delay(2)
		//save change
		WebUI.click(external_save_change)
		//WebUI.refresh()
	}

	//click the trip miles and get the value of the miles
	public String trip_miles(){
		WebUI.click(trip_miles)
		WebUI.delay(1)
		//get the value of the trip miles
		String miles = WebUI.getText(trip_miles_field)
		return miles
	}

	//the trip miles automatically calculate
	//this method will enter zero into the trip miles input field
	public void remove_trip_miles() {
		WebUI.setText(trip_miles_field, "0")
	}

	//add values for AR amounts
	public void add_AR_amounts() {
		//add an amount to the rate input in the customer info section
		WebUI.setText(findTestObject('Object Repository/Page_iTMS Loadview/Customer Information/input_rate'), "20")
		//tab out of the rate input
		WebUI.sendKeys(findTestObject('Object Repository/Page_iTMS Loadview/Customer Information/input_rate'), Keys.chord(Keys.TAB))
	}

	//add values for AP amounts
	public void add_AP_amounts() {
		//click the pay truck button and switch to the next window
		WebUI.click(findTestObject('Object Repository/Page_iTMS Loadview/Billing/pay_truck_button'))
		WebUI.switchToWindowIndex(2)
		//click the Add button to add a pay truck amount
		WebUI.click(findTestObject('Object Repository/Page_iTMS Loadview/Billing/pay_truck_add_btn'))
		WebUI.setText(findTestObject('Object Repository/Page_iTMS Loadview/Billing/pay_truck_rate_input'), "10")
		//tab out of the rate field
		WebUI.sendKeys(findTestObject('Object Repository/Page_iTMS Loadview/Billing/pay_truck_rate_input'), Keys.chord(Keys.TAB))
		WebUI.delay(2)
		//close the pay truck window
		WebUI.closeWindowIndex(2)

		WebUI.delay(1)
		//WebUI.refresh()
		//switch back to the Load View window index
		WebUI.switchToWindowIndex(1)
		WebUI.delay(1)

	}

	//add AR amounts for Bill section
	public void add_AR_amounts_bill_amount(String amount) {
		//click the expenses button and switch to the next window
		WebUI.click(findTestObject('Object Repository/Page_iTMS Loadview/Billing/AR/Expenses/button_Expenses'))
		WebUI.switchToWindowIndex(2)
		//click the Add button to add an expense amount
		WebUI.click(findTestObject('Object Repository/Page_iTMS Loadview/Billing/AR/Expenses/button_Add'))
		WebUI.setText(findTestObject('Object Repository/Page_iTMS Loadview/Billing/AR/Expenses/amount_input'), amount)
		//move the focus/cursor to the input field, since it has an "onblur" trigger
		//then move the focus away from the input field
		//WebUI.focus(findTestObject('Object Repository/Page_iTMS Loadview/Billing/expense_amount_input'))
		WebUI.click(findTestObject('Object Repository/Page_iTMS Loadview/Billing/AR/Expenses/notes_input'))
		WebUI.delay(2)

		WebUI.refresh()
		WebUI.delay(1)
		//close the pay truck window
		WebUI.closeWindowIndex(2)
		//switch back to the Load View window index
		WebUI.switchToWindowIndex(1)
		WebUI.delay(1)
	}
}

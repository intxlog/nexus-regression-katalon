package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import internal.GlobalVariable
import itms_behaviors.Loadview_Behaviors

public class Loadboard_Behaviors {

	//Page element object declarations
	public TestObject futureLoadsTab = findTestObject('Object Repository/Page_iTMS  Loadboard/tab_futureLoads')
	public TestObject customerHeader = findTestObject('Object Repository/Page_iTMS  Loadboard/th_Customer')
	public TestObject poNumHeader = findTestObject('Object Repository/Page_iTMS  Loadboard/th_PO Number')
	public TestObject topRowPONum = findTestObject('Object Repository/Page_iTMS  Loadboard/top_row_PO_num')
	public TestObject loadStatus = findTestObject('Object Repository/Page_iTMS  Loadboard/tab_futureLoads')
	public TestObject loadOwnerLabel = findTestObject('Object Repository/Page_iTMS Loadview/Load Information/label_pONumOwner')
	public TestObject search_po_input = findTestObject('Object Repository/Page_iTMS  Loadboard/quicksearch_po')
	public TestObject loadview_po_owner = findTestObject('Object Repository/Page_iTMS Loadview/Load Information/label_pONumOwner')

	//object for carrier rating
	Loadview_Behaviors rating = new Loadview_Behaviors()

	String voided_row_background_color = "rgb(128, 128, 128,0)"

	public void goto_future_loads_tab(){
		//make sure the future load tab appears, and then click on it
		WebUI.verifyElementPresent(futureLoadsTab, 3, FailureHandling.STOP_ON_FAILURE)
		WebUI.refresh()
		WebUI.click(futureLoadsTab)
		//click the load column header for PO twice to sort, and make sure the new load is at the top of the table
		//		WebUI.click(poNumHeader)
		//		WebUI.click(poNumHeader)
		//		WebUI.click(futureLoadsTab)
		//wait for customer table header to be visible
		WebUI.waitForElementVisible(customerHeader, 5)
		WebUI.delay(2)
	}

	public void open_future_load(){
		//open the load
		WebUI.doubleClick(topRowPONum)
		WebUI.delay(1)
		WebUI.switchToWindowIndex(1, FailureHandling.STOP_ON_FAILURE)
		WebUI.waitForElementVisible(loadOwnerLabel, 3)
	}

	//retrieve the IEL PO number from the load on the LOADBOARD, so that it can be returned and used in other methods
	public String get_iel_po(){
		//getting the string value for the top PO number in the load board
		String iel_po = WebUI.getText(topRowPONum)
		return iel_po
	}

	//quick search for a PO number which is passed from the get_iel_po() method above
	public void itms_load_search(String po){
		//search for the PO number and submit, opening the corresponding load
		WebUI.setText(search_po_input, po)
		WebUI.submit(search_po_input)

		WebUI.delay(3)
		//switch to the load view window
		WebUI.switchToWindowIndex(1, FailureHandling.STOP_ON_FAILURE)

		//after opening the load, check for the carrier rating popup
		//if it appears, then add the rating and submit
		if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_iTMS Loadview/Drops/carrier_rating_popup'), 7, FailureHandling.OPTIONAL)) {
			rating.add_carrier_rating()
			WebUI.delay(1)
		}

		WebUI.waitForElementVisible(loadview_po_owner, 10)
	}

	public void goto_load_requests_tab() {
		WebUI.click(findTestObject('Object Repository/Page_iTMS  Loadboard/pending_requests_tabs'))
	}

	public void accept_load_request() {
		WebUI.click(findTestObject('Object Repository/Page_iTMS  Loadboard/button_Accept'))
		WebUI.click(findTestObject('Object Repository/Page_iTMS  Loadboard/ok_btn'))
		WebUI.delay(5)
		WebUI.refresh()
	}

	public void verify_request_is_voided(String po) {

		//		String status = WebUI.getText(findTestObject('Object Repository/Load_Request/Request_History/itms_top_status_element'))
		//		WebUI.verifyMatch(status, "VOIDED", false)
		//find the element located using the passed-in random PO number
		TestObject to = new TestObject("request_row")
		to.addProperty("text", ConditionType.EQUALS, po)

		//get the background color of the row
		String actual_bgroung_color = WebUI.getCSSValue(to, 'background-color')

		//verify the background color as grey
		WebUI.verifyMatch(actual_bgroung_color, voided_row_background_color, false)
	}

	//verify that the randomized values from the Nexus load request are present on the Load Requests tab in load board
	//take in the list of random values from nexus as the parameter
	public void verify_request_values(List values_list) {
		//get the values from the list
		String rand_po = values_list[0]
		String rand_note = values_list[1]

		//create dynamic objects using the parameter values from the list
		TestObject po = new TestObject("random PO")
		po.addProperty("text", ConditionType.EQUALS, rand_po)
		WebUI.verifyElementPresent(po, 8)

		TestObject note = new TestObject("random note")
		note.addProperty("text", ConditionType.EQUALS, rand_note)
		WebUI.verifyElementPresent(note, 8)
	}
}

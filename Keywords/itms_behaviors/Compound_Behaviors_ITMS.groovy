package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable


//This class will hold compound behavior methods -
//behaviors which go through several steps to setup loads, go through statuses, etc.
public class Compound_Behaviors_ITMS {

	//objects reused
	Create_Load create = new Create_Load()
	Loadboard_Behaviors loadboard = new Loadboard_Behaviors()
	Loadview_Behaviors loadview = new Loadview_Behaviors()

	//variables for methods
	String customer = GlobalVariable.customer
	String shed_1 = "Abiding Savior Lutheran Church"
	String shed_2 = "Aaron Lake Dam"
	String conignee_1 = "Abells Wharf"
	String consignee_2 = "Adkins Chapel"
	String carrier = GlobalVariable.carrier_mc

	/////LOAD SETUP METHODS/////

	//create a new load for a Nexus customer and go through the statuses to get it to
	//be in the Today's Loads in Nexus
	public void create_todays_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		//?? do we need to return the value of the IEL PO?
		//String iel_po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		//loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add drop date to today
		loadview.add_drop_date()
		//add carrier
		//loadview.add_carrier(carrier)

		//WebUI.refresh()
		//WebUI.closeBrowser()
	}

	//create a new load for a Nexus customer and go through the statuses to get it to
	//be in the In Transit Loads in Nexus
	public void create_in_transit_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		//?? do we need to return the value of the IEL PO?
		//String iel_po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		loadview.dispatch_load()
		//check in the pickup
		loadview.pick_checked_in()
		//mark the pickup as loaded
		loadview.pick_loaded()

		//WebUI.refresh()
		//WebUI.closeBrowser()
	}

	//create a new load for a Nexus customer and go through the statuses to get it to
	//be in the Future Loads in Nexus
	public void create_future_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		//?? do we need to return the value of the IEL PO?
		//String iel_po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add a future date for the pickup
		loadview.add_pickup_future_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		//loadview.dispatch_load()

		//WebUI.refresh()
		//WebUI.closeBrowser()
	}

	//create a new load for a Nexus customer and go through the statuses to get it to
	//be in the Delivered Loads in Nexus
	public String create_delivered_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		String po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		loadview.dispatch_load()
		//check in the pickup
		loadview.pick_checked_in()
		//mark the pickup as loaded
		loadview.pick_loaded()
		//check in the drop
		loadview.drop_checked_in()
		//mark the drop as delivered
		loadview.drop_delivered()
		//add the carrier rating
		loadview.add_carrier_rating()

		//WebUI.refresh()
		//WebUI.closeBrowser()

		return po
	}

	//create a load that only goes to the Dispatched status
	//return the string value of the IEL PO number
	public String create_dispatched_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()
		//get the PO number
		String po = loadboard.get_iel_po()
		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		loadview.dispatch_load()

		WebUI.refresh()
		//close the load view window
		WebUI.closeWindowIndex(1)

		return po
	}

	//create a load that only goes to the Pickup Checked-In status
	//return the string value of the IEL PO number
	public String create_pick_checkedin_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		String po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		loadview.dispatch_load()
		//check in the pickup
		loadview.pick_checked_in()

		WebUI.refresh()
		//close the load view window
		WebUI.closeWindowIndex(1)

		return po
	}

	//create a load that only goes to the Pickup Loaded status
	//return the string value of the IEL PO number
	public String create_pick_loaded_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		String po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		loadview.dispatch_load()
		//check in the pickup
		loadview.pick_checked_in()
		//mark the pickup as loaded
		loadview.pick_loaded()

		WebUI.refresh()
		//close the load view window
		WebUI.closeWindowIndex(1)

		return po
	}

	//create a load that only goes to the Drop Checked-In status
	//return the string value of the IEL PO number
	public String create_drop_checkedin_load(){
		//create new load with customer from the profile
		create.create_new_load(customer)
		//go to the future loads tab
		loadboard.goto_future_loads_tab()

		String po = loadboard.get_iel_po()

		//open the top load from the future loads
		loadboard.open_future_load()
		//pickups
		loadview.add_pickup(shed_1)
		//add today's date for the pickup
		loadview.add_pickup_date()
		//drops
		loadview.add_drop(conignee_1)
		//add carrier
		loadview.add_carrier(carrier)
		//dispatch the load
		loadview.dispatch_load()
		//check in the pickup
		loadview.pick_checked_in()
		//mark the pickup as loaded
		loadview.pick_loaded()
		//check in the drop
		loadview.drop_checked_in()

		WebUI.refresh()
		//close the load view window
		WebUI.closeWindowIndex(1)

		return po

	}

	/////General Load Status Compound Behaviors/////
}

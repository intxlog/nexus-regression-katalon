package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Void_Loads {

	//open each Future load in Load View, then void the load and wait, since it takes some time to update
	public void void_future_loads(){
		/*
		 * Steps:
		 * !! Count the Number of loads in the future loads table!
		 * while future_load_count > 0 do:
		 * 		login to itms
		 * 		go to the future loads tab on load board
		 * 		sort by date..
		 * 		open the first load, at the top of the table
		 * 		void the open load
		 * 		refresh the load view
		 * 		close the load view
		 * 		Wait!! how long? -> ** 10-15 seconds AT LEAST
		 * 		The load should disappear from the load board, and the next newest one can be opened
		 * Stop when there are no more loads in the future tab
		 * OR keep one load? for testing purposes? (while load_count > 1)
		 */

		/*
		 * OR instead of waiting for the top load to go away,
		 * iterate through the tables, IF there is a reliable index value
		 * This would go very fast, so still would need to wait, 
		 * but it might be quicker than waiting for each load to go away
		 * 
		 * ** ALSO refreshing the load board may speed up the removal of the load
		 * Tested manually, and after 4-5 seconds and refreshing, the load goes away
		 */
	}

	//open each Active load in Load View, then void the load and wait, since it takes some time to update
	public void void_active_loads(){

	}

	//open each Today's load in Load View, then void the load and wait, since it takes some time to update
	public void void_todays_loads(){

	}
}

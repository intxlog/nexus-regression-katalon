package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType
//import com.kms.katalon.core.testobject.TestObject

public class Release_To_Billing {


	//page element objects
	TestObject rel_to_bill_btn = findTestObject('Object Repository/Page_iTMS Billing/button_release_to_billing')
	//error messages
	TestObject no_carrier = findTestObject('Object Repository/Page_iTMS Billing/error_no_carrier')
	TestObject not_delivered = findTestObject('Object Repository/Page_iTMS Billing/error_drop_not_delivered')

	//click the button to release to billing
	public void click_release_to_billing() {
		WebUI.click(rel_to_bill_btn)
		WebUI.delay(8)
	}

	//verify the Visibility of the text in the error pop-up
	public void verify_error_message(String error_msg) {
		String msg = error_msg
		//create a dynamic test object which finds the page element by the passed-in string parameter
		TestObject to = new TestObject("error_ms_txt")
		to.addProperty("text", ConditionType.EQUALS, msg)
		//verify the dynamic text element is visible on the page
		WebUI.verifyElementVisible(to)
	}
}

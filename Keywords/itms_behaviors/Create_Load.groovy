package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject
import groovy.json.JsonSlurper

public class Create_Load {

	//Page Element Declarations for the Create New Load behavior:
	public TestObject newLoadButton = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/button_newLoad')
	public TestObject selectCustomerDropdown = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/dropdown_selectCustomer')
	public TestObject valueDropdown = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/dropdown_value')
	public TestObject weightInput = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/input_weight')
	public TestObject commodityDropdown = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/dropdown_commodity')
	public TestObject trailerDropdown = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/dropdown_trailer')
	public TestObject sizeDropdown = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/dropdown_size')
	public TestObject tempInput = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/input_temp')
	public TestObject lTLDropdown = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/dropdown_LTL')
	public TestObject addButton = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/button_add')
	public TestObject cust_select = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/cust_select')
	public TestObject cust_input = findTestObject('Object Repository/Page_iTMS  Loadboard/New Load/cust_input')

	//variables for load attributes
	public String weight = '20000'
	public String temperature = '45'
	public String size_value = "53 ft"
	public String commodity = "Furniture"
	public String trailer_type = "Van"

	public List api_create_load_list() {
		//run the load payload API call
		def response = WS.sendRequest(findTestObject('Object Repository/WebService_Objects/create_load'))
		//get the content of the response
		def getContent = response.getResponseBodyContent()
		//println "content is: " + getContent
		//parse the content of the response
		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(getContent)
		//println "parsed json is: " + parsedJson

		//create empty list to return
		def load_info_list = []

		//get the string text of the "loadIDs" json member
		String id = parsedJson.get("loadIDs").get(0) //get index 0 member
		//add the id to the front of the list
		load_info_list << id

		//get the string text of the "poNumbers" json member
		String po = parsedJson.get("poNumbers").get(0) //get index 0 member
		//add the PO number to the list
		load_info_list << po

		//list should be : [id, po]
		return load_info_list
	}

	public List api_create_load_list_2_picks_drops() {
		//run the load payload API call
		def response = WS.sendRequest(findTestObject('Object Repository/WebService_Objects/create_load_2_picks_and_drops'))
		//get the content of the response
		def getContent = response.getResponseBodyContent()
		//println "content is: " + getContent
		//parse the content of the response
		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(getContent)
		//println "parsed json is: " + parsedJson

		//create empty list to return
		def load_info_list = []

		//get the string text of the "loadIDs" json member
		String id = parsedJson.get("loadIDs").get(0) //get index 0 member
		//add the id to the front of the list
		load_info_list << id

		//get the string text of the "poNumbers" json member
		String po = parsedJson.get("poNumbers").get(0) //get index 0 member
		//add the PO number to the list
		load_info_list << po

		//list should be : [id, po]
		return load_info_list
	}

	//create and add a load to the load board
	public void create_new_load(String customer){
		//Create a new non-reefer load
		WebUI.waitForElementVisible(newLoadButton, 5)
		WebUI.click(newLoadButton)
		//Select the customer variable passed in from the global variable in the test case
		//Click the drop-down menu, enter the global variable customer, then enter the text
		WebUI.click(cust_select)
		//WebUI.delay(1)
		WebUI.setText(cust_input, customer)
		WebUI.delay(1)
		WebUI.sendKeys(cust_input, Keys.chord(Keys.ENTER))
		WebUI.delay(1)

		//Enter a weight value
		WebUI.setText(weightInput, weight)
		//Select values from drop-downs
		WebUI.selectOptionByLabel(commodityDropdown, commodity, false)
		//non-reefer trailer type
		WebUI.selectOptionByLabel(trailerDropdown, trailer_type, false)
		//temperature
		WebUI.setText(tempInput, temperature)
		//add the trailer
		WebUI.click(addButton)
		WebUI.delay(2)
	}
}

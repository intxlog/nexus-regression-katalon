package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

public class Customers {

	//page element test object declarations
	public static TestObject customers = findTestObject("Object Repository/Page_iTMS_Customers_Module/itms_customer")
	public static TestObject external = findTestObject("Object Repository/Page_iTMS_Customer/itms_customer_external_btn")
	public static TestObject create_acct = findTestObject("Object Repository/Page_iTMS_Customer/itms_create_account_btn")
	public static TestObject first_name = findTestObject("Object Repository/Page_iTMS_Customer/itms_customer_first_name")
	public static TestObject last_name = findTestObject("Object Repository/Page_iTMS_Customer/itms_customer_last_name")
	public static TestObject email_input = findTestObject("Object Repository/Page_iTMS_Customer/itms_customer_email")
	public static TestObject phone = findTestObject("Object Repository/Page_iTMS_Customer/itms_customer_phone")
	public static TestObject submit = findTestObject("Object Repository/Page_iTMS_Customer/itms_customer_submit_btn")

	//credit request test object declarations
	public static TestObject credit_request_change_button = findTestObject("Object Repository/Page_iTMS_Customer/Credit Request/itms_customer_credit_request_btn")
	public static TestObject available_credit_edit_button = findTestObject("Object Repository/Page_iTMS_Customer/Credit Request/itms_customer_available_credit_edit_btn")
	public static TestObject available_credit_increase = findTestObject("Object Repository/Page_iTMS_Customer/Credit Request/itms_customer_available_credit_incease_input")
	public static TestObject available_credit_decrease = findTestObject("Object Repository/Page_iTMS_Customer/Credit Request/itms_customer_available_credit_decrease_input")
	public static TestObject available_credit_submit = findTestObject("Object Repository/Page_iTMS_Customer/Credit Request/itms_customer_available_credit_save_btn")


	//click the Customers module from the top tool bar and switch to the window
	public static void click_customers_module(){
		WebUI.click(customers)
		WebUI.delay(1)
		WebUI.switchToWindowIndex(1)
	}

	//click the External tab in the Customers module
	public static void click_external(){
		WebUI.click(external)
	}

	//click the Create Account option
	public static void click_create_account(){
		WebUI.click(create_acct)
		WebUI.delay(1)
		WebUI.switchToWindowIndex(2)
	}

	//enter info for the new customer account and submit
	public static void submit_new_acct_info(String email){
		WebUI.setText(first_name, "TesterName")
		WebUI.setText(last_name, "TesterLastName")
		WebUI.setText(email_input, email)
		WebUI.setText(phone, "1115559755")
		WebUI.click(submit)
	}

	//submit a credit request
	public static void submit_credit_request()
	{
		WebUI.click(credit_request_change_button)
	}

	//change available credit - increase
	public void increase_available_credit(String amount)
	{
		WebUI.click(available_credit_edit_button)
		WebUI.setText(available_credit_increase, amount)
		WebUI.click(available_credit_submit)
	}

	//change available credit - decrease
	public void decrease_available_credit(String amount)
	{
		WebUI.click(available_credit_edit_button)
		WebUI.setText(available_credit_decrease, amount)
		WebUI.click(available_credit_submit)
	}
}

package itms_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType

public class Idocs {

	public TestObject idocs_btn = findTestObject('Object Repository/Page_iTMS Loadview/iDocs/idocs_btn')
	public TestObject fake_bol = findTestObject('Object Repository/Page_iTMS Loadview/iDocs/fake_bol')

	public void open_idocs() {
		WebUI.click(idocs_btn)
		WebUI.delay(2)
		WebUI.switchToWindowIndex(2)
	}

	//take a file and drop/add it to a specified test object for the drop box in the iDocs pop-up
	//pass in the string file path for the test file
	public void drop_file_into_idocs(String file_path) {
		//upload the file by dragging the file to the iDocs drop box
		WebUI.uploadFileWithDragAndDrop(findTestObject('Object Repository/Page_iTMS Loadview/iDocs/dropbox'), file_path)

		WebUI.delay(2)
	}

	//drag and drop a file from the unfiled folder to the Nexus Documents folder
	//pass in the string locator for the test file in the original Unfiled location/folder
	public void drag_unfiled_file_into_nexus_folder(String file_name) {
		//create a new test object for the original file object
		TestObject to1 = new TestObject("target_file")
		to1.addProperty("text", ConditionType.CONTAINS, file_name)

		//drag and drop the file from its original folder to the Nexus Documents folder
		WebUI.dragAndDropToObject(to1, findTestObject('Object Repository/Page_iTMS Loadview/iDocs/nexus_documents_folder'))

		WebUI.delay(2)
	}

	//verify the presence of the fake BOL on the iDOCS page
	public void verify_fake_bol() {
		WebUI.verifyElementPresent(fake_bol, 8)
	}

	//verify that no fake BOL is present on the page
	public void verify_no_bol() {
		WebUI.verifyElementNotPresent(fake_bol, 8)
	}

	//verify that a document that was added in Nexus is present in the same load
	//pass in the name of the file to verify
	public void verify_document_from_nexus(String file_name) {
		TestObject to = new TestObject("file")
		to.addProperty("text", ConditionType.CONTAINS, file_name)

		WebUI.verifyElementPresent(to, 8)
	}

	//after user permission for IDOCS_DELETE is denied, the Delete button in iDocs is not present
	public void verify_doc_delete_not_allowed() {
		WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_iTMS Loadview/iDocs/delete_selected_btn'), 8)
	}
}

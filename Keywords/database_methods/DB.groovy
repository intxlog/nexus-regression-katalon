package database_methods

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.sql.Connection

//*** Need to paste the postgresql-42.2.9.jar (or other version) file into the Drivers folder of the project
//*** User and pass variables taken from internal data file which is NOT PUSHED to the repository. Needs to be in .gitignore file
//*** When using the database methods, always call connect_db() , then the SQL executor needed, then call close_connection() after

public class DB {

	//instantiate a connection object
	private Connection connection = null

	/**
	 * Connect to the database
	 *
	 * @return Connection object
	 */
	public Connection connect_db(){
		//get data from data file for connection parameters
		def data = findTestData("Data Files/db_data")
		String ee = data.getValue(1, 1)
		String vv = data.getValue(2, 1)
		String uu = data.getValue(3, 1)

		//load the postgres driver
		Class.forName("org.postgresql.Driver")
		//String conn = "jdbc:postgresql://10.0.8.52:5432/itms?public"
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = DriverManager.getConnection(uu, ee, vv)
		return connection
	}

	/**
	 * Execute SQL string: Use when you want to just query the database (e.g. SELECT statement)
	 *
	 * @param SQL query as a string
	 * @return ResultSet object - a table of retrieved values
	 */
	public ResultSet execute_query(String query_string){
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(query_string)
		return rs
	}

	/**
	 * Execute SQL string 2: Use when you want to just query the database (e.g. SELECT statement).
	 * Use when you want to query the database AND get an integer returned - if the integer needs to be passed to another method
	 * e.g. when getting integer ID value from database
	 *
	 * @param SQL query as a string
	 * @return integer - value retrieved from database
	 */
	public int get_result(String query_string){
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(query_string)
		int ID
		while(rs.next()){
			ID = rs.getInt("id")
			//print for testing
			println "ID = " + ID
		}
		return ID
	}
	
	/**
	 * Execute SQL string - then with the target column name, return the string value from the column
	 *
	 * @param SQL query as a string
	 * @param column name as a string
	 * @return string - text value retrieved from database
	 */
	public String get_result_string(String query_string, String column_name){
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(query_string)
		String str
		while(rs.next()){
			str = rs.getString(column_name)
			//print for testing
			println "value from column = " + str
		}
		return str
	}

	/**
	 * Execute SQL string 3: Use when you want to update the database. e.g. an UPDATE statement
	 *
	 * @param SQL query as a string
	 * @return boolean - true if the first result is a ResultSet object; false if it is an update count or there are no results (throws exception)
	 */
	public boolean execute_update(String query_string){
		Statement stm = connection.createStatement()
		boolean result = stm.execute(query_string)
		if(result){
			println "Query executed successfully and returned a result set"
		}
		return result
	}

	/**
	 * Execute SQL string 4: Overloaded method to pass a previously retrieved integer value (e.g. ID number)
	 * The query string which is passed, needs to be partial, with the first integer parameter concatenated with it
	 *
	 * @param SQL query string
	 * @param integer retrieved from database (e.g. ID)
	 * @return boolean - true if the first result is a ResultSet object; false if it is an update count or there are no results (throws exception)
	 */
	public boolean execute_update(String query_string, int ID){
		//concatenate the returned integer with the partial query string
		query_string = query_string + ID
		Statement stm = connection.createStatement()
		boolean result = stm.execute(query_string)
		if(result){
			println "Query executed successfully and returned a result set"
		}
		return result
	}

	/**
	 * Close the database connection: Needs to be called after opening a connection
	 */
	public void close_connection(){
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = null
	}
}

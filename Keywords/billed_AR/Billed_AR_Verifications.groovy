package billed_AR

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Billed_AR_Verifications {
	
	//page element objects
	//top of page
	TestObject nexus_icon = findTestObject('Object Repository/Load_Summary/nexus_image')
	TestObject billed_ar_label = findTestObject('Object Repository/Billed_AR/Billed_AR_label')
	TestObject AR_customer = findTestObject('Object Repository/Billed_AR/AR_customer')
	TestObject make_payment_btn = findTestObject('Object Repository/Billed_AR/make_a_payment_button')
	//info section
	TestObject info_cust_name = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/customer_name')
	TestObject info_addrss_1 = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/address_1')
	TestObject info_addrss_2 = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/address_2')
	TestObject info_contact = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/Contact')
	TestObject info_email = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/Email')
	TestObject info_fax = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/Fax')
	TestObject info_phone = findTestObject('Object Repository/Billed_AR/billed_ar_info_section/Phone')
	//financial summary section
	TestObject pay_terms = findTestObject('Object Repository/Un_Billed_AR/pay_terms')
	TestObject zero_30 = findTestObject('Object Repository/Un_Billed_AR/0_to_30_days')
	TestObject thirtyone_60 = findTestObject('Object Repository/Un_Billed_AR/31_to_60_days')
	TestObject sixtyone_90 = findTestObject('Object Repository/Un_Billed_AR/61_to_90_days')
	TestObject ninetyone_over = findTestObject('Object Repository/Un_Billed_AR/91_days_and_over')
	TestObject billed_ar = findTestObject('Object Repository/Un_Billed_AR/billed_ar')
	TestObject unbilled_loads = findTestObject('Object Repository/Un_Billed_AR/unbilled_loads')
	TestObject total = findTestObject('Object Repository/Un_Billed_AR/total')
	//chart section
	TestObject chart_zero_30 = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/chart_0-30')
	TestObject chart_zero_30_circle = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/0-30_circle')
	TestObject chart_thirty_one_60 = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/chart_31-60')
	TestObject chart_thirty_one_60_circle = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/31-60_circle')
	TestObject chart_sixty_one_90 = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/chart_61-90')
	TestObject chart_sixty_one_90_circle = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/61-90_circle')
	TestObject chart_ninety_plus = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/chart_90plus')
	TestObject chart_ninety_plus_circle = findTestObject('Object Repository/Billed_AR/billed_ar_chart_section/90plus_circle')
	//filters section - taken from the Load Search folder in Object Repository (until moved to a common location)
	TestObject clear_all = findTestObject('Object Repository/Load_Search/clear_all_button')
	TestObject apply = findTestObject('Object Repository/Load_Search/apply_button')
	TestObject po_order = findTestObject('Object Repository/Load_Search/poorder_input')
	TestObject iel_po = findTestObject('Object Repository/Load_Search/iel_po_input')
	TestObject export_csv = findTestObject('Object Repository/Billed_AR/Export_to_CSV_button')
	//loads table elements
	TestObject age = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Age')
	TestObject amount = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Amount')
	TestObject amount_paid = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Amount_Paid')
	TestObject balance = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Balance')
	TestObject credits_applied = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Credits_Applied')
	TestObject date_billed = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Date_Billed')
	TestObject debits = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Debits')
	TestObject delivery_date = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_Delivery_Date')
	TestObject iel_po_header = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_IEL_PO')
	TestObject po_order_header = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_POOrder')
	TestObject view_details = findTestObject('Object Repository/Billed_AR/billed_ar_loads_table_headers/div_View_Details')
	
	//verify he page elements on the Billed AR page
	public void billed_top_elements_verify(){
		//top part of the page
		//WebUI.verifyElementPresent(nexus_icon, 6)
		WebUI.verifyElementPresent(billed_ar_label, 6)
		WebUI.verifyElementPresent(AR_customer, 6)
		WebUI.verifyElementPresent(make_payment_btn, 6)
		WebUI.verifyElementPresent(info_cust_name, 6)
	}
	
	public void billed_info_section_verify(){
		//info section
		WebUI.verifyElementPresent(info_addrss_1, 6)
		//WebUI.verifyElementPresent(info_addrss_2, 6)
		WebUI.verifyElementPresent(info_contact, 6)
		WebUI.verifyElementPresent(info_email, 6)
		WebUI.verifyElementPresent(info_fax, 6)
		WebUI.verifyElementPresent(info_phone, 6)
	}
	
	public void billed_financial_summary_elements_verify(){
		//verify the elements in the Financial Summary section are present in the page
		WebUI.verifyElementPresent(pay_terms, 3)
		WebUI.verifyElementPresent(zero_30, 3)
		WebUI.verifyElementPresent(thirtyone_60, 3)
		WebUI.verifyElementPresent(sixtyone_90, 3)
		WebUI.verifyElementPresent(ninetyone_over, 3)
		WebUI.verifyElementPresent(billed_ar, 3)
		WebUI.verifyElementPresent(unbilled_loads, 3)
		WebUI.verifyElementPresent(total, 3)
	}
	
	public void billed_chart_section_verify(){
		WebUI.verifyElementPresent(chart_zero_30, 3)
		//WebUI.verifyElementPresent(chart_zero_30_circle, 3)
		WebUI.verifyElementPresent(chart_thirty_one_60, 3)
		//WebUI.verifyElementPresent(chart_thirty_one_60_circle, 3)
		WebUI.verifyElementPresent(chart_sixty_one_90, 3)
		//WebUI.verifyElementPresent(chart_sixty_one_90_circle, 3)
		WebUI.verifyElementPresent(chart_ninety_plus, 3)
		//WebUI.verifyElementPresent(chart_ninety_plus_circle, 3)
	}
	
	public void billed_filter_section_verify(){
		WebUI.verifyElementPresent(clear_all, 3)
		WebUI.verifyElementPresent(apply, 3)
		WebUI.verifyElementPresent(po_order, 3)
		WebUI.verifyElementPresent(iel_po, 3)
		WebUI.verifyElementPresent(export_csv, 3)
	}
	
	public void billed_loads_table_verify(){
		WebUI.verifyElementPresent(age, 3)
		WebUI.verifyElementPresent(amount, 3)
		WebUI.verifyElementPresent(amount_paid, 3)
		WebUI.verifyElementPresent(balance, 3)
		WebUI.verifyElementPresent(credits_applied, 3)
		WebUI.verifyElementPresent(date_billed, 3)
		WebUI.verifyElementPresent(debits, 3)
		WebUI.verifyElementPresent(delivery_date, 3)
		WebUI.verifyElementPresent(iel_po_header, 3)
		WebUI.verifyElementPresent(po_order_header, 3)
		WebUI.verifyElementPresent(view_details, 3)
	}
}

package billed_AR

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType
import helper_methods.Helper_Methods

public class Billed_AR_Behaviors {

	Helper_Methods js = new Helper_Methods()

	//page test objects/elements
	TestObject billed_ar_btn = findTestObject('Object Repository/Billed_AR/Billed_AR_link')
	TestObject billed_ar_label = findTestObject('Object Repository/Billed_AR/Billed_AR_label')
	TestObject view_details = findTestObject('Object Repository/Billed_AR/View_Details_button')
	TestObject billing_tools = findTestObject("Object Repository/Header/Billing_Tools_button")
	TestObject load_details_label = findTestObject('Object Repository/Load_Details/Load_Details_label')
	TestObject iel_po_input = findTestObject('Object Repository/Load_Search/iel_po_input')
	TestObject apply_btn = findTestObject('Object Repository/Load_Search/apply_button')
	TestObject iel_po_label = findTestObject('Object Repository/Billed_AR/iel_po_label')
	TestObject nexus_po_input = findTestObject("Object Repository/Load_Search/poorder_input")
	TestObject nexus_po_label = findTestObject('Object Repository/Billed_AR/nexus_po_label')
	TestObject csv_link = findTestObject('Object Repository/Billed_AR/Export_to_CSV_button')
	//age boxes
	TestObject green = findTestObject('Object Repository/Billed_AR/green_age_box')
	TestObject red = findTestObject('Object Repository/Billed_AR/red_age_box')
	TestObject blue = findTestObject('Object Repository/Billed_AR/blue_age_folder')
	TestObject orange = findTestObject('Object Repository/Billed_AR/orange_age_box')


	//Open the top right Billing Tools drop-down menu
	public void click_billing_tools(){
		WebUI.click(billing_tools)
		WebUI.delay(1)
	}

	//Select the Billed A/R from the drop-down
	public void goto_billed_ar(){
		WebUI.click(billing_tools)
		WebUI.delay(4)
		WebUI.click(billed_ar_btn)
		WebUI.delay(4)
		WebUI.verifyElementPresent(billed_ar_label, 5)
		//WebUI.waitForElementVisible(billed_ar_btn, 5)
	}

	//Click the View Details button
	public void select_billed_ar_details(){
		WebUI.click(view_details)
		WebUI.delay(2)
	}

	//verify that you landed on the Load Details page
	public void verify_load_details_page(){
		WebUI.verifyElementPresent(load_details_label, 5)
		WebUI.verifyElementVisible(load_details_label)
	}

	//Search for an IEL, iTMS PO number
	//Takes the PO as a string as the parameter
	public void filter_by_iel_po(String po){
		WebUI.setText(iel_po_input, po)
		WebUI.delay(1)
		WebUI.click(apply_btn)
		WebUI.delay(3)
	}

	//verify that the IEL PO column value is the same as the one entered for the filter after it is applied
	public void verify_iel_po_filtered(String expected_po){
		String actual_po = WebUI.getText(iel_po_label)
		WebUI.verifyMatch(actual_po, expected_po, false)
	}

	//Search for a Nexus PO number
	//Takes the PO as a string as the parameter
	public void filter_by_nexus_po(String po){
		WebUI.setText(nexus_po_input, po)
		WebUI.click(apply_btn)
		WebUI.delay(3)
	}

	//Get the Nexus PO after it's filtered, and verify that it is the PO which is passed in as the parameter
	public void verify_nexus_po_filtered(String expected_po){
		//		String actual_po = WebUI.getText(nexus_po_label)
		//		WebUI.verifyMatch(actual_po, expected_po, false)
		TestObject to1 = new TestObject("po")
		to1.addProperty("text", ConditionType.CONTAINS, expected_po)

		WebUI.verifyElementPresent(to1, 6)
	}

	//Click on the Export CSV button on the Billed AR page and download the CSV file
	public void download_csv(){
		//click to download the CSV report file
		WebUI.click(csv_link)
		//add some time for the file to fully download
		WebUI.delay(5)
	}

	//click on a specified pie slice chart, using the stroke color attribute value in the HTML
	//then verify that the colors of the boxes under the Age column don't contain the colors of the other pie slices
	public void filter_pie_chart(String pie_slice_xpath, String box_color) {

		//test object for the specified pie chart slice
		TestObject to = new TestObject("pie_slice")
		to.addProperty("fill", ConditionType.EQUALS, pie_slice_xpath)
		//filter the pie chart by clicking the slice
		//WebUI.click(to)
		js.clickUsingJS(to)
		WebUI.delay(4)

		switch(box_color) {
			//green selected
			case "background-color: rgb(220, 57, 18);":
			//verify the boxes that are not green, are not present after filtering for green
				WebUI.verifyElementNotPresent(blue, 10)
				WebUI.verifyElementNotPresent(red, 10)
				WebUI.verifyElementNotPresent(orange, 10)
				break
			//red selected
			case "background-color: rgb(16, 150, 24);":
			//verify the boxes that are not red, are not present after filtering for red
				WebUI.verifyElementNotPresent(blue, 10)
				WebUI.verifyElementNotPresent(green, 10)
				WebUI.verifyElementNotPresent(orange, 10)
				break
			//blue selected
			case "background-color: rgb(51, 102, 204);":
			//verify the boxes that are not blue, are not present after filtering for blue
				WebUI.verifyElementNotPresent(red, 10)
				WebUI.verifyElementNotPresent(green, 10)
				WebUI.verifyElementNotPresent(orange, 10)
				break
			//orange selected
			case "background-color: rgb(255, 153, 0);":
			//verify the boxes that are not orange, are not present after filtering for orange
				WebUI.verifyElementNotPresent(red, 10)
				WebUI.verifyElementNotPresent(green, 10)
				WebUI.verifyElementNotPresent(blue, 10)
				break
		}

		WebUI.delay(2)
		WebUI.refresh()
	}
	
	public void search_with_po_order_number(String number) {
		WebUI.setText(findTestObject('Object Repository/Billed_AR/po_order_input'), number)
		WebUI.click(apply_btn)
		WebUI.delay(2)
		
	}
	
	public void verify_po_order_number_not_case_sensitive(String upper_number_result) {
		TestObject to = new TestObject("order_po")
		to.addProperty("text", ConditionType.EQUALS, upper_number_result)
		
		WebUI.verifyElementPresent(to, 10)
	}
}

package load_search

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType

public class Load_Search_Behaviors {

	//page element objects declared
	public TestObject load_search = findTestObject('Object Repository/Load_Search/load_search_link')
	public TestObject filters = findTestObject('Object Repository/Load_Search/filters_text')
	public TestObject iel_po_input = findTestObject('Object Repository/Load_Search/iel_po_input')
	public TestObject load_search_label = findTestObject('Object Repository/Load_Search/Load_Search_label')
	public TestObject iel_po_value = findTestObject('Object Repository/Load_Search/iel_po_value_after_search')
	public TestObject subscribe_btn = findTestObject('Object Repository/Load_Search/Subscribe_button_first')
	public TestObject track_now_btn = findTestObject('Object Repository/Load_Search/Subscribe_Track_Now_button')
	public TestObject filter = findTestObject('Object Repository/Load_Search/status_dropdown')
	public TestObject apply = findTestObject('Object Repository/Load_Search/apply_button')
	public TestObject po_order_number = findTestObject('Object Repository/Load_Search/poorder_input')

	public void click_load_search(){
		WebUI.click(load_search)
		WebUI.delay(1)
		WebUI.waitForElementPresent(filters, 16)
		WebUI.waitForElementClickable(filters, 16)
		WebUI.waitForElementClickable(iel_po_input, 16)
	}

	public void verify_load_search_page(){
		WebUI.verifyElementPresent(load_search_label, 6)
	}

	public void iel_po_search(String iel_po){
		WebUI.delay(2)
		WebUI.setText(iel_po_input, iel_po)
		WebUI.sendKeys(iel_po_input, Keys.chord(Keys.ENTER))
		WebUI.delay(3)
	}

	public void verify_split_load_elements(String po1, String po2) {
		TestObject to1 = new TestObject("iel_po _1")
		to1.addProperty("text", ConditionType.EQUALS, po1)

		WebUI.verifyElementPresent(to1, 9)

		TestObject to2 = new TestObject("iel_po_2")
		to2.addProperty("text", ConditionType.EQUALS, po2)

		WebUI.verifyElementPresent(to2, 9)
	}

	public void verify_iel_po_input(String iel_po){
		//Get the text from the IEL PO input element, and compare it to the global IEL PO value
		//String po = WebUI.getText(iel_po_value)
		//WebUI.verifyMatch(po, iel_po, false)
		TestObject to = new TestObject("iel_po")
		to.addProperty("text", ConditionType.EQUALS, iel_po)

		WebUI.verifyElementPresent(to, 9)
	}

	//subscribe to updates for the load
	public void select_subscribe(){
		WebUI.click(subscribe_btn)
		WebUI.delay(2)
		WebUI.click(track_now_btn)
		WebUI.delay(2)
	}

	//select a filter on the side menu in load search
	//apply the selected filter
	public void apply_filter(String filter_name) {
		//select the filter based on the filter name passed-in
		switch(filter_name) {
			case "Pickup Check-In":
			//select the status filter
				WebUI.selectOptionByIndex(filter, 3)
			//apply the filter
				WebUI.click(apply)
				WebUI.delay(1)
				break;

			case "Delivery Check-In":
			//select the status filter
				WebUI.selectOptionByIndex(filter, 5)
			//apply the filter
				WebUI.click(apply)
				WebUI.delay(1)
				break;
		}
	}

	public void clear_status_filter() {
		WebUI.click(findTestObject('Object Repository/Load_Search/filter_close_btn'))
		WebUI.delay(1)
	}

	//after filtering the results, verify that the non-filtered values are not present
	public void verify_filtered_results(String filter_name) {
		//verify the non-filtered results are not present for the selected filter
		switch(filter_name) {
			case "Pickup Check-In":
				WebUI.verifyElementNotPresent(findTestObject('Object Repository/Load_Search/Delivery Check In text'), 12)
				break;

			case "Delivery Check-In":
				WebUI.verifyElementNotPresent(findTestObject('Object Repository/Load_Search/Pickup Check In text'), 12)
				break;
		}
	}
	
	public void search_with_po_order_number(String number) {
		WebUI.setText(po_order_number, number)
		WebUI.click(apply)
		WebUI.delay(2)
		
	}
	
	public void verify_po_order_number_not_case_sensitive(String upper_number_result) {
		TestObject to = new TestObject("order_po")
		to.addProperty("text", ConditionType.EQUALS, upper_number_result)
		
		WebUI.verifyElementPresent(to, 10)
	}
	
	public void verify_total_AR_present() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Search/total_AR_column'), 10)
	}
	
	public void verify_total_AR_amount(String amount) {
		String actual_amount = WebUI.getText(findTestObject('Object Repository/Load_Search/first_total_AR_amount'))
		String dollar_amount = '$' + amount
		println "actual total AR amount is: " + actual_amount
		WebUI.verifyMatch(actual_amount, dollar_amount, false)
	}
}

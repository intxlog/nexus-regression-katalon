package load_search

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

//verify the Text and color of the various statuses, which are updated through iTMS
public class Load_Search_Statuses {

	//page element objects declared
	TestObject load_search = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject filters = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject iel_po_input = findTestObject("Object Repository/Load_Search/iel_po_input")
	TestObject load_search_label = findTestObject("Object Repository/Load_Search/Load_Search_label")
	TestObject iel_po_value = findTestObject("Object Repository/Load_Search/iel_po_value_after_search")
	TestObject subscribe_btn = findTestObject("Object Repository/Load_Search/Subscribe_button_first")
	TestObject track_now_btn = findTestObject("Object Repository/Load_Search/Subscribe_Track_Now_button")
	//statuses
	TestObject delivered = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject delivery_checkin = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject pick_checkin = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject dispatched = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject loaded = findTestObject("Object Repository/Load_Search/load_search_link")
	TestObject ordered = findTestObject("Object Repository/Load_Search/load_search_link")


	//after searching for the load
	//verify the presence of the status element by text
	//verify the "Ordered" text and Yellow color
	public void verify_ordered_status() {
		WebUI.verifyElementPresent(ordered, 8)
		//get the CSS color value of the element
		def css_color = WebUI.getCSSValue(ordered, 'color')
		WebUI.verifyEqual(css_color, 'rgb(253, 203, 74)')
	}

	//after searching for the load
	//verify the presence of the status element by text
	//verify the "Dispatched" text and teal color
	public void verify_dispatched_status() {
		WebUI.verifyElementPresent(dispatched, 8)
		//get the CSS color value of the element
		def css_color = WebUI.getCSSValue(dispatched, 'color')
		WebUI.verifyEqual(css_color, 'rgb(67, 158, 151)')
	}

	//after searching for the load
	//verify the presence of the status element by text
	//verify the "Loaded" text and Purple color
	public void verify_loaded_status() {
		WebUI.verifyElementPresent(loaded, 8)
		//get the CSS color value of the element
		def css_color = WebUI.getCSSValue(loaded, 'color')
		WebUI.verifyEqual(css_color, 'rgb(102, 74, 151)')
	}

	//after searching for the load
	//verify the presence of the status element by text
	//verify the "Pickup Check-In" text and Blue color
	public void verify_pick_checkin_status() {
		WebUI.verifyElementPresent(pick_checkin, 8)
		//get the CSS color value of the element
		def css_color = WebUI.getCSSValue(pick_checkin, 'color')
		WebUI.verifyEqual(css_color, 'rgb(0, 119, 178)')
	}

	//after searching for the load
	//verify the presence of the status element by text
	//verify the "Delivery Check-In" text and Blue color
	public void verify_drop_checkin_status() {
		WebUI.verifyElementPresent(delivery_checkin, 8)
		//get the CSS color value of the element
		def css_color = WebUI.getCSSValue(delivery_checkin, 'color')
		WebUI.verifyEqual(css_color, 'rgb(0, 119, 178)')
	}

	//after searching for the load
	//verify the presence of the status element by text
	//verify the "Delivered" text and Blue color
	public void verify_delivered_status() {
		WebUI.verifyElementPresent(delivered, 8)
		//get the CSS color value of the element
		def css_color = WebUI.getCSSValue(delivered, 'color')
		WebUI.verifyEqual(css_color, 'rgb(68, 157, 80)')
	}
}

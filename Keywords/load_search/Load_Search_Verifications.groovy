package load_search

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Load_Search_Verifications {

	//page objects mostly unique to the Load Search page
	public TestObject logo = findTestObject("Object Repository/Header/top_left_logo")
	public TestObject load_summary_row = findTestObject('Object Repository/Load_Search/load_summary_row')
	public TestObject filters = findTestObject("Object Repository/Load_Search/filters_text")
	public TestObject clear_all = findTestObject("Object Repository/Load_Search/clear_all_button")
	public TestObject po_order = findTestObject("Object Repository/Load_Search/poorder_input")
	public TestObject iel_po = findTestObject("Object Repository/Load_Search/iel_po_input")
	public TestObject pick_date = findTestObject("Object Repository/Load_Search/pickup_date")
	public TestObject pick_city = findTestObject("Object Repository/Load_Search/pickup_city")
	public TestObject pick_state = findTestObject("Object Repository/Load_Search/pickup_state")
	public TestObject deliver_date = findTestObject("Object Repository/Load_Search/delivery_date")
	public TestObject delivery_city = findTestObject("Object Repository/Load_Search/delivery_city")
	public TestObject deliver_state = findTestObject("Object Repository/Load_Search/delivery_state_dropdown")
	public TestObject status = findTestObject("Object Repository/Load_Search/status_dropdown")
	public TestObject apply_button = findTestObject("Object Repository/Load_Search/apply_button")

	//verify the elements are present in the load search page
	public void load_search_elements_verify(){
		//WebUI.verifyElementPresent(logo, 5)
		WebUI.verifyElementPresent(load_summary_row, 5)
		WebUI.verifyElementPresent(filters, 5)
		WebUI.verifyElementPresent(clear_all, 5)
		WebUI.verifyElementPresent(po_order, 5)
		WebUI.verifyElementPresent(iel_po, 5)
		WebUI.verifyElementPresent(pick_date, 5)
		WebUI.verifyElementPresent(pick_city, 5)
		WebUI.verifyElementPresent(pick_state, 5)
		WebUI.verifyElementPresent(deliver_date, 5)
		WebUI.verifyElementPresent(delivery_city, 5)
		WebUI.verifyElementPresent(deliver_state, 5)
		WebUI.verifyElementPresent(status, 5)
		WebUI.verifyElementPresent(apply_button, 5)
	}

	//verify the colors of some of the side-bar elements
	public void load_search_sidebar_colors_verify(){
		//verify the white color of the side-bar Filters text
		String text_color = WebUI.getCSSValue(filters, 'color')
		WebUI.verifyMatch(text_color, 'rgba(255, 255, 255, 1)', false)

		//verify the orange text of the Clear All text
		String clear_all_color = WebUI.getCSSValue(clear_all, 'color')
		WebUI.verifyMatch(clear_all_color, 'rgba(227, 118, 28, 1)', false)
	}
}

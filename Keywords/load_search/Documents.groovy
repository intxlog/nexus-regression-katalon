package load_search

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Documents {

	public void open_documents_filter() {
		WebUI.click(findTestObject('Object Repository/Load_Search/Documents_Filters/button_Document_Filters'))
		WebUI.delay(1)
	}

	public void click_apply_filters() {
		//apply the selection
		WebUI.click(findTestObject('Object Repository/Load_Search/Documents_Filters/button_Apply'))
		WebUI.delay(1)
		//close the filter window
		WebUI.click(findTestObject('Object Repository/Load_Search/Documents_Filters/doc_close_button'))
	}

	public void select_none_filter() {
		WebUI.check(findTestObject('Object Repository/Load_Search/Documents_Filters/input_None_none'))
		//verify that the other selection options are disabled
		WebUI.verifyElementNotClickable(findTestObject('Object Repository/Load_Search/Documents_Filters/input_BOL_bol'))
		WebUI.verifyElementNotClickable(findTestObject('Object Repository/Load_Search/Documents_Filters/input_Invoice_invoice'))
	}

	public select_bol_filter() {
		WebUI.check(findTestObject('Object Repository/Load_Search/Documents_Filters/input_BOL_bol'))
		WebUI.delay(1)
	}

	public void select_invoice_filter() {
		WebUI.check(findTestObject('Object Repository/Load_Search/Documents_Filters/input_Invoice_invoice'))
		WebUI.delay(1)
	}

	public void select_bol_and_invoice_filters() {
		WebUI.check(findTestObject('Object Repository/Load_Search/Documents_Filters/input_BOL_bol'))
		WebUI.check(findTestObject('Object Repository/Load_Search/Documents_Filters/input_Invoice_invoice'))
		WebUI.delay(1)
	}

	//in the View Details page for load, verify the the Documents section is blank
	public void verify_docs_section_blank() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/documents_Empty_text'), 6)
	}

	//in the View Details page for load, verify the the Documents section includes an invoice or BOL document
	public void verify_bol_or_invoice_docs_section() {
		//if either of the two files appear, this will pass with a warning.
		//if neither of the documents is present, then it will fail
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/bol_document_text'), 10, FailureHandling.OPTIONAL) || WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/invoice_document_text'), 10, FailureHandling.OPTIONAL)) {
			println "one of the documents was present"
		}
		else {
			println "neither of the documents was present"
		}
	}

	public void verify_bol_docs_section() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/bol_document_text'), 6)
		//verify the invoice document is NOT present
		WebUI.verifyElementNotPresent(findTestObject('Object Repository/Load_Details/invoice_document_text'), 6)
	}

	public void verify_invoice_docs_section() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/invoice_document_text'), 6)

		//the NoBOL document can be present
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/bol_document_text'), 10, FailureHandling.OPTIONAL)) {
			println "NoBOL document is present"
		}
	}
}

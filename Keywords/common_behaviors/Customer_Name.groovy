package common_behaviors
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

//This class's methods verify that the correct customer name appears on the respective pages
public class Customer_Name {

	//page element objects
	public TestObject cust_dropdown = findTestObject('Object Repository/Load_Summary/customer_dropdown_load_summary')
	public TestObject cust_display_not_dropdown = findTestObject('Object Repository/Other_Common_Elements/customer_name_display')
	public TestObject cust_load_details = findTestObject('Object Repository/Load_Details/cust_name_load_details')
	public TestObject cust_ar = findTestObject('Object Repository/Billed_AR/AR_customer')

	//verify the correct customer name appears on the load section (i.e. In Transit) info page
	public void verify_load_section_customer(String customer_name){
		WebUI.delay(1)
		String cust = WebUI.getText(cust_dropdown)
		WebUI.verifyMatch(cust, customer_name, false)
	}

	//a more generic method to verify the correct customer name appears on the top of page
	//if the element is a drop down selection, verify the name
	//else if the element is not a drop down, verify the display name
	public void verify_customer_selection(String customer_name){
		if(WebUI.verifyElementPresent(cust_dropdown, 2, FailureHandling.OPTIONAL)){
			String cust_dropdown = WebUI.getText(cust_dropdown)
			WebUI.verifyMatch(cust_dropdown, customer_name, false)
		}
		else{
			String cust = WebUI.getText(cust_display_not_dropdown)
			WebUI.verifyMatch(cust, customer_name, false)
		}

	}

	//verify the correct customer name, which is passed as the parameter, on the Load Details
	public void verify_customer_load_details(String customer_name){
		WebUI.delay(1)
		String cust = WebUI.getText(cust_load_details)
		WebUI.verifyMatch(cust, customer_name, false)
	}

	//verify the correct customer name, which is passed as the parameter, on the AR page
	public void verify_customer_ar(String customer_name){
		String cust = WebUI.getText(cust_ar)
		WebUI.verifyMatch(cust, customer_name, false)
	}
}

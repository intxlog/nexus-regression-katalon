package common_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Open_Nexus {

	//Open the browser to a specified URL
	//URL string is passed as a parameter
	public static void open(String URL){
		WebUI.openBrowser(URL)
		//need this viewport size to be able to work properly in headless Chrome
		WebUI.setViewPortSize(1366, 768)
	}

	//opens the page for the emailed password reset page
	public static void open_pass_reset_page(){
		//navigate to generic password reset page
		WebUI.openBrowser('https://cupo-qa.web.app/?email=qatesting@intxlog.com&pin=1469&action=reset')
		WebUI.setViewPortSize(1366, 768)
	}

	//opens the URL which is passed as a parameter - the password reset URL from the email
	public static void open_pass_reset_url(String url){
		//navigate to generic password reset page
		WebUI.openBrowser(url)
		WebUI.setViewPortSize(1366, 768)
	}

	//open the itms login page
	public static void open_itms(){
		//QA itms page
		WebUI.openBrowser('https://qa.intxlog.com/')
		WebUI.setViewPortSize(1366, 768)
	}
}

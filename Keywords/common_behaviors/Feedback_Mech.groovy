package common_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Feedback_Mech {

	public void click_feedback_mech() {
		WebUI.click(findTestObject('Object Repository/Other_Common_Elements/feedback_button'))
	}

	//enter feedback, with a randomized string
	//return the randomized string
	public String enter_feedback() {
		//select option from the recommendation drop-down
		WebUI.selectOptionByIndex(findTestObject('Object Repository/Other_Common_Elements/recommendation_dropdown'), 1)

		//get random integer
		Integer rand_int = Math.abs(new Random().nextInt() % 1000000) + 1000
		String rand_str = rand_int.toString()
		//enter the randomized feedback string into the feedback input
		String feedback_str = "test feedback" + rand_str
		WebUI.setText(findTestObject('Object Repository/Other_Common_Elements/feedback_input'), feedback_str)

		//submit the feedback
		WebUI.click(findTestObject('Object Repository/Other_Common_Elements/submit_feedback_btn'))

		return feedback_str
	}
}

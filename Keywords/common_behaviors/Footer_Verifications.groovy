package common_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Footer_Verifications {

	//footer page elements
	public TestObject nexus_logo_footer = findTestObject('Object Repository/Footer/nexus_logo_footer')
	public TestObject x_marks_spot = findTestObject('Object Repository/Footer/X_Marks_The_Spot_footer')
	public TestObject contact = findTestObject('Object Repository/Footer/Contact_label_footer')
	public TestObject nexus_support = findTestObject('Object Repository/Footer/nexus_support_label_footer')
	public TestObject facebook = findTestObject('Object Repository/Footer/Feedback_footer')
	public TestObject twitter = findTestObject('Object Repository/Footer/twitter_link_footer')
	public TestObject iel_logo_footer = findTestObject('Object Repository/Footer/iel_logo_footer')
	public TestObject copyright = findTestObject('Object Repository/Footer/copyright_footer')
	public TestObject privacy = findTestObject('Object Repository/Footer/Privacy_Policy_footer')
	public TestObject terms_of_use = findTestObject('Object Repository/Footer/Terms_of_Use_footer')
	public TestObject feedback = findTestObject('Object Repository/Footer/Feedback_footer')
	public TestObject version = findTestObject('Object Repository/Footer/Version_footer')

	public void footer_verifications(){
		//verify footer elements are present
		WebUI.verifyElementPresent(nexus_logo_footer, 4)
		WebUI.verifyElementPresent(x_marks_spot, 4)
		WebUI.verifyElementPresent(contact, 4)
		WebUI.verifyElementPresent(nexus_support, 4)
		WebUI.verifyElementPresent(facebook, 4)
		WebUI.verifyElementPresent(twitter, 4)
		WebUI.verifyElementPresent(iel_logo_footer, 4)
		WebUI.verifyElementPresent(copyright, 4)
		WebUI.verifyElementPresent(privacy, 4)
		WebUI.verifyElementPresent(terms_of_use, 4)
		WebUI.verifyElementPresent(feedback, 4)
		WebUI.verifyElementPresent(version, 4)
	}
}

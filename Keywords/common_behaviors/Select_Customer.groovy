package common_behaviors
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys as Keys
import internal.GlobalVariable

public class Select_Customer {

	//page element objects
	public static TestObject cust_input = findTestObject('Object Repository/Load_Summary/customer_input')
	public static TestObject cust_dropdown = findTestObject('Object Repository/Load_Summary/customer_dropdown_load_summary')
	public static TestObject cust_checkmark = findTestObject('Object Repository/Load_Summary/customer_check_mark')
	public static TestObject cust_input_2 = findTestObject('Object Repository/Load_Summary/customer_input_2')


	//Select the customer from the customer drop-down
	//The customer is passed as a string parameter, and can be taken from the profile/global variables
	public static void select_customer(String customer){
		//try to select the customer, if there's more than one to select, using the drop-down
		//** There will be a yellow WARNING message in the log view
		//and there will be element not found errors in the Console log
		//these are expected if the customer drop-down is not present, and are NOT valid failures
		WebUI.delay(1)
		if (WebUI.verifyElementPresent(cust_dropdown, 2, FailureHandling.OPTIONAL)){
			WebUI.click(cust_dropdown)
			WebUI.delay(1)
			WebUI.setText(cust_input, customer)
			//Send the Enter key to enter the selected customer
			WebUI.sendKeys(cust_input, Keys.chord(Keys.ENTER))
			WebUI.delay(1)
			//need to click the green check mark to select the customer
			//WebUI.click(cust_checkmark)
			//TODO - see what other waiting methods can be used instead of delay
			WebUI.delay(4)
		}
		else{
			println "customer drop-down is not present"
		}
	}

	//select customer with a different customer input id (i.e. found on load request disclaimer page)
	//TODO : possibly put try-catch in original method, for the other input element, if the first one isn't found.
	public static void select_customer_disclaimer(String customer){
		if (WebUI.verifyElementPresent(cust_dropdown, 2, FailureHandling.OPTIONAL)){
			WebUI.click(cust_dropdown)
			WebUI.delay(1)
			WebUI.setText(cust_input_2, customer)
			//Send the Enter key to enter the selected customer
			WebUI.sendKeys(cust_input_2, Keys.chord(Keys.ENTER))
			WebUI.delay(1)
			//need to click the green check mark to select the customer
			//WebUI.click(cust_checkmark)
			//TODO - see what other waiting methods can be used instead of delay
			WebUI.delay(4)
		}
		else{
			println "customer drop-down is not present"
		}
	}

}

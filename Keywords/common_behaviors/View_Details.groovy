package common_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class View_Details {

	//page element objects
	public static TestObject view_details = findTestObject('Object Repository/Load_Summary/View_Details_button')
	public static TestObject po_num = findTestObject('Object Repository/Load_Summary/first_po_link')
	public static TestObject view_details_po_num = findTestObject('Object Repository/Load_Summary/view_details_po_label')
	public static TestObject load_det_label = findTestObject('Object Repository/Load_Details/Load_Details_label')

	//get the first PO in the View Details page and return the PO as a string
	public static String get_first_po(){
		//get the PO number from the PO link
		String first_po = WebUI.getText(po_num)
		return first_po
	}

	public static void click_view_load_details(){
		//click a View Details button
		WebUI.click(view_details)
		//loading the load details page is often slow, and may need the delay
		WebUI.delay(10)
	}

	//get the PO displayed on the View Details page, and return the PO string
	private static String get_view_details_po(){
		String details_po = WebUI.getText(view_details_po_num)
		return details_po
	}

	//verify that two PO numbers match, which are passed as string parameters
	public static void verify_load_details_po(String po_1, String po_2){
		WebUI.verifyMatch(po_1, po_2, false)
	}
}

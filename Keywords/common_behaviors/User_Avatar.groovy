package common_behaviors

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class User_Avatar {

	TestObject avatar = findTestObject('Object Repository/Header/User_Drop_Down')
	TestObject load_summ_label = findTestObject('Object Repository/Load_Summary/Load_Summary_label')
	TestObject logout = findTestObject('Object Repository/Header/User_Avatar_Dropdown/avatar_log_out')
	
	//click the user login avatar at the upper right of the page, to display the drop-down selection
	public void click_user_avatar(){
		//delay - the top row elements take time to load
		WebUI.delay(2)
		WebUI.click(avatar)
	}
	
	//click outside the avatar menu to close it and verify the log out link is not click-able
	public void click_outside_avatar(){
		//click the load summary label
		WebUI.click(load_summ_label)
	}
}

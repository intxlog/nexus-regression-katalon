package common_behaviors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Nexus_Login {

	//possible to put in test case listener/hook
	//login to Nexus with testing email and password
	public static void login(String email, String pass){
		//get cupo / nexus testing user name from the db_data file
		//		def data = findTestData("Data Files/db_data")
		//		email = data.getValue(1, 4)
		//		pass = data.getValue(2, 4)

		WebUI.setText(findTestObject('LogIn_IEL_Nexus/email_address_input'), email)
		WebUI.setEncryptedText(findTestObject('LogIn_IEL_Nexus/password_input'), pass)
		WebUI.click(findTestObject('LogIn_IEL_Nexus/sign_in_button'))
		//Experimental:
		//wait for the first page to load for up to 60 seconds - sometimes it is slow to load
		WebUI.waitForPageLoad(60)
	}

	//Log out of Nexus
	public static void logout(){
		//click the user login avatar at the upper right of the page, to display the drop-down selection
		//delay because the top row elements take time to be present
		WebUI.delay(2)
		//have to click the user drop down twice for some reason
		//WebUI.click(findTestObject('Object Repository/Header/User_Drop_Down'))
		WebUI.click(findTestObject('Object Repository/Header/User_Drop_Down'))
		//click the Log Out option
		WebUI.click(findTestObject('Object Repository/Header/User_Avatar_Dropdown/avatar_log_out'))
		WebUI.delay(1)
		//verify user landed on the Welcome log in page
		WebUI.verifyElementPresent(findTestObject('Object Repository/LogIn_IEL_Nexus/Welcome_text'), 6)
	}

	//login to iTMS with all permissions - user name and password from data file are passed as parameters
	public static void itms_login(String login, String pass){
		//get itms user name from the db_data file
		//def data = findTestData("Data Files/db_data")
		//login = data.getValue(1, 3)
		//pass = data.getValue(2, 3)

		WebUI.setText(findTestObject('Object Repository/Page_iTMS  Login/itms_username_input'), login)
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_iTMS  Login/itms_password_input'), pass)
		WebUI.click(findTestObject('Object Repository/Page_iTMS  Login/itms_login_btn'))
	}
}

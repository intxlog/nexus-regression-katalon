package general_misc_tests
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Payment_History {

	//page element objects
	public TestObject pay_hist = findTestObject('Object Repository/Header/Pay_History_btn')
	public TestObject user_dropdown = findTestObject('Object Repository/Header/User_Drop_Down')
	public TestObject csv_link = findTestObject('Object Repository/Payment_History/export_csv')
	public TestObject pay_hist_labl = findTestObject('Object Repository/Payment_History/Payment_History_label')
	public TestObject amount_paid = findTestObject('Object Repository/Payment_History/Amount_Paid')
	public TestObject cust_po = findTestObject('Object Repository/Payment_History/Customer_PO')
	public TestObject date_paid = findTestObject('Object Repository/Payment_History/Date_Paid')
	public TestObject iel_po = findTestObject('Object Repository/Payment_History/IEL_PO_Number')
	public TestObject invoice_date = findTestObject('Object Repository/Payment_History/Invoice_Date')

	public void select_pay_history(){
		WebUI.click(user_dropdown)
		WebUI.delay(1)
		WebUI.click(pay_hist)
		WebUI.delay(1)
	}

	//Click the Export CSV link and download the csv file
	public void export_pay_history(){
		WebUI.click(csv_link)
		//delay to let the file to download
		WebUI.delay(6)
	}

	//verify the presence of the Pay History page elements
	public void verify_pay_hist_elements(){
		WebUI.verifyElementPresent(pay_hist, 5)
		WebUI.verifyElementPresent(user_dropdown, 5)
		WebUI.verifyElementPresent(csv_link, 5)
		WebUI.verifyElementPresent(pay_hist_labl, 5)
		WebUI.verifyElementPresent(amount_paid, 5)
		WebUI.verifyElementPresent(cust_po, 5)
		WebUI.verifyElementPresent(date_paid, 5)
		WebUI.verifyElementPresent(iel_po, 5)
		WebUI.verifyElementPresent(invoice_date, 5)
	}
}

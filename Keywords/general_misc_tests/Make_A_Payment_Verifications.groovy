package general_misc_tests

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Make_A_Payment_Verifications {

	//top page elements
	public TestObject previous_page = findTestObject('Other_Common_Elements/Previous_Page_button')
	public TestObject pay_history = findTestObject("Object Repository/Make_A_Payment/Pay_History_btn")
	public TestObject prepay_load = findTestObject("Object Repository/Make_A_Payment/Prepay_Load_button")
	public TestObject pay_now = findTestObject("Object Repository/Make_A_Payment/Pay_Now_button")
	public TestObject invoice_total = findTestObject('Object Repository/Make_A_Payment/Invoice_Total')
	//make a payment section column elements
	public TestObject amount_paid = findTestObject('Object Repository/Make_A_Payment/Columns/Amount_Paid')
	public TestObject amount_to_pay = findTestObject('Object Repository/Make_A_Payment/Columns/Amount_to_Pay')
	public TestObject customer_po = findTestObject('Object Repository/Make_A_Payment/Columns/Customer_PO')
	public TestObject days_old = findTestObject('Object Repository/Make_A_Payment/Columns/Days_Old')
	public TestObject iel_po = findTestObject('Object Repository/Make_A_Payment/Columns/IEL_PO_Number')
	public TestObject invoice_date = findTestObject('Object Repository/Make_A_Payment/Columns/Invoice_Date')
	public TestObject outstanding_balance = findTestObject('Object Repository/Make_A_Payment/Columns/Outstanding_Balance')
	public TestObject status = findTestObject('Object Repository/Make_A_Payment/Columns/Status')
	public TestObject total_balance = findTestObject('Object Repository/Make_A_Payment/Columns/Total_Balance')

	//verify the presence of the elements at the top of the Make a Payment page
	public void top_elements_verify(){
		WebUI.verifyElementPresent(previous_page, 6)
		WebUI.verifyElementPresent(pay_history, 6)
		//remove prepay buton per https://intxlog.atlassian.net/browse/CUPO-550
		//WebUI.verifyElementPresent(prepay_load, 6)
		WebUI.verifyElementPresent(pay_now, 6)
		WebUI.verifyElementPresent(invoice_total, 6)
	}

	//verify the presence of the column names on the Make a Payment page
	public payment_columns_verify(){
		WebUI.verifyElementPresent(amount_paid, 6)
		WebUI.verifyElementPresent(amount_to_pay, 6)
		WebUI.verifyElementPresent(customer_po, 6)
		WebUI.verifyElementPresent(days_old, 6)
		WebUI.verifyElementPresent(iel_po, 6)
		WebUI.verifyElementPresent(invoice_date, 6)
		WebUI.verifyElementPresent(outstanding_balance, 6)
		WebUI.verifyElementPresent(status, 6)
		WebUI.verifyElementPresent(total_balance, 6)
	}
}

package general_misc_tests

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Make_A_Payment_Behaviors {

	//page element objects
	public TestObject make_payment_btn = findTestObject('Object Repository/Load_Summary/Make_a_payment_button')
	public TestObject customer_po = findTestObject("Object Repository/Make_A_Payment/customer_po_first")
	public TestObject load_details_label = findTestObject("Object Repository/Load_Details/Load_Details_label")
	public TestObject pay_now = findTestObject("Object Repository/Make_A_Payment/Pay_Now_button")
	public TestObject first_include_payment = findTestObject('Object Repository/Make_A_Payment/first_include_payment_checkbox')
	public TestObject second_include_payment = findTestObject('Object Repository/Make_A_Payment/second_include_payment_checkbox')
	public TestObject first_overpayment = findTestObject('Object Repository/Make_A_Payment/first_unpaid_invoice_input')
	public TestObject second_overpayment = findTestObject('Object Repository/Make_A_Payment/second_unpaid_invoice_input')
	//	TestObject popup_error_2 = findTestObject("Object Repository/Make_A_Payment/popup_There are 2 invoices that is invalid or missing information")
	//	TestObject popup_error_1 = findTestObject("Object Repository/Make_A_Payment/popup_There is 1 invoice that is invalid or missing information")
	public TestObject ok_btn = findTestObject("Object Repository/Make_A_Payment/error_popup_Ok_btn")
	public TestObject reason_1 = findTestObject('Object Repository/Make_A_Payment/first_reason_for_payment_input')
	public TestObject reason_2 = findTestObject('Object Repository/Make_A_Payment/second_reason_for_payment_input')
	public TestObject popup_2_errors = findTestObject("Object Repository/Make_A_Payment/popup_There are 2 invoices that is invalid or missing information")
	public TestObject popup_1_error = findTestObject("Object Repository/Make_A_Payment/popup_There is 1 invoice that is invalid or missing information")
	//prepay load elements
	public TestObject prepay_btn = findTestObject("Object Repository/Make_A_Payment/Prepay_Load_button")
	public TestObject prepay_popup = findTestObject('Object Repository/Make_A_Payment/prepay_load_popup')
	public TestObject prepay_description = findTestObject("Object Repository/Make_A_Payment/prepay_payment_description")
	public TestObject prepay_amount = findTestObject("Object Repository/Make_A_Payment/prepay_payment_amount")
	public TestObject payeezy_text = findTestObject("Object Repository/Make_A_Payment/payeezy_footer_text")
	public TestObject pay_invoice_btn = findTestObject("Object Repository/Make_A_Payment/prepay_Pay_Invoice_btn")

	public void select_make_a_payment(){
		WebUI.click(findTestObject('Object Repository/Header/User_Drop_Down'))
		WebUI.click(findTestObject('Object Repository/Header/User_Avatar_Dropdown/avatar_pay_now'))
		WebUI.delay(4)
		//click outside the drop-down to close it
		WebUI.click(findTestObject('Object Repository/Make_A_Payment/Make_A_Payment_label'))
	}

	public void select_customer_po(){
		WebUI.click(customer_po)
		WebUI.delay(3)
	}

	public void click_pay_now(){
		WebUI.click(pay_now)
		WebUI.delay(1)
	}

	public void verify_load_details_page(){
		WebUI.verifyElementPresent(load_details_label, 10)
	}

	public void select_first_include_payment(){
		WebUI.check(first_include_payment)
	}

	public void deselect_first_incl_payment(){
		WebUI.uncheck(first_include_payment)
	}

	public void select_second_include_payment(){
		WebUI.check(second_include_payment)
	}

	public void add_first_overpayment(){
		WebUI.setText(first_overpayment, "5000")
	}

	public void add_second_overpayment(){
		WebUI.setText(second_overpayment, "5000")
	}

	public void verify_first_overpayment(){
		//get the text from he first payment input and compare it to the set text from above in add_first_overpayment()
		String actual_payment = WebUI.getText(first_overpayment)
		WebUI.verifyMatch(actual_payment, "5000", false)
	}

	public void verify_popup_2_errors(){
		//verify that the error pop up with Two errors is present
		WebUI.verifyElementPresent(popup_2_errors, 6)
	}

	public void verify_popup_1_error(){
		//verify that the error pop up with One errors is present
		WebUI.verifyElementPresent(popup_1_error, 6)
	}

	public void verify_error_popup_not_present(){
		WebUI.verifyElementNotPresent(popup_1_error, 6)
		WebUI.verifyElementNotPresent(popup_2_errors, 6)
	}

	public void verify_reason_red_border(){
		//get the border-color CSS value
		String cssval = WebUI.getCSSValue(reason_1, 'border-color')
		println "reason box border in red CSS value = " + cssval
		//verify match for the CSS value
		WebUI.verifyMatch(cssval, "rgb(238, 39, 39)", false)
	}

	public void verify_reason_no_border(){
		//verify the border when text is entered into the reason box
		String cssval = WebUI.getCSSValue(reason_1, 'border-color')
		println "reason box border without border = " + cssval
		//verify match for the CSS value
		WebUI.verifyMatch(cssval, "rgb(101, 101, 101)", false)
	}

	public void enter_reason_1(){
		WebUI.setText(reason_1, "reason 1")
	}

	public void enter_reason_2(){
		WebUI.setText(reason_2, "reason 2")
	}

	public void verify_notes_box_removed(){
		//after the notes ox is de-selected, it should no longer be visible
		WebUI.verifyElementNotPresent(reason_1, 4)
	}

	public void close_popup(){
		WebUI.click(ok_btn)
		WebUI.delay(1)
	}

	public void select_prepay_load(){
		WebUI.click(prepay_btn)
		WebUI.delay(1)
	}

	public void verify_prepay_popup(){
		WebUI.verifyElementPresent(prepay_popup, 10)
	}

	public void enter_prepay_values(String po){
		WebUI.setText(prepay_description, po)
		WebUI.setText(prepay_amount, "500")
		WebUI.click(pay_invoice_btn)
		WebUI.delay(1)
	}

	//verifies the wanted footer text, when the Payeezy page loads
	public void verify_payeezy_text(){
		WebUI.verifyElementPresent(payeezy_text, 10)
	}
}

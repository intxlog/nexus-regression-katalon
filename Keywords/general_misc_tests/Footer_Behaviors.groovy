package general_misc_tests

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Footer_Behaviors {

	//page element objects
	TestObject facebook = findTestObject('Object Repository/Footer/facebook_link_footer')
	TestObject twitter = findTestObject('Object Repository/Footer/twitter_link_footer')
	TestObject nexus_support = findTestObject('Object Repository/Footer/nexus_support_label_footer')
	TestObject priv_pol_link = findTestObject('Object Repository/Footer/Privacy_Policy_footer')
	TestObject priv_pol_labl = findTestObject('Object Repository/Footer/Privacy_Policy_footer')
	TestObject priv_pol_content = findTestObject('Object Repository/Footer/Privacy_Policy_footer')
	TestObject terms_of_use_link = findTestObject('Object Repository/Footer/Terms_of_Use_footer')
	TestObject feedback = findTestObject('Object Repository/Footer/Feedback_footer')
	TestObject version = findTestObject('Object Repository/Footer/Version_footer')

	//click the Facebook logo link and verify that the correct page opens
	public void click_facebook(){
		WebUI.click(facebook)
		WebUI.delay(2)
		//switch to the new tab
		WebUI.switchToWindowIndex(1)
		//get the URL of the current window
		//String url = WebUI.getUrl()
		//verify that the tab has the correct Facebook URL
		//WebUI.verifyMatch(url, "https://www.facebook.com/pg/ielfreight/", false)
		//switch back to the first tab
		WebUI.switchToWindowIndex(0)
	}

	public void twitter(){
		WebUI.click(twitter)
		WebUI.delay(2)
		//switch to the new tab
		WebUI.switchToWindowIndex(2)
		//get the URL of the current window
		String url = WebUI.getUrl()
		//verify that the tab has the correct Facebook URL
		WebUI.verifyMatch(url, "https://twitter.com/iel_llc/", false)
		//switch back to the first tab
		WebUI.switchToWindowIndex(0)
	}

	//verify the link for the ielfright mail link
	public void verify_email_link(){
		// get the link href value and verify
		String href = WebUI.getAttribute(nexus_support, 'href')
		println href
		WebUI.verifyMatch(href, "mailto:nexus@ielfreight.com", false)
	}

	//click privacy policy and the go back to the previous page
	public void privacy_policy(){
		WebUI.click(priv_pol_link)
		WebUI.verifyElementPresent(priv_pol_labl, 5)
		WebUI.verifyElementPresent(priv_pol_content, 5)
		//return to the previous page
		WebUI.back()
		WebUI.delay(1)
	}
	
	//click privacy and stay on page
	public void click_privacy_policy() {
		WebUI.delay(2)
		WebUI.click(priv_pol_link)
		WebUI.waitForElementPresent(priv_pol_labl, 5)
	}

	public void terms_of_use(){
		String href = WebUI.getAttribute(terms_of_use_link, 'href')
		WebUI.verifyMatch(href, "https://www.ielfreight.com/wp-content/uploads/2019/10/IEL-Standard-Terms-of-Use.pdf", false)
	}

	public void feedback(){
		String href = WebUI.getAttribute(feedback, 'href')
		WebUI.verifyMatch(href, "mailto:feedback@ielfreight.com", false)
	}

	//verify the match of the dev or qa url for the version
	public void version(String dev_or_qa_url){
		if(dev_or_qa_url == "dev" || dev_or_qa_url == "DEV"){
			WebUI.click(version)
			//get the URL of the current window
			String url = WebUI.getUrl()
			//verify that the tab has the correct versions page URL
			WebUI.verifyMatch(url, "https://cupo-dev-bb7ef.web.app/release", false)
		}

		else if(dev_or_qa_url == "qa" || dev_or_qa_url == "QA"){
			WebUI.click(version)
			//get the URL of the current window
			String url = WebUI.getUrl()
			//verify that the tab has the correct versions page URL
			WebUI.verifyMatch(url, "https://cupo-qa.web.app/release", false)
		}

		else if(dev_or_qa_url == "uat" || dev_or_qa_url == "UAT"){
			WebUI.click(version)
			//get the URL of the current window
			String url = WebUI.getUrl()
			//verify that the tab has the correct versions page URL
			WebUI.verifyMatch(url, "https://cupo-uat-4a726.web.app/release", false)
		}
	}
}

package general_misc_tests

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Back_Button {

	//page element objects
	public TestObject previous_page_btn = findTestObject("Other_Common_Elements/Previous_Page_button")
	public TestObject load_summary_label = findTestObject("Object Repository/Load_Summary/Load_Summary_label")
	public TestObject make_payment_label = findTestObject("Object Repository/Make_A_Payment/Make_A_Payment_label")

	public void click_previous_page_button(){
		WebUI.click(previous_page_btn)
		WebUI.delay(4)
	}

	public void verify_load_summary_page(){
		WebUI.verifyElementPresent(load_summary_label, 6)
	}

	public void verify_make_a_payment_page(){
		WebUI.verifyElementPresent(make_payment_label, 6)
	}
}

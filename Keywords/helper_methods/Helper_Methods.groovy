package helper_methods

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.openqa.selenium.Keys as Keys

//Class to hold common, Non-Functional methods
public class Helper_Methods {
	
	public void clickUsingJS(TestObject to) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element = WebUiCommonHelper.findWebElement(to, 30)
		JavascriptExecutor executor = ((driver) as JavascriptExecutor)
		executor.executeScript('arguments[0].click()', element)
	}
	
	/**
	 * Search in a specified folder on the desktop for a specified file.
	 * e.g. Use when downloading a file from the application, then verify that the file is present in the downloads folder
	 *
	 * @param String for download file path
	 * @param String for file name to search for
	 * @return boolean flag - true if the file is found, and false if not found
	 */
	public boolean is_file_downloaded(String fileName){
		//get home user directory
		String user_dir = System.getProperty('user.home')
		//get Downloads folder path
		String download_path = user_dir + '\\Downloads'

		boolean flag = false

		File dir = new File(download_path)

		File[] dir_contents = dir.listFiles()

		println('Total Files Available in the folder are :  ' + dir_contents.length)

		for (int i = 0; i < dir_contents.length; i++) {
			println('File Name at 0 is : ' + dir_contents[i].getName())

			if (dir_contents[i].getName().equals(fileName)) {
				//delete the file after it's found
				//(running the test multiple times will always pass if the file is not deleted)
				dir_contents[i].delete()
				//custom test step pass message
				KeywordUtil.markPassed(fileName + 'is present in' + download_path + 'and was deleted after it was found')
				return flag = true
			}
		}
		//return false if the file is not found
		return flag
	}

	/**
	 * Get Today's date using the Groovy date class
	 *
	 * @return String of today's date formatted in 'M/d/yy'
	 */

	public String today_date(){
		//Get today's date in the M/d/yy format
		Date today = new Date()
		String today_string = today.format('M/d/yy')
		return today_string
	}

	//get the longer format of today's date
	public String today_date_long(){
		//Get today's date in the M/d/yy format
		Date today = new Date()
		String today_string = today.format('M/d/yyyy')
		return today_string
	}

	/**
	 * Get Today's date and then add two days to it to get the future date, using the Groovy date class
	 *
	 * @return String of date two days from today's date formatted in 'M/d/yy'
	 */
	public String future_date(){
		//get today's date and add two days to it
		Date future_day = new Date().plus(2)
		String future_day_string = future_day.format('M/d/yy')
		return future_day_string
	}

	/**
	 * Get the date for 6 days ago from today
	 *
	 * @return String of date two days from today's date formatted in 'M/d/yy'
	 */
	//TODO : add methods that will take in parameters for varying integer day amounts in the future or past
	public String past_date(){
		Date today = new Date().minus(4)
		String today_string = today.format('M/d/yy')
		return today_string
	}

	/**
	 * Get the current timestamp, using the Groovy date class
	 *
	 * @return String of time formatted in "HH:mm"
	 */
	public String get_time(){
		Date time_object = new Date()
		String time = time_object.format("H:mm:ss a")
		return time
	}

	/**
	 * Clear text from an input field
	 *
	 * @param TestObject : page element test object with text that needs to be cleared
	 */
	public void clear_text(TestObject to){
		//send keys to the test object for Ctrl + a
		WebUI.sendKeys(to, Keys.chord(Keys.CONTROL, 'a'))
		WebUI.sendKeys(to, Keys.chord(Keys.BACK_SPACE))
	}
}

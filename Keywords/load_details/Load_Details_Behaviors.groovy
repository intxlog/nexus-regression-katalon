package load_details

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.By

public class Load_Details_Behaviors {

	//page test objects
	public TestObject load_det_label = findTestObject('Object Repository/Load_Details/Load_Details_label')
	public TestObject view_details = findTestObject('Object Repository/Load_Details/view_details_button')
	public TestObject last_view_details = findTestObject('Object Repository/Future_Loads/last_view_details_btn')
	public TestObject pick_num = findTestObject('Object Repository/Load_Details/pick_1_of_2')
	public TestObject drop_num = findTestObject('Object Repository/Load_Details/drop_1_of_2')
	//broker notes
	public TestObject broker_notes_text = findTestObject('Object Repository/Load_Details/Broker_Notes_header')
	public TestObject notes_section = findTestObject('Object Repository/Load_Details/Notes_broker')
	//find broker notes by xpath - used for checking the ordering of > one note
	public TestObject broker_notes_1 = findTestObject('Object Repository/Load_Details/broker_note_text_1')
	public TestObject broker_notes_2 = findTestObject('Object Repository/Load_Details/broker_note_text_2')
	//find the broker notes each by text instead of xpath - used to only check presence of element, NOT ordering
	public TestObject broker_notes_1_by_text = findTestObject('Object Repository/Load_Details/broker_note_1_by_text')
	public TestObject broker_notes_2_by_text = findTestObject('Object Repository/Load_Details/broker_note_2_by_text')
	public TestObject show_more_button = findTestObject('Object Repository/Load_Details/show_more_btn')

	//string for nexus notes - **same as used in the Loadview_Behaviors class
	//TODO : add the notes as a global variable
	String notes_1 = "Note:  nexus notes 1"
	String notes_2 = "Note:  nexus notes 2"
	//TODO : put in profiles
	String notes_3 = "nexus_note_3 !!#^&!!"

	public void verify_load_details_label(){
		WebUI.verifyElementPresent(load_det_label, 4)
		//verify text is correct for the label
		String label = WebUI.getText(load_det_label)
		WebUI.verifyMatch(label, 'Load Details', false)
	}

	//click the first View Details button
	public void click_view_details(){
		//top View Details option
		WebUI.click(view_details)
		WebUI.waitForElementPresent(load_det_label, 5)
	}

	//click the last of the View Details button on the page
	public void click_last_view_details(){
		//find the View Details button with the last index of the set of the view details buttons
		WebUI.delay(2)
		//find the size of the set of View Details buttons on  the page
		WebDriver driver = DriverFactory.getWebDriver()
		def eleCount = driver.findElements(By.xpath("//*[@class = 'styles-module__primary___2rSQt styles-module__small___3TpxN' and (text() = 'View Details' or . = 'View Details')]")).size()
		//println eleCount

		//xpath with the element count from above as the final index
		String btn_xpath = "(//a[contains(text(),'View Details')])" + "[" + eleCount + "]"
		//println btn_xpath

		//create a dynamic test object for the View Details button with an index
		TestObject to = new TestObject("view_dets_btn")

		//add the notes text property to the custom test object
		to.addProperty("xpath", ConditionType.EQUALS, btn_xpath)

		//click the last View Details option
		WebUI.click(to)
		WebUI.waitForElementPresent(load_det_label, 6)
	}

	public void verify_pick_and_drop_number(){
		WebUI.verifyElementPresent(pick_num, 3)
		WebUI.verifyElementPresent(drop_num, 3)
	}

	public void verify_broker_notes_section_is_present(){
		WebUI.verifyElementPresent(broker_notes_text, 5)
		WebUI.verifyElementPresent(notes_section, 5)
	}

	public void verify_broker_notes_section_not_present(){
		WebUI.verifyElementNotPresent(broker_notes_text, 5)
	}

	//verify the notes text for the First broker notes is not present on the page - * xpath
	public void verify_first_broker_note_not_present(){
		WebUI.verifyElementNotPresent(broker_notes_1, 5)
	}

	//verify the notes text for the Second broker notes is not present on the page - * xpath
	public void verify_second_broker_note_not_present(){
		WebUI.verifyElementNotPresent(broker_notes_2, 5)
	}

	//verify the first broker notes by text is not present
	public void verify_first_broker_note_text_not_present(){
		WebUI.verifyElementNotPresent(broker_notes_1_by_text, 5)
	}

	//verify the second broker notes by text is not present
	public void verify_second_broker_note_text_not_present(){
		WebUI.verifyElementNotPresent(broker_notes_2_by_text, 5)
	}

	//when there is more than one notes in the broker notes section, the "Show more" button needs
	//to clicked to show the second note
	public void click_show_more(){
		//if the show more button element is present, click it, else do nothing
		if(WebUI.verifyElementPresent(show_more_button, 6)){
			WebUI.click(show_more_button)
			WebUI.delay(1)
		}
		else{
			println "Show more button is not present"
		}
	}

	//Verify that the Nexus notes check call text matches the text on the load details page
	public void verify_broker_notes_text(){
		//method to verify notes text AND ordering (relative position) - but the xpaths may be flakey
		//		String actual_note_1 = WebUI.getText(broker_notes_1)
		//		WebUI.verifyMatch(actual_note_1, "Note: " + notes_1, false)

		//one possible way to verify broker notes text - does NOT verify ordering:
		//dynamic test object, which can change
		TestObject to = new TestObject("notes_text")
		//add the notes text property to the custom test object
		to.addProperty("text", ConditionType.CONTAINS, notes_1)
		//verify the element with the notes text is present
		WebUI.verifyElementPresent(to, 5)
	}

	public void verify_second_broker_notes_text(){
		//method to verify notes text AND ordering (relative position) - but the xpaths may be flaky
		//		String actual_note_2 = WebUI.getText(broker_notes_2)
		//		WebUI.verifyMatch(actual_note_2, "Note: " + notes_2, false)

		//Need to click the Show more button first
		WebUI.delay(1)
		WebUI.click(show_more_button)
		//method to verify broker notes text - does NOT verify ordering:
		//dynamic test object, which can change
		TestObject to = new TestObject("notes_text_2")
		//add the notes text property to the custom test object
		to.addProperty("text", ConditionType.CONTAINS, notes_2)
		//verify the element with the notes text is present
		WebUI.verifyElementPresent(to, 5)
	}

	public void verify_broker_notes_text_special_chars(){
		//method to verify notes text AND ordering (relative position) - but the xpaths may be flaky
		//		String actual_note_2 = WebUI.getText(broker_notes_2)
		//		WebUI.verifyMatch(actual_note_2, "Note: " + notes_2, false)

		//Need to click the Show more button first
		//WebUI.click(show_more_button)
		//method to verify broker notes text - does NOT verify ordering:
		//dynamic test object, which can change
		TestObject to = new TestObject("notes_text_2")
		//add the notes text property to the custom test object
		to.addProperty("text", ConditionType.EQUALS, notes_3)
		//verify the element with the notes text is present
		WebUI.verifyElementPresent(to, 5)
	}
}


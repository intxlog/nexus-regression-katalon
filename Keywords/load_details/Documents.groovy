package load_details

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType

public class Documents {

	//add a document in the Documents section in Load Details
	//the string of the target file path is passed-in
	public void add_document(String file_path) {
		WebUI.click(findTestObject('Object Repository/Load_Details/Documents/add_document_btn'))
		WebUI.delay(1)
		//upload the file by dragging the file to the iDocs drop box
		WebUI.uploadFileWithDragAndDrop(findTestObject('Object Repository/Load_Details/Documents/drag_and_drop_section'), file_path)
		WebUI.delay(1)
		
		//verify the X close button is present next to the uploaded document
		WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/Documents/document_x_button'), 8)
		
		//submit the file
		WebUI.click(findTestObject('Object Repository/Load_Details/Documents/submit_btn'))

		WebUI.delay(3)
	}

	//verify a file which was added in Nexus appears in the same load details page
	public void verify_doc_present_in_nexus(String file_name) {
		//create a new test object for the original file object
		TestObject to1 = new TestObject("file_name")
		to1.addProperty("text", ConditionType.CONTAINS, file_name)

		WebUI.verifyElementPresent(to1, 5)
	}
	
	//verify a file which was added in iTMS iDocs appears in the same load details page in Nexus
	public void verify_itms_doc_present_in_nexus(String file_name) {
		//create a new test object for the original file object
		TestObject to1 = new TestObject("file_name")
		to1.addProperty("text", ConditionType.CONTAINS, file_name)

		WebUI.verifyElementPresent(to1, 5)
	}
	
	//try to upload an invalid document type
	public void upload_invalid_doc_type(String file_path, String file_name) {
		WebUI.click(findTestObject('Object Repository/Load_Details/Documents/add_document_btn'))
		WebUI.delay(2)
		//upload the file by dragging the file to the iDocs drop box
		WebUI.uploadFileWithDragAndDrop(findTestObject('Object Repository/Load_Details/Documents/drag_and_drop_section'), file_path)
		WebUI.delay(1)
		
		//verify the file not supported message
		WebUI.verifyElementPresent(findTestObject('Object Repository/Load_Details/Documents/invalid_type_text'), 8)
		
		//try to submit the file
		WebUI.click(findTestObject('Object Repository/Load_Details/Documents/submit_btn'))
		//close the window
		WebUI.click(findTestObject('Object Repository/Load_Details/Documents/close_documents_btn'))
		
		//verify the document is not present in load details//
		//create a new test object for the original file object
		TestObject to1 = new TestObject("file_name")
		to1.addProperty("text", ConditionType.CONTAINS, file_name)

		WebUI.verifyElementNotPresent(to1, 6)

		WebUI.delay(3)
	}
}

package load_details

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType

public class Load_Details_Verifications {

	//page elements for the top Details section
	public TestObject details_label = findTestObject('Object Repository/Load_Details/Details_Section/Details_Label')
	public TestObject status_bar = findTestObject('Object Repository/Load_Details/Details_Section/Details_status_bar')
	public TestObject po_order = findTestObject('Object Repository/Load_Details/Details_Section/Details_POOrder')
	public TestObject iel_po = findTestObject('Object Repository/Load_Details/Details_Section/Details_IEL_PO')
	public TestObject weight = findTestObject('Object Repository/Load_Details/Details_Section/Details_Weight')
	public TestObject load_date = findTestObject('Object Repository/Load_Details/Details_Section/Details_Load_Date')
	public TestObject commodity = findTestObject('Object Repository/Load_Details/Details_Section/Details_Commodity')
	public TestObject lane = findTestObject('Object Repository/Load_Details/Details_Section/Details_Lane')
	public TestObject deliver_date = findTestObject('Object Repository/Load_Details/Details_Section/Details_Deliver_Date')
	public TestObject broker = findTestObject('Object Repository/Load_Details/Details_Section/Details_Broker')
	public TestObject office_phone = findTestObject('Object Repository/Load_Details/Details_Section/Details_Office_Phone')
	public TestObject mobile_phone = findTestObject('Object Repository/Load_Details/Details_Section/Details_Mobile_Phone')
	public TestObject email = findTestObject('Object Repository/Load_Details/Details_Section/Details_Email')
	//map element
	public TestObject google_map = findTestObject('Object Repository/Load_Details/google_map')
	//accessorial and billing sections elements
	public TestObject accessorial_label = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Accessorial_Charges_label')
	public TestObject expense = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Expense')
	public TestObject amount = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Amount')
	public TestObject billing_label = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Billing_label')
	public TestObject billed_amnt = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Billed_Amount')
	public TestObject accessorials = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Accessorials')
	public TestObject adjustments = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/adjustments')
	public TestObject total = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Total')
	public TestObject billing_docs = findTestObject('Object Repository/Load_Details/Accessorial_and_Billing/Billing_Documents')
	//check calls elements in the load details page
	public TestObject checkcall_label = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Check_Calls_label')
	public TestObject city = findTestObject('Object Repository/Load_Details/Details_Check_Calls/City')
	public TestObject state = findTestObject('Object Repository/Load_Details/Details_Check_Calls/State')
	public TestObject date = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Date')
	public TestObject temp = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Temp')
	public TestObject time = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Time')
	public TestObject type = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Type')
	public TestObject zip = findTestObject('Object Repository/Load_Details/Details_Check_Calls/Zip')
	//Origin elements
	public TestObject origin_label = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Origin_label')
	public TestObject pick = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Pick')
	public TestObject or_city = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Origin_City')
	public TestObject or_state = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Origin_State')
	public TestObject or_zip = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Origin_Zip')
	public TestObject pick_date = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Pickup_Date')
	public TestObject pick_num = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Pickup_Number')
	public TestObject pick_time = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Pickup_Time')
	public TestObject pick_commodity = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/pick_Commodity')
	public TestObject loaded = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Loaded')
	//Destination elements
	public TestObject destination_label = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Destination_label')
	public TestObject drop = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Drop')
	public TestObject dest_city = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Destination_City')
	public TestObject dest_state = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Destination_State')
	public TestObject dest_zip = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Destination_Zip')
	public TestObject del_date = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Del_Date')
	public TestObject del_po = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Del_PO')
	public TestObject del_time = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Del_Time')
	public TestObject unload_charge = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Unload_Chg')
	public TestObject delivered = findTestObject('Object Repository/Load_Details/Details_Origin_and_Destination/Delivered')
	public TestObject delivered_text_details = findTestObject('Object Repository/Load_Details/Details_Section/Details_Delivered_text')
	public TestObject load_delivered_text = findTestObject('Object Repository/Load_Details/Details_Section/Details_load_delivered_text')
	public TestObject estimated_miles_remaining = findTestObject('Object Repository/Load_Details/Details_Section/Details_estimated_miles_rem')
	public TestObject value_remaining_miles = findTestObject('Object Repository/Load_Details/Details_Section/Details_value_miles_remaining')

	public void details_section_verify(){
		WebUI.verifyElementPresent(details_label, 10)
		WebUI.verifyElementPresent(status_bar, 10)
		WebUI.verifyElementPresent(po_order, 10)
		WebUI.verifyElementPresent(iel_po, 10)
		WebUI.verifyElementPresent(weight, 10)
		WebUI.verifyElementPresent(load_date, 10)
		WebUI.verifyElementPresent(commodity, 10)
		WebUI.verifyElementPresent(lane, 10)
		WebUI.verifyElementPresent(deliver_date, 10)
		WebUI.verifyElementPresent(broker, 10)
		WebUI.verifyElementPresent(office_phone, 10)
		WebUI.verifyElementPresent(mobile_phone, 10)
		WebUI.verifyElementPresent(email, 10)
	}

	//verify the presence a Google maps page element
	public void google_map_verify(){
		WebUI.verifyElementPresent(google_map, 10)
	}

	public void accessorial_and_billing_verify(){
		WebUI.verifyElementPresent(accessorial_label, 10)
		WebUI.verifyElementPresent(expense, 10)
		WebUI.verifyElementPresent(amount, 10)
		WebUI.verifyElementPresent(billing_label, 10)
		WebUI.verifyElementPresent(billed_amnt, 10)
		WebUI.verifyElementPresent(accessorials, 10)
		WebUI.verifyElementPresent(adjustments, 10)
		WebUI.verifyElementPresent(total, 10)
		WebUI.verifyElementPresent(billing_docs, 10)
	}

	public void check_calls_details_verify(){
		WebUI.verifyElementPresent(checkcall_label, 10)
		WebUI.verifyElementPresent(city, 10)
		WebUI.verifyElementPresent(state, 10)
		WebUI.verifyElementPresent(date, 10)
		WebUI.verifyElementPresent(temp, 10)
		WebUI.verifyElementPresent(time, 10)
		WebUI.verifyElementPresent(type, 10)
		WebUI.verifyElementPresent(zip, 10)
	}

	public void origin_and_destination_verify(){
		WebUI.verifyElementPresent(origin_label, 10)
		WebUI.verifyElementPresent(pick, 10)
		WebUI.verifyElementPresent(or_city, 10)
		WebUI.verifyElementPresent(or_state, 10)
		WebUI.verifyElementPresent(or_zip, 10)
		WebUI.verifyElementPresent(pick_date, 10)
		WebUI.verifyElementPresent(pick_num, 10)
		WebUI.verifyElementPresent(pick_time, 10)
		WebUI.verifyElementPresent(pick_commodity, 10)
		WebUI.verifyElementPresent(loaded, 10)
		WebUI.verifyElementPresent(destination_label, 10)
		WebUI.verifyElementPresent(drop, 10)
		WebUI.verifyElementPresent(dest_city, 10)
		WebUI.verifyElementPresent(dest_state, 10)
		WebUI.verifyElementPresent(dest_zip, 10)
		WebUI.verifyElementPresent(del_date, 10)
		WebUI.verifyElementPresent(del_po, 10)
		WebUI.verifyElementPresent(del_time, 10)
		WebUI.verifyElementPresent(unload_charge, 10)
		WebUI.verifyElementPresent(delivered, 10)
	}

	//in the Details section at the top of the Load Details page, verify that after a load is delivered,
	//the text shows as "Delivered" and "load delivered"
	//verify the remaining miles text is NOT present
	public void verify_details_delivered_text(){
		WebUI.verifyElementPresent(delivered_text_details, 5)
		WebUI.verifyElementPresent(load_delivered_text, 5)
		WebUI.verifyElementNotPresent(estimated_miles_remaining, 5)
	}

	//verify the "estimated miles remaining" is present in the Details section
	public void verify_estimated_remaining_miles_text(){
		WebUI.verifyElementPresent(estimated_miles_remaining, 5)
	}

	//take the string value of the remaining miles, retrieved from iTMS, and verify that it appears
	//in the Details section
	public void verify_remaining_miles_value(String miles){
		WebUI.delay(1)
		//find the element on the page which matches the passed-in miles value
		TestObject to = new TestObject("remaining_miles")
		to.addProperty("text", ConditionType.EQUALS, miles)
		//WebUI.click(to)
		String actual_miles = WebUI.getText(to)
		WebUI.verifyMatch(actual_miles, miles, false)
	}
}

package un_billed_AR

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType

public class Un_Billed_AR_Behaviors {

	//page element objects
	public TestObject nexus_logo = findTestObject('Object Repository/Load_Summary/nexus_image')
	public TestObject unbilled_label = findTestObject('Object Repository/Un_Billed_AR/unbilled_loads_label')
	public TestObject cust_dropdown = findTestObject('Object Repository/Load_Summary/customer_dropdown_load_summary')
	public TestObject make_a_payment = findTestObject('Object Repository/Load_Summary/Make_a_payment_button')
	public TestObject billing_tools = findTestObject('Object Repository/Header/Billing_Tools_button')
	public TestObject unbilled_btn = findTestObject('Object Repository/Un_Billed_AR/unbilled_loads_link')
	public TestObject customer_label = findTestObject('Object Repository/Un_Billed_AR/customer_label')
	public TestObject pay_terms = findTestObject('Object Repository/Un_Billed_AR/pay_terms')
	public TestObject zero_30 = findTestObject('Object Repository/Un_Billed_AR/0_to_30_days')
	public TestObject thirtyone_60 = findTestObject('Object Repository/Un_Billed_AR/31_to_60_days')
	public TestObject sixtyone_90 = findTestObject('Object Repository/Un_Billed_AR/61_to_90_days')
	public TestObject ninetyone_over = findTestObject('Object Repository/Un_Billed_AR/91_days_and_over')
	public TestObject billed_ar = findTestObject('Object Repository/Un_Billed_AR/billed_ar')
	public TestObject unbilled_loads = findTestObject('Object Repository/Un_Billed_AR/unbilled_loads')
	public TestObject total = findTestObject('Object Repository/Un_Billed_AR/total')
	public TestObject apply_button = findTestObject("Object Repository/Load_Search/apply_button")
	public TestObject clear_all = findTestObject("Object Repository/Load_Search/clear_all_button")
	public TestObject po_order = findTestObject("Object Repository/Load_Search/poorder_input")
	public TestObject iel_po = findTestObject("Object Repository/Load_Search/iel_po_input")
	public TestObject csv_export_btn = findTestObject("Object Repository/Billed_AR/Export_to_CSV_button")
	TestObject apply_btn = findTestObject('Object Repository/Load_Search/apply_button')
	//Loads Table elements
	public TestObject table_poorder = findTestObject('Object Repository/Un_Billed_AR/table_POOrder')
	public TestObject table_ielpo = findTestObject('Object Repository/Un_Billed_AR/table_IELPO')
	public TestObject table_amount = findTestObject('Object Repository/Un_Billed_AR/table_Amount')
	public TestObject table_deliver_date = findTestObject('Object Repository/Un_Billed_AR/table_Delivery_Date')
	public TestObject table_contact = findTestObject('Object Repository/Un_Billed_AR/table_Contact')
	public TestObject table_view_details_text = findTestObject('Object Repository/Un_Billed_AR/table_View_Details')
	public TestObject table_view_details_button = findTestObject('Object Repository/Load_Summary/View_Details_button')
	

	public void click_billing_tools(){
		WebUI.click(billing_tools)
		WebUI.delay(2)
	}

	public void goto_un_billed_loads(){
		WebUI.click(unbilled_btn)
		WebUI.delay(3)
		//WebUI.verifyElementVisible(unbilled_btn)
	}

	public void unbilled_header_elements_verify(){
		//Verify Un billed Loads elements are present

		//verify Nexus logo element on top
		WebUI.verifyElementPresent(nexus_logo, 3)

		//verify the Un billed Loads label and text
		WebUI.verifyElementPresent(unbilled_label, 3)

		//verify company drop-down - TODO : if - else for verifying this - may or may not have a drop down
		//WebUI.verifyElementPresent(cust_dropdown, 3)

		//verify Make a Payment button element
		WebUI.verifyElementPresent(make_a_payment, 3)

		//verify the customer name element is present
		WebUI.verifyElementPresent(customer_label, 3)
	}

	public void unbilled_information_elements_verify(){
		//TODO -> add verification of generic elements in the information section
	}

	public void unbilled_financial_summary_elements_verify(){
		//verify the elements in the Financial Summary section are present in the page

		WebUI.verifyElementPresent(pay_terms, 3)
		WebUI.verifyElementPresent(zero_30, 3)
		WebUI.verifyElementPresent(thirtyone_60, 3)
		WebUI.verifyElementPresent(sixtyone_90, 3)
		WebUI.verifyElementPresent(ninetyone_over, 3)
		WebUI.verifyElementPresent(billed_ar, 3)
		WebUI.verifyElementPresent(unbilled_loads, 3)
		WebUI.verifyElementPresent(total, 3)
	}

	public void unbilled_filter_section_elements_verify(){
		//verify the elements in the left search / filter section

		WebUI.verifyElementPresent(apply_button, 3)
		WebUI.verifyElementPresent(clear_all, 3)
		WebUI.verifyElementPresent(po_order, 3)
		WebUI.verifyElementPresent(iel_po, 3)
	}

	public void unbilled_loads_table_elements_verify(){
		//verify elements in the loads table headers AND the export CSV button
		WebUI.verifyElementPresent(csv_export_btn, 3)
		WebUI.verifyElementPresent(table_poorder, 3)
		WebUI.verifyElementPresent(table_ielpo, 3)
		WebUI.verifyElementPresent(table_amount, 3)
		WebUI.verifyElementPresent(table_deliver_date, 3)
		WebUI.verifyElementPresent(table_contact, 3)
		WebUI.verifyElementPresent(table_view_details_text, 3)
		//WebUI.verifyElementPresent(table_view_details_button, 3)
	}
	
	public void search_with_po_order_number(String number) {
		WebUI.setText(findTestObject('Object Repository/Billed_AR/po_order_input'), number)
		WebUI.click(apply_btn)
		WebUI.delay(2)
		
	}
	
	public void verify_po_order_number_not_case_sensitive(String upper_number_result) {
		TestObject to = new TestObject("order_po")
		to.addProperty("text", ConditionType.EQUALS, upper_number_result)
		
		WebUI.verifyElementPresent(to, 10)
	}
}

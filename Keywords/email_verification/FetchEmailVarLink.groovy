package email_verification

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.mail.Folder
import javax.mail.Session
import javax.mail.Store
import javax.mail.internet.MimeMessage
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.TestObject

public class FetchEmailVarLink {

	//Need to import the java mail jar files in the Project Settings under external libraries

	//host type for testing, (i.e. outlook)
	String host = "outlook.office365.com"

	/*This method checks if the reset password link is sent. It passes if the message body contains the specified URL,
	 *it fails if the URL is not found. 
	 *It does not return a string
	 * */
	public void check_email_pass_reset(String user, String password) {

		String subject_line_1 = "Integrity Express Logistics: Portal Password Reset"
		String subject_line_2 = "Portal Password Reset"
		String body_text_1 = "Password Reset"
		String body_text_2 = "You have requested to reset your password for NEXUS."
		String body_text_3 = "Click the button below to create a new password for your account."
		String body_text_4 = "For your security, this link will expire if not used within 24 hours. On future visits, please use this email address to login to the portal here at"
		String body_text_5 = "Marks the Spot"
		String body_text_6 = "For Tracking Every Shipment"
		String expected_link = '<p><a href="https://cupo-qa.web.app/?email=qatesting@intxlog.com&amp;pin'

		//create properties field
		Properties properties = new Properties()

		properties.put("mail.imap.host",host)
		properties.put("mail.imap.port", "993")
		properties.put("mail.imap.starttls.enable", "true")
		properties.setProperty("mail.imap.socketFactory.class","javax.net.ssl.SSLSocketFactory")
		properties.setProperty("mail.imap.socketFactory.fallback", "false")
		properties.setProperty("mail.imap.socketFactory.port",String.valueOf(993))
		//Session emailSession = Session.getDefaultInstance(properties)
		Session emailSession = Session.getInstance(properties)

		//create the store object and connect with the server
		Store store = emailSession.getStore("imap")

		store.connect(host, user, password)

		//create the folder object and open it
		Folder emailFolder = store.getFolder("INBOX")
		emailFolder.open(Folder.READ_ONLY)

		// retrieve the messages from the folder in an array and print it
		MimeMessage[] messages = emailFolder.getMessages()
		int n = messages.length

		//change n- to number of Emails you want to dig through
		for(int i = n-4; i<n; i++) {
			//save the email message in an array
			MimeMessage message = messages[i]
			//check if the email subject contains text relating to the portal password reset
			if(message.getSubject().contains(subject_line_1) || message.getSubject().contains(subject_line_2)){
				String desc = message.getContent().toString()
				//boolean for if the text elements are present in the message body
				boolean text = verify_pass_reset_email_text(desc)

				if(desc.contains(expected_link) && text){
					println "FOUND THE RESET PASSWORD URL IN THE MESSAGE BODY IN EMAIL NUMBER: " + i + " and message text is correct"
				}else{
					assert false : "Did not find the reset password URL in the email message body or message text is incorrect"
				}

			}else{
				println("Email:"+ i + " does not have the wanted subject line")
			}
		}
		//close the store and folder objects
		emailFolder.close(false)
		store.close()
	}


	/*This method checks if the reset password link is sent. 
	 * It then parses the HTML and extracts the URL for the pass reset, 
	 * and returns the URL as a string
	 * */
	//** as of 1/19/2021 the HTML reset link will not open unless directly opened from the email
	//the link will not open if copied from the HTML and entered into the browser. (it just goes to the generic log in page)
	public String get_reset_url_string(String user, String password){

		String subject_line_1 = "Integrity Express Logistics: Portal Password Reset"
		String subject_line_2 = "Portal Password Reset"
		//String expected_link = '<p><a href="https://cupo-qa.web.app/?email=qatesting@intxlog.com&amp;pin'
		List<String> containedUrls = new ArrayList<String>()

		//create properties field
		Properties properties = new Properties()

		properties.put("mail.imap.host",host)
		properties.put("mail.imap.port", "993")
		properties.put("mail.imap.starttls.enable", "true")
		properties.setProperty("mail.imap.socketFactory.class","javax.net.ssl.SSLSocketFactory")
		properties.setProperty("mail.imap.socketFactory.fallback", "false")
		properties.setProperty("mail.imap.socketFactory.port",String.valueOf(993))
		//Session emailSession = Session.getDefaultInstance(properties)
		Session emailSession = Session.getInstance(properties)

		//create the store object and connect with the server
		Store store = emailSession.getStore("imap")

		store.connect(host, user, password)

		//create the folder object and open it
		Folder emailFolder = store.getFolder("INBOX")
		emailFolder.open(Folder.READ_ONLY)

		// retrieve the messages from the folder in an array and print it
		MimeMessage[] messages = emailFolder.getMessages()
		int n = messages.length

		//change n- to number of emails you want to dig through
		for(int i = n-4; i<n; i++) {
			//save the email message in an array
			MimeMessage message = messages[i]
			//check if the email subject contains text relating to the portal password reset
			if(message.getSubject().contains(subject_line_1) || message.getSubject().contains(subject_line_2)){
				String desc = message.getContent().toString()
				println "body = " + desc
				//boolean returned from method checking if the message body are present
				//boolean text = verify_pass_reset_email_text(desc)
				//regular expression to find all the links in the email message
				String urlRegex = '((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)'
				Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE)
				Matcher urlMatcher = pattern.matcher(desc)

				//first check if text elements appear, then find URL patterns in the  message body
				//if(text){
				while (urlMatcher.find()){
					containedUrls.add(desc.substring(urlMatcher.start(0),
							urlMatcher.end(0)))
				}
				//}

			}
		}
		//close the store and folder objects
		emailFolder.close(false)
		store.close()

		//get the 5th array item, which is the pass reset URL
		String target_url = containedUrls[5]

		//TEST to see which array element is the correct URL
		for(int i; i < containedUrls.size(); i++){
			println containedUrls[i]
		}

		//!!! need to strip the amp; characters from the URL string from the message, in order to go to the right page
		target_url = target_url.replaceAll("amp;", "")
		//println "url after stripping = " + target_url
		return target_url
	}

	//method to find matching email message text elements
	public static boolean verify_pass_reset_email_text(String body_text){
		//strings to find in the body of the text
		String body_text_1 = "Password Reset"
		String body_text_2 = "You have requested to reset your password for NEXUS."
		String body_text_3 = "Click the button below to create a new password for your account."
		String body_text_4 = "For your security, this link will expire if not used within 24 hours. On future visits, please use this email address to login to the portal here at"
		String body_text_5 = "Marks the Spot"
		String body_text_6 = "For Tracking Every Shipment"
		//boolean values to simplify the if statement
		boolean a = body_text.contains(body_text_1)
		boolean b = body_text.contains(body_text_2)
		boolean c = body_text.contains(body_text_3)
		boolean d = body_text.contains(body_text_4)
		boolean e = body_text.contains(body_text_5)
		boolean f = body_text.contains(body_text_6)

		//return true if the body text elements are found in the message
		if(a && b && c && d && e && f){
			return true
		}
		else{
			return false
		}
	}

	/*This method checks if an email contains two specified Strings, which is passed as a parameter.
	 * The Subject Line text is also passed as a parameter.
	 * It passes if the message body contains the specified string,
	 *it fails if the string is not found.
	 * */
	public void check_email_for_string(String user, String password, String subject_string, String search_string) {

		//create properties field
		Properties properties = new Properties()

		properties.put("mail.imap.host",host)
		properties.put("mail.imap.port", "993")
		properties.put("mail.imap.starttls.enable", "true")
		properties.setProperty("mail.imap.socketFactory.class","javax.net.ssl.SSLSocketFactory")
		properties.setProperty("mail.imap.socketFactory.fallback", "false")
		properties.setProperty("mail.imap.socketFactory.port",String.valueOf(993))
		Session emailSession = Session.getInstance(properties)

		//create the store object and connect with the server
		Store store = emailSession.getStore("imap")

		store.connect(host, user, password)

		//create the folder object and open it
		Folder emailFolder = store.getFolder("INBOX")
		emailFolder.open(Folder.READ_ONLY)

		// retrieve the messages from the folder in an array and print it
		MimeMessage[] messages = emailFolder.getMessages()
		int n = messages.length

		//change n- to number of Emails you want to dig through
		for(int i = n-2; i<n; i++) {
			//save the email message in an array
			MimeMessage message = messages[i]
			//check if the email subject contains text relating to the portal password reset
			if(message.getSubject().contains(subject_string)){
				String desc = message.getContent().toString()
				//boolean for if the text elements are present in the message body
				//boolean text = verify_pass_reset_email_text(desc)
				println "email body = " + desc
				if(desc.contains(search_string)){
					println "FOUND THE EXPECTED STRING IN THE MESSAGE BODY IN EMAIL NUMBER: " + i + " and message text is correct"
				}else{
					assert false : "Did not find the expected string in the email message body or message text is incorrect"
				}

			}else{
				println("Email:"+ i + " does not have the wanted subject line")
			}
		}
		//close the store and folder objects
		emailFolder.close(false)
		store.close()
	}


	/*This method checks if an email contains a group of strings.
	 * It uses a boolean check, passed in from a helper method which returns a boolean.
	 * The helper method will check the email for a group of strings, and return true if all of them are found.
	 * The Subject Line text is also passed as a parameter.
	 * It passes if the message body contains the specified string,
	 *it fails if the string is not found.
	 * */
	public void check_email_for_status_strings(String user, String password, String subject_string) {

		//create properties field
		Properties properties = new Properties()

		properties.put("mail.imap.host",host)
		properties.put("mail.imap.port", "993")
		properties.put("mail.imap.starttls.enable", "true")
		properties.setProperty("mail.imap.socketFactory.class","javax.net.ssl.SSLSocketFactory")
		properties.setProperty("mail.imap.socketFactory.fallback", "false")
		properties.setProperty("mail.imap.socketFactory.port",String.valueOf(993))
		Session emailSession = Session.getInstance(properties)

		//create the store object and connect with the server
		Store store = emailSession.getStore("imap")

		store.connect(host, user, password)

		//create the folder object and open it
		Folder emailFolder = store.getFolder("INBOX")
		emailFolder.open(Folder.READ_ONLY)

		// retrieve the messages from the folder in an array and print it
		MimeMessage[] messages = emailFolder.getMessages()
		int n = messages.length

		//change n- to number of Emails you want to dig through
		for(int i = n-5; i<n; i++) {
			//save the email message in an array
			MimeMessage message = messages[i]
			//check if the email subject contains text relating to the portal password reset
			if(message.getSubject().contains(subject_string)){
				String desc = message.getContent().toString()
				//boolean for if the text elements are present in the message body
				boolean text = verify_status_text(desc)

				if(text){
					println "FOUND THE RESET EXPECTED STRING IN THE MESSAGE BODY IN EMAIL NUMBER: " + i + " and message text is correct"
				}else{
					assert false : "Did not find the reset expected string in the email message body or message text is incorrect"
				}

			}else{
				println("Email:"+ i + " does not have the wanted subject line")
			}
		}
		//close the store and folder objects
		emailFolder.close(false)
		store.close()
	}

	//method to find matching email message text elements
	//to be used within methods for searching through subjects above
	public static boolean verify_status_text(String body_text){
		//strings to find in the body of the text
		String body_text_1 = "Date"
		String body_text_2 = "Time"
		String body_text_3 = "Status"
		String body_text_4 = "Temp"
		String body_text_5 = "City"
		String body_text_6 = "State"
		String body_text_7 = "Zip"
		String body_text_8 = "Miles Remaining"
		String body_text_9 = "Unsubscribe"

		//boolean values to simplify the if statement
		boolean a = body_text.contains(body_text_1)
		boolean b = body_text.contains(body_text_2)
		boolean c = body_text.contains(body_text_3)
		boolean d = body_text.contains(body_text_4)
		boolean e = body_text.contains(body_text_5)
		boolean f = body_text.contains(body_text_6)
		boolean g = body_text.contains(body_text_7)
		boolean h = body_text.contains(body_text_8)
		boolean i = body_text.contains(body_text_9)

		//return true if the body text elements are found in the message
		if(a && b && c && d && e && f && g && h && i){
			return true
		}
		else{
			return false
		}
	}

}
package load_summary

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.TestObject as TestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.support.ui.Select as Select
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

public class Load_Summary_Sorting {

	public static sort_pickdate_verify(){
		//get a list of po numbers on page by finding elements containing '2020' text (the element classes are almost all the same)
		WebDriver driver = DriverFactory.getWebDriver()
		List<TestObject> date_list = driver.findElements(By.xpath("//*[(contains(text(), '2021') or contains(., '2021')) or contains(., '2020')]"))
		String date_list_string

		for(int i = 0; i < date_list.size(); i++){
			date_list_string = date_list.get(i).getText()
			println date_list_string
		}
	}

	public static sort_po_verify(){
		WebDriver driver = DriverFactory.getWebDriver()
		//find and save the po elements in a list by class
		List<TestObject> po_list = driver.findElements(By.xpath("//*[@class = 'styles_ellipsis__sSQnO']"))

		List<String> actual_list = []
		List<String> sorted_list = []

		//string list for actual po list contents
		String po_list_string
		String po_reverse_list_string

		//loop through and verify that the actual elements are sorted low to high by default
		for(int i = 0; i < po_list.size(); i++){
			po_list_string = po_list.get(i).getText()
			actual_list.add(po_list_string)
			println "actual STRINGS = " + po_list_string
			println "actual LIST = " + actual_list
			sorted_list = actual_list.sort()
			println "sorted LIST = " + sorted_list
			assert actual_list.get(i) == sorted_list.get(i)
		}

		//click the arrow to reverse the order and verify that the order is reversed
		WebUI.click(findTestObject('Object Repository/Load_Summary/PO_OrderDown_arrow'))
		WebUI.delay(1)
		WebUI.click(findTestObject('Object Repository/Load_Summary/PO_OrderDown_arrow - 2'))
		WebUI.delay(1)

		//loop through and verify that the actual elements are reverse sorted high to low after clicking the arrow
		for(int i = 1; i < po_list.size(); i++){
			po_reverse_list_string = po_list.get(i).getText()
			println po_reverse_list_string
			//def reversed_po_list = po_list.reverse(true)
			//assert po_list.get(i) == reversed_po_list.get(i)
		}
	}
}

package load_summary

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.ConditionType

public class Load_Summary_Behaviors {

	//page objects
	static TestObject in_transit = findTestObject('Object Repository/Load_Summary/in_transit_button')
	static TestObject load_po = findTestObject('Object Repository/In_Transit_Loads/in_transit_po')
	static TestObject today = findTestObject('Object Repository/Load_Summary/todays_loads_button')
	static TestObject delivered = findTestObject('Object Repository/Load_Summary/delivered_button')
	static TestObject future = findTestObject('Object Repository/Load_Summary/future_loads_button')
	static TestObject delivered_amnt = findTestObject('Object Repository/Load_Summary/delivered_loads_amount')
	//labels
	static TestObject in_transit_label = findTestObject('Object Repository/In_Transit_Loads/label_In Transit')
	static TestObject today_label = findTestObject('Object Repository/Todays_Loads/Today_label')
	static TestObject delivered_label = findTestObject('Object Repository/Delivered_Loads/DeliveredLast_72 Hours_label')
	static TestObject future_label = findTestObject('Object Repository/Future_Loads/Future_label')
	static TestObject subscribe_btn = findTestObject('Object Repository/Load_Search/Subscribe_button_first')
	static TestObject unsubscribe_btn = findTestObject('Object Repository/Load_Search/Unsubscribe_btn')
	static TestObject yes_btn = findTestObject('Object Repository/Load_Search/Unsubscribe_Yes_confirm_btn')
	static TestObject track_now_btn = findTestObject('Object Repository/Load_Search/Subscribe_Track_Now_button')
	static TestObject subscribe_popup = findTestObject('Object Repository/Other_Common_Elements/subscribe_popup')

	public static void click_in_transit(){
		//click the In Transit section
		WebUI.click(in_transit)
		//verify that the page opens to a list of loads to choose from
		//verify In Transit label appears
		WebUI.verifyElementPresent(in_transit_label, 10)
		WebUI.delay(3)
	}

	public static void click_today_loads(){
		//click the In Transit section
		WebUI.click(today)
		//verify Today label appears
		WebUI.verifyElementPresent(today_label, 10)
		WebUI.delay(3)
	}

	public static void click_delivered_loads(){
		//click the In Transit section
		WebUI.click(delivered)
		//verify Delivered label appears
		WebUI.verifyElementPresent(delivered_label, 10)
		WebUI.delay(3)
	}

	public static void click_future_loads(){
		//click the In Transit section
		WebUI.click(future)
		//verify Future label appears
		WebUI.verifyElementPresent(future_label, 10)
		WebUI.delay(3)
	}

	public static void verify_load_present_in_transit(){
		//verify a PO for a load appears on the In Transit Page
		WebUI.verifyElementPresent(load_po, 6)
	}

	//retrieve the text from the amount of Delivered loads on the upper right corner of the Delivered Loads section
	//returns the string value of the number taken from the elements
	public static String get_delivered_loads_amount(){
		//delay because when the page first loads, the elements shows 0, and the updates
		WebUI.delay(5)
		WebUI.waitForPageLoad(60)
		String del_amnt = WebUI.getText(delivered_amnt)
		return del_amnt
	}

	//select the first subscribe button after selecting the In Transit loads from the load summary page
	//verify that the text of the button is Subscribe
	public static void select_subscribe(){
		String btn_text = WebUI.getText(subscribe_btn)
		WebUI.verifyMatch(btn_text, "Subscribe", false)
		
		WebUI.click(subscribe_btn)
		WebUI.delay(2)
		WebUI.click(track_now_btn)
		WebUI.delay(1)
		//refresh the page
		WebUI.refresh()
	}
	
	//after subscribing, verify that the same button now says Update instead of Subscribe
	public static void verify_update_button() {
		String btn_text = WebUI.getText(subscribe_btn)
		WebUI.verifyMatch(btn_text, "Update", false)
	}
	
	//after Un-subscribing, verify that the same button now says Subscribe instead of Update
	public static void verify_subscribe_button() {
		String btn_text = WebUI.getText(subscribe_btn)
		WebUI.verifyMatch(btn_text, "Subscribe", false)
	}
	
	//click the update button and then click Un-subscribe After the Subscribe behavior
	//this can be used as a tear-down method after subscribing first
	public static void unsubscribe() {
		WebUI.click(subscribe_btn)
		WebUI.delay(2)
		
		WebUI.click(unsubscribe_btn)
		WebUI.delay(2)
		
		//confirm
		WebUI.click(yes_btn)
		WebUI.delay(2)
	}

	public static void keep_subscribe_popup_open(){
		WebUI.click(subscribe_btn)
		WebUI.delay(1)
	}

	public static void verify_subscribe_popup_not_present(){
		WebUI.delay(1)
		WebUI.verifyElementNotPresent(subscribe_popup, 6)
	}

	//get the total number of the Update elements on the page
	//use to verify that the Update count is increased by 1 after Subscribe
	//returns the integer value of the count
	public static Integer get_update_element_count(){
		//delay so the elements are loaded
		WebUI.delay(2)
		//need to use webdriver to get the element count
		WebDriver driver = DriverFactory.getWebDriver()
		//count the elements that have the "Update" text on page
		Integer eleCount = driver.findElements(By.xpath("//*[text()='Update']")).size()
		return eleCount
	}

	public static void verify_status_test_on_todays_loads(String status) {
		TestObject to = new TestObject("status")
		to.addProperty("text", ConditionType.CONTAINS, status)

		WebUI.verifyElementPresent(to, 8)
	}
}

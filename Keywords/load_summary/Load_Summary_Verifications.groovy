package load_summary

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class Load_Summary_Verifications {

	//page elements
	static TestObject cust_dropdown = findTestObject('Object Repository/Load_Summary/customer_dropdown_load_summary')
	static TestObject nexus_logo = findTestObject('Object Repository/Load_Summary/nexus_image')
	static TestObject load_summ_label = findTestObject('Object Repository/Load_Summary/Load_Summary_label')
	static Object delivered_label = findTestObject('Object Repository/Delivered_Loads/DeliveredLast_72 Hours_label')
	static Object label_72hr = findTestObject('Object Repository/Delivered_Loads/Last72Hours_label')
	static Object make_a_payment = findTestObject('Object Repository/Load_Summary/Make_a_payment_button')
	//static Object transit_label = findTestObject('Object Repository/In_Transit_Loads/label_In Transit')
	//common headers
	static Object po = findTestObject('Object Repository/Header_Sections/POOrder')
	static Object pick_date = findTestObject('Object Repository/Header_Sections/Pickup_Date')
	static Object pick_city = findTestObject('Object Repository/Header_Sections/Pickup_City')
	static Object pick_state = findTestObject('Object Repository/Header_Sections/Pickup_State')
	static Object delivery_date = findTestObject('Object Repository/Header_Sections/Delivery_Date')
	static Object delivery_city = findTestObject('Object Repository/Header_Sections/Delivery_City')
	static Object delivery_state = findTestObject('Object Repository/Header_Sections/Delivery_State')
	static Object status = findTestObject('Object Repository/Header_Sections/Status')
	static Object note = findTestObject('Object Repository/Header_Sections/Note')
	static Object view_det_label = findTestObject('Object Repository/Header_Sections/View_Details')
	static Object view_det_btn = findTestObject('Object Repository/Header_Sections/View_Details_Link')
	static Object rec_updates_label = findTestObject('Object Repository/Header_Sections/Receive_Updates')
	static Object rec_updates_btn = findTestObject('Object Repository/Header_Sections/Subscribe_button')
	//footer elements
	static Object nexus_logo_footer = findTestObject('Object Repository/Footer/nexus_logo_footer')
	static Object x_marks_spot = findTestObject('Object Repository/Footer/X_Marks_The_Spot_footer')
	static Object contact = findTestObject('Object Repository/Footer/Contact_label_footer')
	static Object nexus_support = findTestObject('Object Repository/Footer/nexus_support_label_footer')
	static Object facebook = findTestObject('Object Repository/Footer/Feedback_footer')
	static Object twitter = findTestObject('Object Repository/Footer/twitter_link_footer')
	static Object iel_logo_footer = findTestObject('Object Repository/Footer/iel_logo_footer')
	static Object copyright = findTestObject('Object Repository/Footer/copyright_footer')
	static Object privacy = findTestObject('Object Repository/Footer/Privacy_Policy_footer')
	static Object terms_of_use = findTestObject('Object Repository/Footer/Terms_of_Use_footer')
	static Object feedback = findTestObject('Object Repository/Footer/Feedback_footer')
	static Object version = findTestObject('Object Repository/Footer/Version_footer')


	//verify that the user is on the Load Summary page
	public void verify_on_load_summary_page(){
		WebUI.verifyElementPresent(load_summ_label, 10)
	}

	public static void load_summary_elements_verify(){
		//Verify Load Summary elements are present
		//verify Nexus logo element on top
		WebUI.verifyElementPresent(nexus_logo, 3)
		//verify the Load Summary label and text
		WebUI.verifyElementPresent(load_summ_label, 3)
		String text = WebUI.getText(load_summ_label)
		WebUI.verifyMatch(text, 'Load Summary', false)
		//verify company drop-down - TODO : if - else for this
		//WebUI.verifyElementPresent(cust_dropdown, 3)
		//verify Make a Payment button element
		WebUI.verifyElementPresent(make_a_payment, 3)
		//verify the load count icons are present and that there are 4 on the page, one for each section
		//get WebDriver instance
		WebDriver driver = DriverFactory.getWebDriver()
		//verify the 4 main load type elements
		//count the number of elements with the "styles_background__2jEIR" class and make sure there are 4
		int count = driver.findElements(By.className("styles_background__2jEIR")).size()
		println count
		assert count == 4
	}

	public static void delivered_font_verification(){
		//verify CSS font weights for bold or not bold text
		//"Delivered" is in bold ---> CSS font weight = 600
		//"Last 72 hours" is NOT bold --> CSS font weight = 200
		String delivered_weight = WebUI.getCSSValue(delivered_label, 'font-weight')
		String hr72_weight = WebUI.getCSSValue(label_72hr, 'font-weight')
		WebUI.verifyMatch(delivered_weight, '600', false)
		WebUI.verifyMatch(hr72_weight, '200', false)
	}

	//put into common behaviors?
	//verify load section headers for each Load Summary section that opens (PO/Order, Pickup Date, etc.)
	public static void verify_load_section_headers(){
		WebUI.verifyElementPresent(po, 4)
		WebUI.verifyElementPresent(pick_date, 4)
		WebUI.verifyElementPresent(pick_city, 4)
		WebUI.verifyElementPresent(pick_state, 4)
		WebUI.verifyElementPresent(delivery_date, 4)
		WebUI.verifyElementPresent(delivery_city, 4)
		WebUI.verifyElementPresent(delivery_state, 4)
		WebUI.verifyElementPresent(status, 4)
		WebUI.verifyElementPresent(view_det_label, 4)
		WebUI.verifyElementPresent(rec_updates_label, 4)
		//WebUI.verifyElementPresent(rec_updates_btn, 4)
		//WebUI.verifyElementPresent(view_det_btn, 4)
	}

	//also in common_behaviors package
	public static void footer_verifications(){
		//verify footer elements are present
		WebUI.verifyElementPresent(nexus_logo_footer, 4)
		WebUI.verifyElementPresent(x_marks_spot, 4)
		WebUI.verifyElementPresent(contact, 4)
		WebUI.verifyElementPresent(nexus_support, 4)
		WebUI.verifyElementPresent(facebook, 4)
		WebUI.verifyElementPresent(twitter, 4)
		WebUI.verifyElementPresent(iel_logo_footer, 4)
		WebUI.verifyElementPresent(copyright, 4)
		WebUI.verifyElementPresent(privacy, 4)
		WebUI.verifyElementPresent(terms_of_use, 4)
		WebUI.verifyElementPresent(feedback, 4)
		WebUI.verifyElementPresent(version, 4)
	}
}

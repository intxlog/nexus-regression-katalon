package login_page

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import org.openqa.selenium.Keys as Keys

public class Login_Behaviors {

	TestObject sign_in_btn = findTestObject("Object Repository/LogIn_IEL_Nexus/sign_in_button")
	TestObject email_input = findTestObject("Object Repository/LogIn_IEL_Nexus/email_address_input")
	TestObject invalid_format = findTestObject("Object Repository/LogIn_IEL_Nexus/Invalid_format_message")
	TestObject welcome_label = findTestObject("Object Repository/LogIn_IEL_Nexus/Above_Login_Box/Welcome_label")
	TestObject user_not_found = findTestObject("Object Repository/LogIn_IEL_Nexus/valid_user_not_found_message")
	TestObject invalid_user_or_pass = findTestObject("Object Repository/LogIn_IEL_Nexus/invalid_email_or_pass_msg")
	TestObject pass_input = findTestObject("Object Repository/LogIn_IEL_Nexus/password_input")
	TestObject locked_out_msg = findTestObject("Object Repository/LogIn_IEL_Nexus/locked_out_msg")

	//variables
	String wrong_password = "llsspjcn234"

	public void invalid_email_format(){
		//enter invalid email format
		String random_text = "snndi238uois^^mwd"
		WebUI.setText(email_input, random_text)
		//tab out of email input
		WebUI.sendKeys(email_input, Keys.chord(Keys.TAB))
		//verify the invalid format message appears
		WebUI.verifyElementPresent(invalid_format, 4)
	}

	public void random_email(){
		//enter a random email in the proper format
		String random_email = "test11@test7.com"
		WebUI.setText(email_input, random_email)
		//tab out of email input
		//WebUI.sendKeys(email_input, Keys.chord(Keys.TAB))
		//enter a random password
		WebUI.setText(pass_input, wrong_password)
		//call the unsuccessful sign in method from the same class
		//verify the user is still on the log in page
		sign_in_unsuccessful_verify()
		//verify the user not found message appears
		//WebUI.verifyElementPresent(user_not_found, 4)
	}

	//enter the valid testing email with a wrong password one time
	public void valid_email_wrong_pass_once(String email){
		WebUI.setText(email_input, email)
		WebUI.setText(pass_input, wrong_password)
		//call the unsuccessful sign in method
		sign_in_unsuccessful_wrong_email_or_pass_verify()
	}

	//enter the valid testing email with a wrong password ELEVEN times, to lock the user out of the application and verify the error message
	public void valid_email_wrong_pass_multiple_times(String email){
		WebUI.setText(email_input, email)
		WebUI.setText(pass_input, wrong_password)
		//for loop clicking Sign In multiple times
		for(int i = 0; i < 11; i++){
			WebUI.click(sign_in_btn)
			WebUI.delay(1)
		}
		//verify the lock out error message after the loop
		WebUI.verifyElementPresent(locked_out_msg, 5)

		//* need to wait over a minute to wait for the login to be reset *//
	}

	//try to submit and verify the user is still on the login page, with the WELCOME! text
	public void sign_in_unsuccessful_verify(){
		WebUI.click(sign_in_btn)
		WebUI.delay(1)
		WebUI.verifyElementPresent(welcome_label, 4)
	}

	//try to sign in and verify the "Invalid Email or Password" error text
	public void sign_in_unsuccessful_wrong_email_or_pass_verify(){
		WebUI.click(sign_in_btn)
		WebUI.delay(1)
		WebUI.verifyElementPresent(invalid_user_or_pass, 4)
		//verify the welcome text on the login page
		WebUI.verifyElementPresent(welcome_label, 4)
	}
}

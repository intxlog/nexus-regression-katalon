package login_page

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

public class Password_Reset {

	//declare page objects
	static TestObject forgot_pass = findTestObject('Object Repository/LogIn_IEL_Nexus/Forgot_Password_button')
	static TestObject reset_pass_label = findTestObject('Object Repository/LogIn_IEL_Nexus/Reset_Password_label')
	static TestObject email_input = findTestObject("Object Repository/LogIn_IEL_Nexus/email_address_input")
	static TestObject reset_pass_button = findTestObject("Object Repository/LogIn_IEL_Nexus/Reset_Password_button")
	static TestObject email_sent_label = findTestObject('Object Repository/LogIn_IEL_Nexus/Reset_password_sent_label')
	static TestObject pass_input = findTestObject("Object Repository/LogIn_IEL_Nexus/password_input")
	static TestObject pass_confirm_input = findTestObject("LogIn_IEL_Nexus/password_input_confirm")
	static TestObject pass_reset_text = findTestObject('Object Repository/LogIn_IEL_Nexus/pass_reset_text')
	static TestObject request_pass_reset = findTestObject("Object Repository/LogIn_IEL_Nexus/Request_Reset_Password_button")
	static TestObject reset_password_button = findTestObject("Object Repository/LogIn_IEL_Nexus/Reset_Password_button")
	static TestObject invalid_format = findTestObject("Object Repository/LogIn_IEL_Nexus/Invalid_format_message")
	static TestObject user_not_found = findTestObject("Object Repository/LogIn_IEL_Nexus/valid_user_not_found_message")
	static TestObject pass_val_1 = findTestObject("Object Repository/LogIn_IEL_Nexus/password_validation_1")
	static TestObject pass_val_2 = findTestObject("Object Repository/LogIn_IEL_Nexus/password_validation_2")
	static TestObject pass_expired = findTestObject("Object Repository/LogIn_IEL_Nexus/password_expired_message")

	public static void click_forgot_pass(){
		WebUI.click(forgot_pass)
		WebUI.delay(1)
		WebUI.verifyElementPresent(reset_pass_label, 4)
	}

	public static void enter_email_and_reset(String email){
		WebUI.setText(email_input, email)
		WebUI.click(request_pass_reset)
		WebUI.delay(2)
	}

	//verify that the user lands on the page that confirms that the reset email was sent
	public static void verify_email_sent_page(){
		WebUI.verifyElementPresent(email_sent_label, 4)
	}

	public static void verify_pass_reset_elements(){
		WebUI.delay(2)
		WebUI.verifyElementPresent(reset_pass_label, 4)
		WebUI.verifyElementPresent(pass_input, 4)
		WebUI.verifyElementPresent(pass_confirm_input, 4)
		WebUI.verifyElementPresent(reset_password_button, 4)
		//finding text element by the actual text
		WebUI.verifyElementPresent(pass_reset_text, 4)
	}

	public static void invalid_email_format(){
		//enter invalid email format
		String random_text = "sqqdi238u@oismwd@.."
		WebUI.setText(email_input, random_text)
		WebUI.click(reset_pass_button)
		WebUI.delay(1)
		//verify the invalid format message appears
		WebUI.verifyElementPresent(invalid_format, 4)
	}

	public static void random_email(){
		//enter a random email in the proper format
		String random_email = "test11@test7.com"
		WebUI.setText(email_input, random_email)
		WebUI.click(reset_pass_button)
		WebUI.delay(1)
		//verify the user not found message appears
		WebUI.verifyElementPresent(user_not_found, 4)
	}

	public static void inactive_email(String inactive){
		//enter an email for an inactive but existing account
		WebUI.setText(email_input, inactive)
		WebUI.click(reset_pass_button)
		WebUI.delay(1)
		//verify the user not found message appears
		WebUI.verifyElementPresent(user_not_found, 4)
	}

	//method to try various incorrect password character combinations, and verify that the validation errors appear
	public static void password_tries(){
		//strings for password tries
		//less than 8 characters
		String pass_less_8_chars = "mso12"
		//pass try with only two requirements fulfilled
		String pass_2_req = "qwertyui!"
		//pass try with 3 requirements, but less than 8 characters
		String pass_3_req = "Qwrt!22"
		//a non-matching pass for the two inputs
		String pass_unique = "passkl!23AB"
		//valid pass string, validations should go away
		String pass_valid = "Pass987!d_"

		//enter invalid pass and verify the correct validation appears
		WebUI.setText(pass_input, pass_less_8_chars)
		WebUI.setText(pass_confirm_input, pass_less_8_chars)
		WebUI.verifyElementPresent(pass_val_1, 3)

		WebUI.setText(pass_input, pass_2_req)
		WebUI.setText(pass_confirm_input, pass_2_req)
		WebUI.verifyElementPresent(pass_val_1, 3)

		WebUI.setText(pass_input, pass_3_req)
		WebUI.setText(pass_confirm_input, pass_3_req)
		WebUI.verifyElementPresent(pass_val_1, 3)

		//set two non matching passes on the inputs
		WebUI.refresh()
		WebUI.setText(pass_input, pass_valid)
		WebUI.setText(pass_confirm_input, pass_unique)
		//have to try to click the reset button
		WebUI.click(reset_password_button)
		WebUI.verifyElementPresent(pass_val_2, 3)

		//valid pass, validations gone
		//need to refresh the page again for the reset button to be enabled
		WebUI.refresh()
		WebUI.setText(pass_input, pass_valid)
		WebUI.setText(pass_confirm_input, pass_valid)
		WebUI.verifyElementNotPresent(pass_val_2, 3)
		WebUI.verifyElementNotPresent(pass_val_1, 3)
		WebUI.refresh()
	}

	public static void enter_valid_password_reset(String pass){
		//get cupo / nexus testing user name from the db_data file
		def data = findTestData("Data Files/db_data")
		pass = data.getValue(2, 4)
		WebUI.setEncryptedText(pass_input, pass)
		WebUI.setEncryptedText(pass_confirm_input, pass)
		WebUI.click(reset_pass_button)
	}

	public static void verify_password_link_expired(){
		//Verify the message for expired password link : Verification code has expired.
		WebUI.verifyElementPresent(pass_expired, 4)
	}
}

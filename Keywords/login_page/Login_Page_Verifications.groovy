package login_page

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class Login_Page_Verifications {

	//page elements above the log in box
	public TestObject logo = findTestObject('Object Repository/LogIn_IEL_Nexus/Above_Login_Box/login_page_nexus_logo')
	public TestObject welcome = findTestObject('Object Repository/LogIn_IEL_Nexus/Above_Login_Box/Welcome_label')
	public TestObject app_description = findTestObject('Object Repository/LogIn_IEL_Nexus/Above_Login_Box/description_label')
	//page elements inside the login box
	public TestObject login_label = findTestObject('Object Repository/LogIn_IEL_Nexus/Inside_Login_Box/Login_box_label')
	public TestObject acct_request = findTestObject('Object Repository/LogIn_IEL_Nexus/Inside_Login_Box/account_request_label')
	public TestObject login_iel_logo = findTestObject('Object Repository/LogIn_IEL_Nexus/Inside_Login_Box/login_box_logo')
	public TestObject email_input = findTestObject('Object Repository/LogIn_IEL_Nexus/email_address_input')
	public TestObject pass_input = findTestObject('Object Repository/LogIn_IEL_Nexus/password_input')
	public TestObject forgot_pass = findTestObject('Object Repository/LogIn_IEL_Nexus/Forgot_Password_button')
	public TestObject sign_in_btn = findTestObject('Object Repository/LogIn_IEL_Nexus/sign_in_button')
	//footer elements
	public TestObject x_marks_the_spot = findTestObject('Object Repository/Footer/X_Marks_the_spot_short')
	public TestObject logo_footer = findTestObject('Object Repository/Footer/iel_logo_footer')
	public TestObject copyright = findTestObject('Object Repository/Footer/copyright_footer')
	public TestObject privacy_policy = findTestObject('Object Repository/Footer/Privacy_Policy_footer')
	public TestObject terms_use = findTestObject('Object Repository/Footer/Terms_of_Use_footer')
	public TestObject feedback = findTestObject('Object Repository/Footer/Feedback_footer')

	//verify the presence of the elements above the log in box on the log in page
	public void verify_above_login_box_elements(){
		WebUI.verifyElementPresent(logo, 5)
		WebUI.verifyElementPresent(welcome, 5)
		WebUI.verifyElementPresent(app_description, 5)
	}

	//verify the presence of the elements Inside the log in box on the log in page
	public void verify_inside_login_box_elements(){
		WebUI.verifyElementPresent(login_label, 5)
		WebUI.verifyElementPresent(acct_request, 5)
		WebUI.verifyElementPresent(login_iel_logo, 5)
		WebUI.verifyElementPresent(email_input, 5)
		WebUI.verifyElementPresent(pass_input, 5)
		WebUI.verifyElementPresent(forgot_pass, 5)
		WebUI.verifyElementPresent(sign_in_btn, 5)
	}

	//verify the footer elements
	public void verify_login_footer_elements(){
		WebUI.verifyElementPresent(x_marks_the_spot, 5)
		//TODO : text element below the x marks the spot line
		WebUI.verifyElementPresent(logo_footer, 5)
		WebUI.verifyElementPresent(copyright, 5)
		WebUI.verifyElementPresent(privacy_policy, 5)
		WebUI.verifyElementPresent(terms_use, 5)
		WebUI.verifyElementPresent(feedback, 5)
	}
}

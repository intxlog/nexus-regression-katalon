package load_request

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import org.openqa.selenium.Keys as Keys
import helper_methods.*
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import com.kms.katalon.core.testobject.ConditionType

public class Load_Request {

	//page element objects
	public TestObject load_req_link = findTestObject('Object Repository/Header/Load_Request_btn')
	public TestObject continue_btn = findTestObject('Object Repository/Load_Request/Continue_Load_Request')
	public TestObject load_req_label = findTestObject('Object Repository/Load_Request/Load_Request_label')
	public TestObject submit = findTestObject('Object Repository/Other_Common_Elements/Submit_btn')
	public TestObject pick_date = findTestObject('Object Repository/Load_Request/pickup_date_input')
	public TestObject drop_date = findTestObject('Object Repository/Load_Request/drop_date_input')
	public TestObject equipment = findTestObject('Object Repository/Load_Request/equipment_select')
	public TestObject pick_busns_name = findTestObject('Object Repository/Load_Request/pickup_business_name_input')
	public TestObject drop_busns_name = findTestObject('Object Repository/Load_Request/drop_business_name')
	public TestObject pick_addrs_search = findTestObject('Object Repository/Load_Request/pick_address_search_input')
	public TestObject drop_addrs_search = findTestObject('Object Repository/Load_Request/drop_address_search_input')
	public TestObject success_popup = findTestObject('Object Repository/Load_Request/load_request_success_popup_text')
	public TestObject OK_btn = findTestObject('Object Repository/Other_Common_Elements/Ok_btn')
	public TestObject cancel_btn = findTestObject('Object Repository/Load_Request/cancel_btn')
	public TestObject user_dropdown = findTestObject('Object Repository/Header/User_Drop_Down')
	public TestObject req_hist_labl = findTestObject('Object Repository/Load_Request/Request_History/Request_History_label')
	public TestObject request_history = findTestObject('Object Repository/Header/User_Avatar_Dropdown/avatar_request_history')
	public TestObject pick_street = findTestObject('Object Repository/Load_Request/pickup_street_line1')
	public TestObject drop_street = findTestObject('Object Repository/Load_Request/drop_street_line1')

	//Red Required Fields
	public TestObject req_equipment = findTestObject('Object Repository/Load_Request/req_equipment_select')
	public TestObject req_pick_addrss = findTestObject('Object Repository/Load_Request/req_pick_addrss')
	public TestObject req_pick_busins_name = findTestObject('Object Repository/Load_Request/req_pick_business_name')
	public TestObject req_city  = findTestObject('Object Repository/Load_Request/req_pick_city')
	public TestObject req_state = findTestObject('Object Repository/Load_Request/req_pick_state')
	public TestObject req_zip = findTestObject('Object Repository/Load_Request/req_pick_zip')
	public TestObject req_pick_date = findTestObject('Object Repository/Load_Request/req_pickup_date')
	public TestObject req_drop_addrss = findTestObject('Object Repository/Load_Request/req_drop_addrss')
	public TestObject req_drop_busins_name = findTestObject('Object Repository/Load_Request/req_drop_business_name')
	public TestObject req_drop_city = findTestObject('Object Repository/Load_Request/req_drop_city')
	public TestObject req_drop_state = findTestObject('Object Repository/Load_Request/req_drop_state')
	public TestObject req_drop_zip = findTestObject('Object Repository/Load_Request/req_drop_zip')
	public TestObject req_drop_date = findTestObject('Object Repository/Load_Request/req_drop_date')

	//Helper Methods object
	Helper_Methods help = new Helper_Methods()
	//Strings
	public String equip_string = "Van"
	String business_name = "business name"
	//search inputs (auto-filled after)
	String pick_search = "42"
	String drop_search = "46"
	String invalid_address = "6878443114"
	//expected string value of address fields
	String expected_pick_addrs = "4260 Glendale Milford Road"
	String expected_drop_addrs = "4650 Lake Forest Drive"

	//click the Load Request button to go to the Disclaimer page
	public void goto_load_request_disclaimer(){
		WebUI.click(load_req_link)
		WebUI.delay(1)
		//click the Submit Request option from the drop-down
		WebUI.click(findTestObject('Object Repository/Load_Request/submit_request_button'))
		WebUI.delay(1)
	}

	//click the Cancel button on the Disclaimer page
	public void cancel_load_request(){
		WebUI.click(cancel_btn)
	}

	//select Continue to go to the Load Request page
	public void continue_to_load_request(){
		WebUI.click(continue_btn)
		WebUI.delay(1)
	}

	//verify the Load Request label text on the Load Request page
	public void verify_load_request_page(){
		WebUI.verifyElementPresent(load_req_label, 10)
		//WebUI.verifyElementVisible(load_req_label)
		WebUI.verifyElementClickable(load_req_label)
	}

	//click the Submit button for the load request
	public void submit_load_request_no_input_values(){
		WebUI.click(submit)
		WebUI.delay(2)
	}
	//click the Submit button for the load request
	public void submit_load_request(){
		WebUI.click(submit)
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Load_Request/Ok_btn'))
	}

	//verify the presence of the red "Required" validation error text
	public void verify_load_request_required_validations(){
		WebUI.verifyElementPresent(req_equipment, 10)
		WebUI.verifyElementPresent(req_pick_addrss, 10)
		WebUI.verifyElementPresent(req_pick_busins_name, 10)
		WebUI.verifyElementPresent(req_city, 10)
		WebUI.verifyElementPresent(req_state, 10)
		WebUI.verifyElementPresent(req_zip, 10)
		WebUI.verifyElementPresent(req_pick_date, 10)
		WebUI.verifyElementPresent(req_drop_addrss, 10)
		WebUI.verifyElementPresent(req_drop_busins_name, 10)
		WebUI.verifyElementPresent(req_drop_city, 10)
		WebUI.verifyElementPresent(req_drop_state, 10)
		WebUI.verifyElementPresent(req_drop_zip, 10)
		WebUI.verifyElementPresent(req_drop_date, 10)
	}

	//fill in the required fields
	//return the random PO number
	public String fill_in_load_request_data(){
		//use helper methods to get today's date
		String date = help.today_date()
		//Enter today's date into the Pickup Date field
		WebUI.setText(pick_date, date)
		//key in Enter after date is entered
		WebUI.sendKeys(pick_date, Keys.chord(Keys.ENTER))

		//use helper method to get future date
		String drop = help.future_date()
		//Enter future date into the Drop Date field
		WebUI.setText(drop_date, date)
		//key in Enter after date is entered
		WebUI.sendKeys(drop_date, Keys.chord(Keys.ENTER))
		
		//enter a Random PO number
		Integer rand_int = Math.abs(new Random().nextInt() % 1000000) + 1000
		String rand_po = rand_int.toString()
		WebUI.setText(findTestObject('Object Repository/Load_Request/po_num'), rand_po)
		

		//Select Equipment from drop-down
		WebUI.selectOptionByValue(equipment, equip_string, false)
		
		//Enter Pick and Drop Business Names
		WebUI.setText(pick_busns_name, business_name)
		WebUI.setText(drop_busns_name, business_name)
		
		//Search for an address for the Pick and Drop
		//send the keys for 'Arrow Down', and 'Enter' to enter the auto-filled data
		WebUI.setText(pick_addrs_search, pick_search)
		WebUI.delay(1)
		WebUI.sendKeys(pick_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		WebUI.setText(drop_addrs_search, drop_search)
		WebUI.delay(1)
		WebUI.sendKeys(drop_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)

		return rand_po
	}
	
	//fill in the required fields
	//return a List of random values that are entered in the load request inputs
	public List fill_in_load_request_data_random_values(){
		//return list of values
		List values = []
		
		//use helper methods to get today's date
		String date = help.today_date()
		//Enter today's date into the Pickup Date field
		WebUI.setText(pick_date, date)
		//key in Enter after date is entered
		WebUI.sendKeys(pick_date, Keys.chord(Keys.ENTER))

		//use helper method to get future date
		String drop = help.future_date()
		//Enter future date into the Drop Date field
		WebUI.setText(drop_date, date)
		//key in Enter after date is entered
		WebUI.sendKeys(drop_date, Keys.chord(Keys.ENTER))

		//Select Equipment from drop-down
		WebUI.selectOptionByValue(equipment, equip_string, false)

		//enter a Random PO number
		Integer rand_int = Math.abs(new Random().nextInt() % 1000000) + 1000
		String rand_po = rand_int.toString()
		WebUI.setText(findTestObject('Object Repository/Load_Request/po_num'), rand_po)
		//add random PO number to the return list
		values << rand_po

		//random pick business name
		Integer rand_int_2 = Math.abs(new Random().nextInt() % 1000000) + 1000
		String rr1 = rand_int_2.toString()
		String rand_b_name_1 = "business pick" + rr1
		
		//random drop business name
		Integer rand_int_3 = Math.abs(new Random().nextInt() % 1000000) + 1000
		String rr2 = rand_int_3.toString()
		String rand_b_name_2 = "business drop" + rr2
		
		WebUI.setText(pick_busns_name, rand_b_name_1)
		WebUI.setText(drop_busns_name, rand_b_name_2)
	
		//Search for an address for the Pick and Drop
		//send the keys for 'Arrow Down', and 'Enter' to enter the auto-filled data
		//random integers for address search
		Integer pick_search_rand = Math.abs(new Random().nextInt() % 100) + 1
		String add_1 = pick_search_rand.toString()
		
		Integer drop_search_rand = Math.abs(new Random().nextInt() % 100) + 1
		String add_2 = drop_search_rand.toString()
		
		//search for and set the values
		WebUI.setText(pick_addrs_search, add_1)
		WebUI.delay(1)
		WebUI.sendKeys(pick_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		WebUI.setText(drop_addrs_search, add_2)
		WebUI.delay(1)
		WebUI.sendKeys(drop_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		
		//add a random note
		Integer rand_int_note = Math.abs(new Random().nextInt() % 1000000) + 1000
		String note_string_1 = rand_int_note.toString()
		String n_string = "rand_note " + note_string_1 
		
		WebUI.setText(findTestObject('Object Repository/Load_Request/notes_input'), n_string)
		
		//add random note to return list
		values << n_string
		
		return values
	}

	//try to enter invalid address and verify that no address is added
	public void enter_invalid_address(){
		//enter long string of numbers that don't correspond to any address and Enter
		WebUI.setText(pick_addrs_search, invalid_address)
		WebUI.sendKeys(pick_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		//get the string content of the Street Address field
		String pick_addrs_actual = WebUI.getText(pick_street)
		//verify that the retrieved value is blank
		WebUI.verifyMatch(pick_addrs_actual, "", false)

		//repeat the above for the drop side of the request
		WebUI.setText(drop_addrs_search, invalid_address)
		WebUI.sendKeys(drop_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		//get the string content of the Street Address field
		String drop_addrs_actual = WebUI.getText(drop_street)
		//verify that the retrieved value is blank
		WebUI.verifyMatch(drop_addrs_actual, "", false)

	}

	//try to enter in a valid address after the address search field is cleared out
	public void enter_valid_address(){
		//clear the pick address 1 field - using helper method
		help.clear_text(pick_addrs_search)
		//enter pick valid address
		WebUI.setText(pick_addrs_search, pick_search)
		WebUI.delay(1)
		WebUI.sendKeys(pick_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		//get the string content of the Street Address field
		String pick_addrs_actual = WebUI.getAttribute(pick_street, 'value')
		//verify that an address was populated in the first address field
		WebUI.verifyNotEqual(pick_addrs_actual,'')
		//WebUI.verifyMatch(pick_addrs_actual, expected_pick_addrs, false)

		//clear the drop address 1 field - helper method
		help.clear_text(drop_addrs_search)
		//enter valid drop address
		WebUI.setText(drop_addrs_search, drop_search)
		WebUI.delay(1)
		WebUI.sendKeys(drop_addrs_search, Keys.chord(Keys.DOWN, Keys.ENTER))
		WebUI.delay(1)
		//get the string content of the Street Address field
		String drop_addrs_actual = WebUI.getAttribute(drop_street, 'value')
		//verify that the retrieved value is the expected address
		//verify that an address was populated in the first address field
		WebUI.verifyNotEqual(drop_addrs_actual,'')
		//WebUI.verifyMatch(drop_addrs_actual, expected_drop_addrs, false)
	}

	//verify the Load Request success pop-up text
	public void verify_load_request_success_msg(){
		WebUI.delay(3)
		WebUI.verifyElementPresent(success_popup, 13)
		WebUI.click(OK_btn)
		WebUI.delay(1)
	}

	//click the user avatar drop-down at the upper right of the page, then select the Request History option
	public void select_request_history(){
		WebUI.click(findTestObject('Object Repository/Load_Request/load_request_dropdown'))

		WebUI.delay(3)
		WebUI.click(request_history)
		//click away from the avatar drop-down, so that it goes away
		//WebUI.click(req_hist_labl)
	}

	//sort the load requests by date, so that the newest one is at the top
	public void sort_load_requests() {
		WebUI.click(findTestObject('Object Repository/Load_Request/Request_History/sort_date_down_arrow'))
		WebUI.delay(2)
	}

	//sort the load requests by Status
	public void sort_load_requests_by_status() {
		WebUI.click(findTestObject('Object Repository/Load_Request/Request_History/sort_status_down_arrow'))
		WebUI.delay(2)
	}

	public void open_top_pending_request() {

		sort_load_requests_by_status()

		//get a list of all the pending loads on the page
		List<WebElement> pending_elements = WebUI.findWebElements(findTestObject('Object Repository/Load_Request/Request_History/pending_text'), 10)

		//get the index of the last test object in the list of pending button elements
		int last_index = pending_elements.size() - 1
		//save the test element in the last index of the list
		WebElement newest_pending = pending_elements.get(last_index)

		//need to use the Actions class to double click, since we're using WebDriver methods
		WebDriver driver = DriverFactory.getWebDriver()
		Actions act = new Actions(driver)
		act.doubleClick(newest_pending).perform()

		WebUI.delay(2)
	}

	//add a randomized string to the business name
	//return the string
	public String modify_pending_request() {
		Integer rand_int = Math.abs(new Random().nextInt() % 10000) + 500
		String rand_str = rand_int.toString()

		String new_str = "modified" + rand_str
		WebUI.delay(1)

		WebUI.setText(pick_busns_name, new_str)

		return new_str
	}

	public void verify_load_accepted_status_in_request_history(String po) {
		//open the load in the last index of the accepted button list
		sort_load_requests_by_status()
		WebUI.delay(2)
		//sort_load_requests_by_status()
		//get a list of all the pending loads on the page
		List<WebElement> accepted_elements = WebUI.findWebElements(findTestObject('Object Repository/Load_Request/Request_History/accepted_text'), 10)

		//get the index of the last test object in the list of pending button elements
		int last_index = accepted_elements.size() - 1
		//save the test element in the last index of the list
		WebElement newest_accepted = accepted_elements.get(last_index)

		//need to use the Actions class to double click, since we're using WebDriver methods
		WebDriver driver = DriverFactory.getWebDriver()
		Actions act = new Actions(driver)
		act.doubleClick(newest_accepted).perform()

		//get the value attribute of the PO number field
		String attr = WebUI.getAttribute(findTestObject('Object Repository/Load_Request/po_num'), 'value')

		//verify that the passed-in PO number matches the actual value
		WebUI.verifyMatch(po, attr, false)

		WebUI.delay(2)
	}

	public void verify_load_pending_status_in_request_history() {
		sort_load_requests()
		String status = WebUI.getText(findTestObject('Object Repository/Load_Request/Request_History/top_request_status'))
		WebUI.verifyMatch(status, "PENDING", false)
	}

	public void verify_load_voided_status_in_request_history(String po) {
		sort_load_requests_by_status()
		sort_load_requests_by_status()

		//get a list of all the voided loads on the page
		List<WebElement> voided_elements = WebUI.findWebElements(findTestObject('Object Repository/Load_Request/Request_History/voided_text'), 10)

		//get the index of the last test object in the list of voided button elements
		int last_index = voided_elements.size() - 1
		//save the test element in the last index of the list
		WebElement newest_voided = voided_elements.get(last_index)

		//need to use the Actions class to double click, since we're using WebDriver methods
		WebDriver driver = DriverFactory.getWebDriver()
		Actions act = new Actions(driver)
		act.doubleClick(newest_voided).perform()

		//get the value attribute of the PO number field
		String attr = WebUI.getAttribute(findTestObject('Object Repository/Load_Request/po_num'), 'value')

		//verify that the passed-in PO number matches the actual value
		WebUI.verifyMatch(po, attr, false)

		WebUI.delay(2)
	}

	public void void_load_request() {
		WebUI.click(findTestObject('Object Repository/Load_Request/void_load_request'))
		//confirm
		WebUI.click(findTestObject('Object Repository/Load_Request/yes_button'))
	}
}

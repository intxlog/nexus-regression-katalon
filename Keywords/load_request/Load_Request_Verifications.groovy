package load_request

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.InternalData

import internal.GlobalVariable

public class Load_Request_Verifications {

	//Request History page//
	public TestObject nexus_logo = findTestObject('Object Repository/Load_Summary/nexus_image')
	public TestObject previous_page_btn = findTestObject('Object Repository/Other_Common_Elements/Previous_Page_button')
	public TestObject pick_date = findTestObject('Object Repository/Load_Request/Request_History/req_hist_Pickup_Date')
	public TestObject drop_date = findTestObject('Object Repository/Load_Request/Request_History/req_hist_Drop_Date')
	public TestObject equipment_history = findTestObject('Object Repository/Load_Request/Request_History/req_hist_Equipment')
	public TestObject pick_zip_history = findTestObject('Object Repository/Load_Request/Request_History/req_hist_Pickup_Zip')
	public TestObject del_zip = findTestObject('Object Repository/Load_Request/Request_History/req_hist_Delivery_Zip')
	
	//Load Request page//
	public TestObject pick_date_cal = findTestObject('Object Repository/Load_Request/pickup_date_input')
	public TestObject drop_date_cal = findTestObject('Object Repository/Load_Request/drop_date_input')
	public TestObject equipment_dropdown = findTestObject('Object Repository/Load_Request/equipment_select')
	public TestObject po_num = findTestObject('Object Repository/Load_Request/po_num')
	public TestObject notes = findTestObject('Object Repository/Load_Request/notes_input')
	public TestObject cancel = findTestObject('Object Repository/Other_Common_Elements/Cancel_btn')
	public TestObject submit = findTestObject('Object Repository/Other_Common_Elements/Submit_btn')
	//Picks
	public TestObject pick_bus_name = findTestObject('Object Repository/Load_Request/pickup_business_name_input')
	public TestObject pick_search_addrs = findTestObject('Object Repository/Load_Request/pick_address_search_input')
	public TestObject pick_street = findTestObject('Object Repository/Load_Request/pickup_street_line1')
	public TestObject pick_street_2 = findTestObject('Object Repository/Load_Request/pickup_street_line2')
	public TestObject pick_city = findTestObject('Object Repository/Load_Request/pickup_city')
	public TestObject pick_state = findTestObject('Object Repository/Load_Request/pickup_state')
	public TestObject pick_zip = findTestObject('Object Repository/Load_Request/pickup_zip')
	public TestObject pick_no = findTestObject('Object Repository/Load_Request/pickup_number')
	public TestObject add_new_pick = findTestObject('Object Repository/Load_Request/add_pick_btn')
	//Drops
	public TestObject drop_bus_name = findTestObject('Object Repository/Load_Request/drop_business_name')
	public TestObject drop_search_addrs = findTestObject('Object Repository/Load_Request/drop_address_search_input')
	public TestObject drop_street = findTestObject('Object Repository/Load_Request/drop_street_line1')
	public TestObject drop_street_2 = findTestObject('Object Repository/Load_Request/drop_street_line2')
	public TestObject drop_city = findTestObject('Object Repository/Load_Request/drop_city')
	public TestObject drop_state = findTestObject('Object Repository/Load_Request/drop_state')
	public TestObject drop_zip = findTestObject('Object Repository/Load_Request/drop_zip')
	public TestObject drop_no = findTestObject('Object Repository/Load_Request/pickup_number')
	public TestObject add_new_drop = findTestObject('Object Repository/Load_Request/add_drop_btn')
	
	//Data values object to compare and verify
	InternalData dropdown_values = findTestData('Data Files/dropdown_values')
	
	//verify the elements on the "Load Request" page
	public void load_request_verify(){
		WebUI.verifyElementPresent(pick_date_cal, 6)
		WebUI.verifyElementPresent(drop_date_cal, 6)
		WebUI.verifyElementPresent(equipment_dropdown, 6)
		WebUI.verifyElementPresent(po_num, 6)
		WebUI.verifyElementPresent(pick_bus_name, 6)
		WebUI.verifyElementPresent(pick_search_addrs, 6)
		WebUI.verifyElementPresent(pick_street, 6)
		WebUI.verifyElementPresent(pick_street_2, 6)
		WebUI.verifyElementPresent(pick_city, 6)
		WebUI.verifyElementPresent(pick_state, 6)
		WebUI.verifyElementPresent(pick_zip, 6)
		WebUI.verifyElementPresent(pick_no, 6)
		WebUI.verifyElementPresent(add_new_pick, 6)
		WebUI.verifyElementPresent(drop_bus_name, 6)
		WebUI.verifyElementPresent(drop_search_addrs, 6)
		WebUI.verifyElementPresent(drop_street, 6)
		WebUI.verifyElementPresent(drop_street_2, 6)
		WebUI.verifyElementPresent(drop_city, 6)
		WebUI.verifyElementPresent(drop_state, 6)
		WebUI.verifyElementPresent(drop_zip, 6)
		WebUI.verifyElementPresent(drop_no, 6)
		WebUI.verifyElementPresent(add_new_drop, 6)
		WebUI.verifyElementPresent(notes, 6)
		WebUI.verifyElementPresent(cancel, 6)
		WebUI.verifyElementPresent(submit, 6)
	}
	
	//verify the page elements on the "Request History" page
	public void request_history_verify(){
		WebUI.verifyElementPresent(equipment_history, 6)
		WebUI.verifyElementPresent(del_zip, 6)
		WebUI.verifyElementPresent(drop_date, 6)
		WebUI.verifyElementPresent(pick_zip_history, 6)
		WebUI.verifyElementPresent(pick_date, 6)
		WebUI.verifyElementPresent(previous_page_btn, 6)
		WebUI.verifyElementPresent(nexus_logo, 6)
	}
		
	//verify the elements in the Equipment drop-down selection (taken from the itms smoke test suite)
	public void verify_equipment_type(){
		//get total number of selections in the drop-down
		int trailer_type_size = WebUI.getNumberOfTotalOption(equipment_dropdown)
		
		//loop through each option and compare the value in the data sheet with the values in the drop-down
		for (int option = 0; option < trailer_type_size -1; ++option){
			String trailer_vals = dropdown_values.internallyGetValue(1, option)
			WebUI.verifyOptionPresentByLabel(equipment_dropdown, trailer_vals, false, 5, FailureHandling.CONTINUE_ON_FAILURE)
		}
	}
}
